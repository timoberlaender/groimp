package de.grogra.pf.registry;

public class ContentDescriptionClass extends ContentDescriptionType  {

	@Override
	public int compareTo(ContentDescriptionType b) {
		if (b.getName() == null && this.getName() == null) {
			return 0;
		}
		if (this.getName() == null) {
			return 1;
		}
		if (b.getName() == null) {
			return -1;
		}
		return this.getName().compareToIgnoreCase (b.getName());
	}

	@Override
	public boolean shouldBeAddedToRegistry() {
		return true;
	}

	@Override
	public boolean shouldBeAddedToStaticParser() {
		return getTVisibility().equals("public");
	}
}
