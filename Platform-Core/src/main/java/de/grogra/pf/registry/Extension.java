package de.grogra.pf.registry;

/**
 * Additional .jar required to run.
 */
public class Extension extends Item {

	public Extension() {
		super(null);
	}
	public Extension(String key, String p) {
		super(key);
		setExtpath(p);
	}

	String extpath;
	//enh:field getter setter
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field extpath$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (Extension.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((Extension) o).extpath = (String) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((Extension) o).getExtpath ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new Extension ());
		$TYPE.addManagedField (extpath$FIELD = new _Field ("extpath", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 0));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new Extension ();
	}

	public String getExtpath ()
	{
		return extpath;
	}

	public void setExtpath (String value)
	{
		extpath$FIELD.setObject (this, value);
	}

//enh:end
	
}
