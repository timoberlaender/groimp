package de.grogra.pf.registry;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.SharedObjectNode;
import de.grogra.persistence.PersistenceInput;
import de.grogra.persistence.PersistenceOutput;
import de.grogra.persistence.ResolvableReference;
import de.grogra.persistence.SOReferenceImpl;
import de.grogra.persistence.Shareable;
import de.grogra.persistence.SharedObjectProvider;
import de.grogra.persistence.SharedObjectReference;
import de.grogra.persistence.Transaction;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ProjectLoader;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;

/**
 * A .gsz file (grz?) that contains the resources of a project. It can be added to a
 * plugin to go with rgg compiled classes.
 */
public class ResourceLibrary extends Item implements SharedObjectProvider, SharedObjectReference
{
	private String file;
	//enh:field

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field file$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (ResourceLibrary.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((ResourceLibrary) o).file = (String) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((ResourceLibrary) o).file;
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new ResourceLibrary ());
		$TYPE.addManagedField (file$FIELD = new _Field ("file", _Field.PRIVATE  | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 0));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new ResourceLibrary ();
	}

//enh:end

	transient private boolean loaded = false;
	transient private Registry reg;
	
	public ResourceLibrary() {
		super(null);
	}
	public ResourceLibrary(String key) {
		super(key);
	}
	
	/**
	 * Load a resource from the project file. The resource is in the "dir" 
	 * resourcedirectory, and has the "name".
	 */
	public Object loadResource(String baseName, String name) {	
		Item i = resolveItem (baseName, name, getLibRegistry());
		//TODO: if sharedobjectnode OR SONR -> CHange the provider name into this.getProviderName() &
		if (i instanceof SONodeReference && ((SONodeReference)i).getBaseObjectImpl() instanceof SharedObjectNode) {
			((SharedObjectNode)((SONodeReference)i).getBaseObjectImpl()).setProviderName(this.getProviderName());
			((SharedObjectNode)((SONodeReference)i).getBaseObjectImpl()).getSharedObject().addReference(this);
		}
		else if (i instanceof ObjectItem && ((ObjectItem)i).getObject() instanceof SharedObjectNode) {
			((SharedObjectNode)((ObjectItem)i).getObject()).setProviderName(this.getProviderName());
			((SharedObjectNode)((ObjectItem)i).getObject()).getSharedObject().addReference(this);
		}
		return i;
	}

		
	protected Item resolveItem(String baseName, String name, RegistryContext ctx) {
		StringBuffer b = new StringBuffer (baseName.length () + 8);
		b.append ("/project").append (baseName);
		String dir = b.toString ();
		Item result = null;
		Item d = Item.resolveItem (ctx, dir);
		if (d != null)
		{
			result = d.getItem (name);
			if (result==null) {
				// also check if the user forgot the file extension 
				result = findChildrenWithoutExtension(d, name);
			}
			if (result != null)
			{
				result = result.resolveLink (ctx);
			}
			
		}
		if (result == null)
		{
			d = Item.resolveItem (ctx, b.delete (0, 8).toString ());
			if (d != null)
			{
				result = d.getItem (name);
				if (result==null) {
					// also check if the user forgot the file extension 
					result = findChildrenWithoutExtension(d, name);
				}
				if (result != null)
				{
					result = result.resolveLink (ctx);
				}
			}
		}
		return result;
	}
	
	
	
	protected Item findChildrenWithoutExtension(Item dir, String n) {
		for (Edge e = dir.getFirstEdge(); e != null; e = e.getNext(dir)){
			if (e.getSource()==dir && e.getTarget()!=dir) {
				if (removeExtension(e.getTarget().getName()).equals(n) && e.getTarget() instanceof Item) {
					return (Item) e.getTarget();
				}
			}
		}
		return null;
	}
	protected String removeExtension(String fname) {
		int p;
		if ( (p = fname.lastIndexOf('.')) >=0 ) {
			return fname.substring(0,p);
		}
		return fname;
	}
	
	public Registry getLibRegistry() {
		if (!loaded) {
			loadLib();
		}
		return reg;
	}

	private void loadLib() {
		if (loaded) {
			throw new IllegalStateException("Resource lib : " + getName() + " is already loaded");
		}
		loaded = true;
		File f = null;
		try {
			URL u = getPluginDescriptor().getClassLoader().getResource(file);
			f = new File(u.toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		Registry reg = Registry.create(getRegistry());
		FileSource fs = new FileSource(f, new MimeType("application/x-grogra-project+zip"), reg, new StringMap());
		FilterSource s = IO.createPipeline(fs, IOFlavor.PROJECT_LOADER);
		if (!(s instanceof ObjectSource)) {
			return;
		}
		try {
			ProjectLoader loader = (ProjectLoader) ((ObjectSource) s).getObject();
			StringMap m = new StringMap().putObject("registry", reg);
			((Item) reg.getRoot()).add(new Directory("project"));
			Executable.runExecutables(reg.getRootRegistry(), "/hooks/configure", reg, m);
			loader.loadRegistry(reg);
			Executable.runExecutables(reg.getRootRegistry(), "/hooks/complete", reg, m);
			reg.activateItems();
			loader.loadGraph(reg);
		} catch (IOException e) {
			
		}
		this.reg = reg;
	}

	
	@Override
	public String getProviderName() {
		return getAbsoluteName();
	}
	
	
	/**
	 * Following are used when the graph is read with an attribute that refer to 
	 * a resource in the ResourceLibrary
	 */
	private final SOReferenceImpl ref = new SOReferenceImpl ();

	@Override
	public ResolvableReference readReference(PersistenceInput in) throws IOException {
		//IF in includes '#' -> load from graph
		//IF in startswith '/' -> load from registry (if it starts with '/' it should be "quoted")
		char c = in.peek();
		if (c == '#') {
			String s  = in.readString();
			s = s.substring(1, s.length());
			long id = Long.valueOf(s);
			Object o = getLibRegistry().getProjectGraph().getNodeForId(id);
			if ((o instanceof SharedObjectNode) && (((SharedObjectNode)o).getSharedObject () != null))
			{
				((SharedObjectNode)o).setProviderName(this.getProviderName());
				ref.object = ((SharedObjectNode)o).getSharedObject ();
				ref.object.addReference(this);
				return ref;
			}
				
		} else {
			return getLibRegistry().readReference (in); // probably should resolve this itself then set the provider name?
		}
		return null;
	}
	
	@Override
	public void writeObject(Shareable object, PersistenceOutput out) throws IOException {
	}

	@Override
	public void sharedObjectModified(Shareable object, Transaction t) {
	}
	
}
