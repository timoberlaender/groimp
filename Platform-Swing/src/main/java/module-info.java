module platform.swing {
	exports de.grogra.pf.ui.awt;
	exports com.keypoint;
	exports de.grogra.pf.ui.swing;
	exports de.grogra.docking;

	requires graph;
	requires platform;
	requires platform.core;
	requires utilities;
	requires xl.core;
	requires java.datatransfer;
	requires java.desktop;
	requires java.prefs;
	requires java.xml;
	requires java.logging;
	
	opens de.grogra.docking to utilities;
	opens de.grogra.pf.ui.swing to utilities;
}