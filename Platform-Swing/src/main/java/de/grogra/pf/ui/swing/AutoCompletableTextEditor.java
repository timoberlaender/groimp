package de.grogra.pf.ui.swing;

import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.Segment;

import de.grogra.pf.ui.autocomplete.AutoCompletableTextArea;
import de.grogra.pf.ui.autocomplete.TextAreaEvent;
import de.grogra.pf.ui.autocomplete.TextAreaListener;
import de.grogra.pf.ui.autocomplete.impl.CompletionProvider;

/**
 * An extended text area used by an AutoCompletable panel
 */
public class AutoCompletableTextEditor extends JTextArea implements AutoCompletableTextArea {

	protected static final String EMPTY_STRING = "";
	protected Segment seg;

	public AutoCompletableTextEditor(Document doc) {
		super(doc);
	}

	@Override
	public String getAlreadyEnteredText(CompletionProvider provider) {
		Document doc = getDocument();

		int dot = getCaretPosition();
		Element root = doc.getDefaultRootElement();
		int index = root.getElementIndex(dot);
		Element elem = root.getElement(index);
		int start = elem.getStartOffset();
		int len = dot-start;
		try {
			doc.getText(start, len, seg);
		} catch (BadLocationException ble) {
			ble.printStackTrace();
			return EMPTY_STRING;
		}

		int segEnd = seg.offset + len;
		start = segEnd - 1;
		while (start>=seg.offset && provider.isValidChar(seg.array[start])) {
			start--;
		}
		start++;

		len = segEnd - start;
		return len==0 ? EMPTY_STRING : new String(seg.array, start, len);
	}


	public void initCompletionProvider() {
		seg = new Segment();
	}

	public void selectText(int start, int end) {
		getCaret().setDot(start);
		getCaret().moveDot(end);
	}

	@Override
	public void moveCaretPositionFromXY(int x, int y) {
		Position.Bias[] bias = new Position.Bias[1];
        int index = getUI().viewToModel(this, new Point(x,y), bias);
        moveCaretPosition(index);
	}
	
	@Override
	public int getLineOfCaret() {
		Document doc = getDocument();
		Element root = doc.getDefaultRootElement();
		return root.getElementIndex(getCaretPosition());
	}

	@Override
	public void getText(int start, int end, Segment s) throws BadLocationException {
		getDocument().getText(start, end, s);
	}

	@Override
	public int getDocumentLength() {
		return getDocument().getLength();
	}

	@Override
	public Position createPosition(int pos) throws BadLocationException {
		return getDocument().createPosition(pos);
	}

	@Override
	public Point getXYCaretPosition() {
		try {
			Rectangle r = modelToView(getCaretPosition());
			Point p = new Point(r.x, r.y+15);
			return p;
		} catch (BadLocationException e) {
		}
		return new Point();
	}

	@Override
	public String getBeginLineText(int line) {
		Document doc = getDocument();
		Element root = doc.getDefaultRootElement();
		Element elem = root.getElement(line);
		int startOffset = elem.getStartOffset();
		int endOffset = elem.getEndOffset() - 1; // Why always "-1"?
		try {
			return getText(startOffset,getCaretPosition());
		} catch (BadLocationException e) {
			return "";
		}
	}


	class WrapDocumentListener implements DocumentListener, PropertyChangeListener {
		TextAreaListener ref;
		public WrapDocumentListener(TextAreaListener ref) {
			this.ref = ref;
		}
		@Override
		public void insertUpdate(DocumentEvent e) {
			ref.insertUpdate(new WrapTextAreaEvent(e));
		}
		@Override
		public void removeUpdate(DocumentEvent e) {
			ref.removeUpdate(new WrapTextAreaEvent(e));
		}
		@Override
		public void changedUpdate(DocumentEvent e) {
			ref.changedUpdate(new WrapTextAreaEvent(e));
		}
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			String name = evt.getPropertyName();

			if ("document".equals(name)) {
				// The document switched out from under us
				Document old = (Document)evt.getOldValue();
				if (old != null) {
					old.removeDocumentListener(this);
				}
				Document newDoc = (Document)evt.getNewValue();
				if (newDoc != null) {
					newDoc.addDocumentListener(this);
				}
			}
		}
	}
	class WrapTextAreaEvent implements TextAreaEvent {
		DocumentEvent e;
		PropertyChangeEvent pe;
		public WrapTextAreaEvent(DocumentEvent event) {
			this.e=event;
		}
		public WrapTextAreaEvent(PropertyChangeEvent event) {
			this.pe=event;
		}
		@Override public int getOffset() { return e.getOffset(); }
		@Override public int getLength() { return e.getLength(); }
		@Override public AutoCompletableTextArea getDocument() { return null;}
		@Override public Type getType() { 
			if (e.getType().equals(DocumentEvent.EventType.INSERT)) {
				return TextAreaEvent.Type.INSERT;
			}
			if (e.getType().equals(DocumentEvent.EventType.REMOVE)) {
				return TextAreaEvent.Type.REMOVE;
			}
			if (e.getType().equals(DocumentEvent.EventType.CHANGE)) {
				return TextAreaEvent.Type.CHANGE;
			}
			return null;
		}
		@Override public AutoCompletableTextArea getOldFile() { 
			Object o = pe.getOldValue();
			if (o instanceof AutoCompletableTextArea) {
				return (AutoCompletableTextArea)o; 
			} return null;
		}
		@Override
		public AutoCompletableTextArea getNewFile() {
			Object o = pe.getNewValue();
			if (o instanceof AutoCompletableTextArea) {
				return (AutoCompletableTextArea)o; 
			} return null;
		}
	}

	Map<TextAreaListener, WrapDocumentListener> tlisteners = new HashMap<TextAreaListener, WrapDocumentListener>();

	@Override
	public void addTextAreaListener(TextAreaListener listener) {
		WrapDocumentListener l = new WrapDocumentListener(listener);
		if (tlisteners.get(listener)!=null){
			return;
		}
		tlisteners.put(listener, l);
		getDocument().addDocumentListener(l);
	}
	@Override
	public void removeTextAreaListener(TextAreaListener listener) {
		WrapDocumentListener l = tlisteners.get(listener);
		getDocument().removeDocumentListener(l);
	}


}
