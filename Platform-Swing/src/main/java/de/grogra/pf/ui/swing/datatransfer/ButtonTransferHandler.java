package de.grogra.pf.ui.swing.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

import de.grogra.pf.datatransfer.UIDataHandler;
import de.grogra.pf.datatransfer.UITransferHandler;
import de.grogra.pf.datatransfer.UITransferable;
import de.grogra.pf.datatransfer.UITransferableBase;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.awt.ButtonSupport;
import de.grogra.pf.ui.tree.UITreePipeline;

public class ButtonTransferHandler extends TransferHandler implements UITransferHandler {
	
	List<UIDataHandler> handlers;

	public ButtonTransferHandler() {
	}

	@Override
	public void setDataHandlers(List<UIDataHandler> d) {
		this.handlers = d;
	}

	@Override
	public DataFlavor[] getDataFlavors() {
		return handlers.stream().map(c -> c.getDataFlavors()).toArray(DataFlavor[]::new);
	}

	@Override
	public List<UIDataHandler> getHandlers(){
		return handlers;
	}
	
	@Override
	public boolean canImport(TransferHandler.TransferSupport support) {
		if (handlers == null) { return false; }
		if(!support.isDrop()) {
			return false;
		}
		support.setShowDropLocation(true);

		boolean canImport = false;
		for (UIDataHandler h : handlers) {
			if (h.isDataFlavorSupported(support.getDataFlavors())) {
				canImport = true;
				break;
			}
		}
		if(!canImport) {
			return false;
		}
		return true;
	}
	
	@Override
	public UITransferable createTransferable(Object source) {
		if (!(source instanceof JComponent)) { return null; }
		ButtonSupport s = ButtonSupport.get (source);
		Object o = s.getNode();
		if (o instanceof UITreePipeline.Node) {
			Object i = ((UITreePipeline.Node)o).getNode();
			if (i instanceof Item) {
				return new UITransferableBase(source, i);
			}
		}
		
		return null;
	}
	@Override
	protected Transferable createTransferable(JComponent c) {
		return createTransferable((Object)c);
	}
	
	@Override
	public boolean importData(TransferHandler.TransferSupport support) {
		if(!canImport(support)) {
			return false;
		}
		if (!(support.getTransferable() instanceof Transferable)) {
			return false;
		}
		Object node = loadData((Transferable) support.getTransferable());
		return true;
	}
	
	@Override
    public int getSourceActions(JComponent c) {
        return COPY_OR_MOVE;
    }

}
