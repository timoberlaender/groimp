# GroIMP\'s 3D Plugin

## Overview

As GroIMP\'s primary purpose is the modelling of three-dimensional
virtual plants, GroIMP\'s 3D facilities play a prominent role. They are
made available by the 3D plugin and contain the following features:

-   3D geometry objects: Spheres, cones and other primitives, NURBS
    curves and various kinds of NURBS surfaces, heightfields.

-   Materials can be assigned to objects to produce a variety of shading
    effects in visualization. Materials are defined using a *shading
    network*: This establishes a data flow through the nodes of the
    network which finally leads to the visible material properties like
    colour and opacity. Several kinds of nodes are available: Image
    maps, procedural maps, transformations. You can freely mix them in
    the shading network, which gives you great flexibility in defining
    materials.

-   An integrated viewer displays the current scene in three dimensions.
    Easy navigation is enabled by navigation buttons; objects can be
    selected in the viewer. Currently, the viewer supports a simple
    wireframe representation on all Systems. An OpenGL-based
    representation is available for Systems where the *Java Binding for
    the OpenGL API* ([JOGL](???)) is installed.

-   External renders can be used to create a 3D view of higher quality.
    The POV-Ray Plugin provides a scene export to the POV-Ray format
    which makes it possible to use the free ray-tracer POV-Ray as an
    external renderer, see the [POV-Ray
    manual](plugin:de.grogra.ext.povray/doc/index.html). The resulting
    image is directly shown in GroIMP\'s 3D view.

## The 3D User Interface

GroIMP\'s 3D user interface consists of two panels, see [GroIMP\'s 3D
Panels](#f-3d-panels): The View panel displays the scene, the toolbar
contains buttons for creating objects and selecting the current tool.

<figure id="f-3d-panels">

<figcaption>GroIMP's 3D Panels</figcaption>
</figure>

In the view panel, objects can be selected with the mouse: The object
currently under the mouse is highlighted, and by clicking with the left
mouse button, it becomes selected. If there is more than one object
under the mouse, you can choose the object you want to select by
clicking with the middle mouse button: This cycles through the list of
objects under the mouse. Once an object is selected, its attributes are
shown in the attribute editor where they can be modified.

The currently selected object can also be modified by 3D tools. In
[GroIMP\'s 3D Panels](#f-3d-panels), you can see three arrows which are
the handles for GroIMP\'s 3D translation tool. By dragging the arrows
with the mouse, the object is moved along the arrow direction. If you
use the middle mouse button for dragging, the movement is in a plane
perpendicular to the arrow direction.

Another 3D tool is the rotation tool. It is activated in the toolbar by
clicking on the rotation icon. As handles, it displays three circles
which can be dragged to rotate the selected object.

Navigation in the view panel is possible with the navigation buttons in
the view panel\'s wupper right corner or with the mouse. To navigate
using the buttons, press a button and drag with the mouse while keeping
the button pressed. To navigate using the mouse only, press the Shift
button and drag with the mouse over the view panel: The left mouse
button rotates the view, the middle mouse button moves the viewpoint in
the viewing plane, and the right mouse button makes a dolly movement
along the viewing direction (or a zoom for a view camera with parallel
projection). The mouse wheel works similarly to the right mouse button.

The Camera menu of GroIMP\'s 3D view allows the selection of different
cameras. Some cameras are predefined, among them the common orthographic
projections. You can add user-defined cameras to your project in the
Camera Explorer (the menu item Panels/Object Explorers/3D/Cameras brings
it up); these cameras can be selected as current camera in the Camera
menu, too. They can be edited through the navigation facilities or in
the Attribute Editor.

The Render View menu displays the installed external renderers. The
POV-Ray Plugin defines an interface to the external renderer POV-Ray, a
free ray-tracer, which can be used to render 3D scenes in high quality,
see [POV-Ray manual](plugin:de.grogra.ext.povray/doc/index.html).

## The OpenGL-based 3D User Interface {#s-ogl-ui}

GroIMP features OpenGL-accelerated display of the scene. The
OpenGL-based display may be selected if the *Java Binding for the OpenGL
API* ([JOGL](???)) is installed. The menu View/Display of GroIMP\'s 3D
view brings up the selection for the different supported display modes.
These are:

-   Wireframe (AWT): A wireframe view of the scene. Will run on all
    systems.

-   OpenGL: OpenGL-based view. Will run on all systems with OpenGL 1.1
    support. This mode will display colors and simple textures.

-   OpenGL (Proteus): OpenGL-based view. Requires a OpenGL 2.1
    compatible GPU. This mode will preview the output of the internal
    raytracer Twilight in realtime. It features:

    -   Preview of most materials including procedurally calculated
        shaders using the OpenGL Shading Language (GLSL)

    -   Per pixel lighting

    -   Rendering of shadows

    -   Preview of Sky-Nodes as background

    -   Rendering of transparent materials

Navigation and control is the same for the Wireframe and both
OpenGL-based modes as explained in [The 3D User Interface](#s-3d-ui). A
comparison of all three modes for an example scene is shown in figure
[Comparison of GroIMP\'s 3D view modes](#f-3d-view-comparison)

![Comparison of GroIMP\'s 3D view
modes](res/3d_view_comp.png){#f-3d-view-comparison}

### Options in OpenGL mode 

The OpenGL view can be configured by opening the Option-Dialog
(View/Display/Option/Edit) of GroIMP\'s 3D view while OpenGL is
selected. The dialog features the following options:

-   lighting: If enabled the scene will be lit by the user-defined
    light-nodes (only up to 8 lights will be used). If disabled the
    scene will not be lit resulting in flat colored surfaces. \[This
    option is not available for OpenGL (Proteus) mode\]

-   Level of Detail: This option defines how \"round\" 3D Primitives
    appear. A higher value needs more rendering time but increases the
    quality

-   Show points: If disabled the 3D primitive \"Point\" will not be
    displayed. Points are normally used for displaying information of
    the used RGG model or to visualizie lightsources. These objects are
    not part of the physical scene. Hiding points helps to preview the
    scene as seen in results produced by a raytracer. Disabled this
    option may also increase performance.

-   Show Grid: If enabled a grid of lines is displayed at the scene\'s
    origin. The grid is an orientation helper while navigating and
    modifying the scene.

-   Grid dimension: This value sets the dimension of the displayed grid
    (in meters).

-   Grid spacing: This value defines the spacing between grid lines. A
    spacing of 1.0 means every meter a line is drawn.

-   Grid color (red, green, blue): These three values define the color
    of the grid. Each value may be set to a number between 0.0 and 1.0.
    If all values are set to 1.0 the lines of the grid will be displayed
    in white.

-   Show axes of coordinates: If enabled the orientation of the
    coordinate axes is displayed in the lower left corner. The axis are
    color-coded in red, green and blue representing the x-, z-, y-Axes
    respectively.

-   Show names at axes: If enabled the names of the axes are shown the
    coordinate axes.

-   Show display size: If enabled the size of the view in pixel is
    rendered at the upper left corner of the view.

### Options in OpenGL (Proteus) mode

OpenGL (Proteus) mode shares the options of OpenGL mode (see [Options in
OpenGL mode](#s-ogl-options)). Additionaly the following options are
available in the Option-Dialog (View/Display/Option/Edit):

-   Enable lights: If this option is enabled contribution of all light
    sources in the scene are shown. If disabled a default, directional
    light source replaces all other lights. This option may be disabled
    if a scene features many lights to increase performance.

-   Use physical lighting falloff: If this option is enabled a quadratic
    falloff is used for all lightsources. This approximates the
    pathtracer of Twilight. If disabled no falloff is used which
    approximates the results of the standard raytracer of Twilight.

-   Enable shadows: If enabled shadows will be approximated and rendered
    to the scene. This will decrease performance drastically for dynamic
    scenes and while editing. For static scenes shadows will decrease
    performance only slightly. Disabling this option has the same effect
    as marking all lightsources as shadowless.

-   Show sky: If enabled the material of Sky-Nodes is rendered as the
    scenes background. If disabled a generic grey-colored pattern is
    shown.

-   Show diffuse Sky-Light: If enabled the 3D-View uses Sky-Nodes as an
    additional lightsource. For a bright, blue sky the scene will be
    shaded in a bluish color.

-   Global brightness: This value is used to scale the brightness of the
    rendered image. A low value will darken the scene while a higher
    value will brighten up the displayed picture.

-   Enable tonemapping: If enabled the value for Global-brightness is
    ignored. The image is transformed by the specified algorithm

-   Tonemapping algorithm: The selected Tonemapping algorithm controlls
    how the lit scene is presented to the user. The default value is
    linear mapping which divides the channels of each pixel by the
    maximum valued channel over all pixels.

    DRR (Dynamic Range Reduction) uses the algorithm descriped in the
    paper \"Dynamic Range Reduction Inspired by Photoreceptor
    Physiology\" by Erik Reinhard and Kate Devlin
    ([link](http://dx.doi.org/10.1109/TVCG.2005.9)). Currently only the
    default values for *m*, *f\'*, *c* and *a* are used.

-   Enable shader antialiasing: Some materials will result in hard edges
    or aliasing-artefacts. If enabled the visual quality of materials
    may be improved resulting in a small performance reduction. (This
    feature is not implemented by most shaders up to now)

-   Expand planes to infinity: If enabled planes will be rendered as
    infinitely large. If disabled a simple square is used to represent
    planes. This feature may not be supported by all GPU\'s so disabling
    will help to increase compatibility.

-   Max. depth layers: If set to a value higher than 0 transparent
    objects will be rendered. This value defines the maximum number of
    transparent layers that may be used to composite the image. A higher
    value allows to see more transparent objects behind other
    transparent object. Depending on the scenes setting (especially on
    how many transparent objects are positioned behind each other) a
    high value will reduce performance drastically. A small value
    (around 4) is recommended.

## GroIMP\'s 3D Objects

### Primitives

GroIMP defines the 3D primitive objects sphere, box, cylinder, cone,
frustum, parallelogram, plane, and text label. They can be moved and
rotated interactively as explained in [The 3D User Interface](#s-3d-ui).

### NURBS Curves and Surfaces

NURBS (Non-Uniform Rational B-Splines) are a popular and versatile
representation of curves and surfaces. GroIMP contains full NURBS
support, e.g., techniques to construct NURBS surfaces out of a set of
NURBS curves, including surface skinning, surfaces of revolution,
extrusion and sweep surfaces. A detailed description of these techniques
will follow in later versions of this manual. For the time being, you
can experiment with the surface settings in the Attribute Editor. Of
course, you have to create a surface via Objects/NURBS/Surface at first.

### Scene Objects

The global 3D objects light and sky are contained in GroIMP. They can be
placed into the scene similarly to primitive objects. In the wireframe
representation, the sky object is displayed as a sphere; its position is
irrelevant because it represents a sky sphere at infinity. Visual
effects cannot be seen in the wireframe representation, use POV-Ray to
get a ray-traced image which makes use of these objects.
