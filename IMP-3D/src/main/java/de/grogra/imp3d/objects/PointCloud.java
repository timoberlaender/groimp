package de.grogra.imp3d.objects;

/**
 * Abstract representation of a point cloud node. The data of the point cloud 
 * is stored in the cloud object.
 * A cloud node is required to push the cloud in the project graph, and to
 * manage the I/O operations. 
 */
public abstract class PointCloud extends ColoredNull implements CloudContext {
	
	public abstract void setCloud(Cloud c);
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (PointCloud.class);
		$TYPE.validate ();
	}

//enh:end
}
