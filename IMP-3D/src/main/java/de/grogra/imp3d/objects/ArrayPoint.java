package de.grogra.imp3d.objects;

import java.io.Serializable;

import de.grogra.imp3d.objects.Cloud.Point;
import de.grogra.persistence.Manageable;
import de.grogra.persistence.PersistenceField;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.Transaction;

/**
 * Minimal implementation of a point in a arraycloud. Its Manageable type enables proper 
 * copies of CloudArray/List as the Point needs to be copied 
 */
public class ArrayPoint  implements Point, Manageable, Serializable, Cloneable {
	
	//enh:sco SCOType
	
	protected float[] pos = new float[3];
	//enh:field getmethod=toFloat setmethod=setPos
	protected Cloud parent;
	public ArrayPoint() {}
	public ArrayPoint(float[] f) { 
		setPos(f);
	}
	@Override
	public Cloud getCloud() { return parent; }
	@Override
	public float[] toFloat() { 
		return pos; 
	}
	@Override
	public void setCloud(Cloud c) { this.parent = c; }
	
	public void setPos(float[] f) {
		if(f.length==3)
			pos = f;
	}
	
	@Override public void fieldModified(PersistenceField field, int[] indices, Transaction t) {}
	@Override public int getStamp() {return 0;}
	@Override
	public Manageable manageableReadResolve() {
		return this;
	}
	@Override
	public Object manageableWriteReplace() {
		return null;
	}
	
	// FOR some reason the enhance code do not work here so this has been manually added

	public static final Type $TYPE;

	public static final Type.Field pos$FIELD;
	
	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (ArrayPoint representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 1;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}
		
		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((ArrayPoint) o).setPos ((float[]) value);
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((ArrayPoint) o).toFloat ();
			}
			return super.getObject (o, id);
		}


		@Override
		public Object newInstance ()
		{
			return new ArrayPoint ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (ArrayPoint.class);
		pos$FIELD = Type._addManagedField ($TYPE, "pos", Type.Field.PROTECTED  | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (float[].class), null, Type.SUPER_FIELD_COUNT + 0);
		$TYPE.validate ();
	}

}
