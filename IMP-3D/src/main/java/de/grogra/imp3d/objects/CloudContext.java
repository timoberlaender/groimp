package de.grogra.imp3d.objects;

/**
 * All object in a cloud are part of a cloud context. 
 * CloudNode, Cloud, and Cloud.Point
 */
public interface CloudContext {

	public Cloud getCloud();
}
