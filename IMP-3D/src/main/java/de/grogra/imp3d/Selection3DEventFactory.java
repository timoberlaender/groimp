package de.grogra.imp3d;

import java.util.EventObject;

import de.grogra.imp.viewhandler.SelectionEvent;
import de.grogra.imp.viewhandler.SelectionEventFactory;
import de.grogra.imp.viewhandler.ViewEventHandler;

public class Selection3DEventFactory extends SelectionEventFactory {

	//enh:sco SCOType
	
	@Override
	public SelectionEvent createEvent
	(ViewEventHandler h, EventObject e) {
		return new SelectionEvent3D(h,e);
	}
}
