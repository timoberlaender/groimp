package de.grogra.imp3d;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.imp.PickList;
import de.grogra.imp.viewhandler.PickVisitor;

public interface PickVisitor3D extends PickVisitor{

	public void pick (View3D view, int x, int y,
			  Point3d origin, Vector3d direction, PickList list);
}
