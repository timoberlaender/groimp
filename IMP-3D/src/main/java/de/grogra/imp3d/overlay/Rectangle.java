package de.grogra.imp3d.overlay;

import javax.vecmath.Point2f;

import de.grogra.imp.overlay.OverlayShape;
import de.grogra.imp3d.RenderState;
import de.grogra.math.RGBColor;

public class Rectangle implements OverlayShape {

	int x,y,w,h;
	public Rectangle(int x, int y, int w, int h) {
		this.x=x;
		this.y=y;
		this.w=w;
		this.h=h;
	}
	
	@Override
	public void draw(Object rs) {
		if (!(rs instanceof RenderState)) { return; }
		
		int height = Math.abs(x-w);
		int width = Math.abs(y-h);
		int offsetx = (x<w) ? 0 : height;
		int offsety = (y<h) ? 0 : width;
		((RenderState)rs).drawRectangle(x-offsetx, y-offsety, height, width, RGBColor.BLACK);
	}

	@Override
	public boolean isIn(Point2f p2) {
		if (p2.x > Math.min(x, w) && 
				p2.x < Math.max(x, w) && 
				p2.y > Math.min(y, h) && 
				p2.y < Math.max(y, h)) {
			return true;
		}
		return false;
	}

	@Override
	public Point2f getCenter(Point2f p2) {
		if (p2 == null) {
			int height = Math.abs(x-w);
			int width = Math.abs(y-h);
			int offsetx = (x<w) ? 0 : height;
			int offsety = (y<h) ? 0 : width;
			return new Point2f(x-offsetx+width/2, y-offsety-height/2);
		}
		return null;
	}

	@Override
	public float getRadius(float f) {
		return Math.max(w, h);
	}

}
