---
ftitle: GroIMP User Manual
...

# Preface


Dear user of GroIMP,

this is the user manual of GroIMP, the GROGRA-related
Interactive Modelling Platform. GroIMP is OSI certified open source software
and available at the web page [www.grogra.de](http://www.grogra.de). Currently, the GroIMP software is in a BETA state, which means that malfunctions may occur, though the software has been tested in-depth. The latest news, help and contact to the community can be found on the [web page](http://www.grogra.de), the [wiki](http://http://wiki.grogra.de/doku.php?id=start), the [code repository](https://gitlab.com/grogra/groimp) and our [slack server](https://groimp.slack.com/).

GroIMP arose out of academic research in the field of
plant modelling, mainly carried out by the [Chair for Practical Computer Science / Graphics Systems](http://www-gs.informatik.tu-cottbus.de/) at the Brandenburg University of Technology
Cottbus (Germany) and the [Plant Modelling Group](http://fbi.forst.uni-goettingen.de/) at the Institute of Forest Biometry and Informatics at the University of Gottingen
(Germany). During research, techniques of rule-based plant-modelling were
developed and refined, and quickly it became clear that these rather abstract
techniques have to be accompanied by a smart, interactive and user-friendly
software platform with an up-to-date graphical user interface. The result of
the effort of bringing together elaborated rule-based modelling and
user-friendly, interactive software is GroIMP and its various plug-ins, which
extend GroIMP with additional functionality.

In the current version, GroIMP supports 3D modelling with OpenGL-based 
rendering, including various forms of NURBS
surfaces, rule-based modelling using Lindenmayer systems and rule-based
modelling using Relational Growth Grammars (RGG) expressed in the programming
language XL. It also contains a 2D interface for
visualization and modification of network-like structures.

As
GroIMP is open source software, you are invited to contribute to this
software. Have a look at the web page to get the latest news and source
code!

In its current state, this user manual is by far not
complete. It explains some basics of how to work with GroIMP's user interface,
more detailed documentation will be published later.

We wish you
much fun and success with GroIMP.

The GroIMP development
team

# Installation

GroIMP is
written in Java. Thus,  it should follow Java's WORA-principle (Write Once, Run
Anywhere) and should run on every Java-capable platform. GroIMP has been tested
successfully on Unix and  Windows platforms.

GroIMP 2.1.6 requires
the installation of a  Java runtime environment of version 17. Which
can be obtained from Oracle at [[https://www.oracle.com/|www.oracle.com]].

For a reasonably
performant runtime behaviour, at least 256 MB RAM and a 1 GHz processor are
recommended.

## Download

The binary distribution of GroIMP is available on the [[https://gitlab.com/grogra/groimp/-/releases|gitlab release section]] as a zip-file, windows exe, or debian release.

## Installation Installation from binaries

The windows exe and debian release are executable files that will extract and install GroIMP on your computer. It will also provide you to create a menu entry and a shortcut.
Mac users need to download the zip-file, extract it and run GroIMP from command line.

The directory structure of the zip-file has to be preserved, check
that your zip program is set up accordingly (and does not extract all files
into one  and the same directory).

After extraction of the zip-file, the following files, among others, are present in the installation
directory:

`README` 

This plain text file contains additional information about the GroIMP distribution. Read it
carefully.

`INSTALL` 

This plain text file contains  instructions for the installation of the GroIMP
distribution, legal notices and other information. Read it carefully. If you
have troubles with the installation procedure described in this manual, consult
the file `INSTALL` a solution.

`plugins`

This directory contains all the plugins that are part of the binary distribution of
GroIMP.

`core.jar`

This file is an executable java archive and the entry-point for the Java runtime
environment to start the GroIMP application.

## Running GroIMP

Because `core.jar` is an executable java archive, it is possible to start GroIMP just by (double-)clicking on the file in your system's file browser as you would do it with a usual executable file. This requires a suitable setup of your file browser, see the documentation of the browser and your Java installation if it does not already work.

### From a Shortcut

You can set up your desktop to show a menu entry for GroIMP. How this is done depends on your system. Usually your system provides a menu editor where you can create a new menu entry and specifiy the command to be executed when the entry is activated. Choose the command as you would do on command line.

### From the Command Line

The file `core.jar` in your installation directory of GroIMP's binary distribution is the entry-point for starting GroIMP. Running GroIMP from the command line is easy:

> **java -jar path/to/core.jar**

Of course, `path/to/core.jar` has to be replaced by the actual path to the file `core.jar`. E.g., if, on a Unix system, the installation directory is `/usr/lib/groimp`, the command would be

> **java -jar /usr/lib/groimp/core.jar**

or, on a Windows system with installation directory `C:\Program Files\GroIMP`,

> **java -Xverify:none -jar "C:\\Program Files\\GroIMP\\core.jar"**

## Additional Options

### Java Parameters

The **java** command has several options which influence the runtime behaviour, see the documentation of your Java installation. These options have to be specified before the `-jar` option. E.g., for the Java runtime environment of Sun, the option `-Xmx` specifies the maximum amount of heap memory size that should be allocated. The default value of this option may be much smaller than your installed main memory size. In this case GroIMP cannot benefit from your memory until you specify the `-Xmx` option as in

> **java -Xmx400m -jar path/to/core.jar**

which allows a maximum heap memory allocation of 400 MB. If you encounter `OutOfMemoryError`s when running GroIMP, you have to specify this option with a larger value.

### GroIMP Parameters

The command line also enables to provides parameters to GroIMP. These parameters need to be passed AFTER the `core.jar` file. E.g.

> **java -jar core.jar --project-tree**

The complete list of GroIMP parameters can be found [online](http://wiki.grogra.de/doku.php?id=user-guide:advanced-groimp-arguments)

# Plugin Architecture 
## Java plugins

Installation of Plugins for the Java Image I/O Framework.

GroIMP requires only a Java runtime environment in order to run. However, the capabilities of some parts of the Java runtime environment can be extended by the installation of additional plugins. GroIMP makes use of the Java Image I/O Framework which provides such a pluggable architecture: The user may install plugins which add support for an image file format to the Java Image I/O Framework. If the standard installation of the Java runtime environment does not support an image file format you need, please consult the [Java Image I/O pages](http://java.sun.com/j2se/1.4.2/docs/guide/imageio/index.html) for a suitable plugin.

## GroIMP Plugins 

GroIMP is developed around a plugin architecture. New plugin can easily be added to your GroIMP installation by simply adding them to the loading directory of GroIMP. 

On startup GroIMP load all plugins under it's installed path/plugins, as well as plugins in the `user plugin path`. This user plugin path can be modified in the GroIMP options, and is by default at ~/.grogra.de-platform/plugins/. 

GroIMP plugins can be installed/ removed/ modified from within GroIMP with the Plugin Manager. The plugin manager can be found in GroIMP on [help/plugin-manager](plugin:de.grogra.plugin.manager/doc/index.html). More information can be found in its own documentation.
A complete list of available plugin can be found [online](https://grogra.gitlab.io/groimp-utils/plugin_web_explorer/).

# Using GroIMP

## User Interface

GroIMP is equipped with a modern, configurable graphical user interface.
It supports user-defined layouts of panels, including arrangement of
panels in tab groups and floating windows. Screenshot of GroIMP
shows a typical screenshot with 3D view.

![Screenshot of GroIMP](src/main/resources/doc/res/GroIMP.jpg){#f-ui}

You can see the panels View, Attribute Editor, File Explorer, Text
Editor, Toolbar and Statusbar. The panels Images and Materials are
hidden, they are arranged together with File Explorer in a *tab group*.
The panel Text Editor is not contained in GroIMP\'s main window but in
its own *floating window*. Details of these panels will be described in
the following.

## Common Panel Handling

GroIMP\'s user interface consists of an arrangement of panels, e.g., a
3D view or a text editor. This arrangement can be changed by the user by
Drag&Drop: For panels with title bars or panels in a tab group, a
Drag&Drop operation is performed by dragging title bars with the mouse.
A contour indicates where the panel will be dropped if you release the
mouse button. For panels without title bars (e.g., toolbars,
statusbars), just drag the panels themselves.

Every panel has a panel menu which provides at least the operations
Undock and Close. A panel menu is opened by clicking on the title bar of
the panel or, if there is no title bar, on the panel itself (usually
with the right mouse button). The Undock operation undocks the panel out
of its current location and brings it up in a new floating window, the
Close operation closes a panel.

Normally, not all possible panels are actually present in the user
interface. However, all panels are accessible through the Panels menu of
GroIMP\'s main window. If a panel you need is missing, just select it in
the main Panels menu. This brings up the panel in a new floating window.

The arrangement of all panels is called a panel *layout*. The menu
Panels/Set Layout provides a set of predefined layouts which are useful
for distinct tasks; the menu item Panels/Add Layout adds the current
layout to the list of user-defined layouts of the current project.
User-defined layouts are also accessible in the menu Panels/Set Layout.

## GroIMP\'s Projects

The main entity you work on in GroIMP is the *project*. A project may
consist of various parts, e.g., files, source code, a scene (2D, 3D, or
other), resource objects like data sets, 3D materials or the like.
Several projects can be open at the same time, each in its own main
window.

You create a new project by choosing the menu item File/New/Project. Via
File/Open, projects can be read from a file. Different file formats are
available: GS and GSZ are GroIMP\'s native project formats, the other
formats are imported into a new project.

Saving of a project is done in the File menu too. Here, only GS and GSZ
are available as file formats. The GS file format in fact consists of a
set of files written in the folder containing the GS file: `graph.xml`
contains the scene graph of the project, the folder `META-INF` some meta
information about the files, and if there are files included in the
project, they will be written in the folder (or subfolders) too. To
avoid conflicts between different projects, it is mandatory to use an
own folder for each project.

Contrary to the GS file format, the GSZ file format only consists of a
single file. This file is actually a file in the common zip-format with
special content: It contains all the files which comprise a project in
the GS file format in a single zip archive. You can use standard zip
tools to examine or even modify its contents.

While working with an open project, there is one difference between
projects in GS/GSZ file format: If you modify and save files (e.g., text
files) contained in a GS project, they will be written to your file
system immediately because the GS file format consists of a set of files
in your file system. However, for a GSZ project, these files are written
to an internal storage: Your modifications are persistently saved only
when the whole project is saved.

## Common Panels

### File Explorer

GroIMP\'s File Explorer shows all those files of the project which have
an immediate meaning as files to GroIMP. These are source code files,
plain text files, HTML files and the like. Files which are used to
define non-file-like objects (e.g., images, 3D geometry, data sets) are
not shown in the file explorer panel, they are accessible in the panels
of the corresponding object type.


![File Explorer](src/main/resources/doc/res/fileexplorer.png){ #f-fileexplorer }

If the file explorer is not already shown, open the panel via the Panel
menu. As you will know it from your system\'s file browser, GroIMP\'s
file exporer displays files in a tree-like structure. Files may be
activated by a double-click or pressing the Enter key: On activation,
source code and text files are opened in the text editor, HTML files are
shown in GroIMP\'s integrated browser. At the moment, files cannot be
renamed, moved or deleted in the file explorer.

Files which already exist in your file system can be added to a project
via the menu item Object/New/Add File. You have the choice to *link* or
*add* these files: A linked file does not become part of the project,
the project just references it in your file systems. Changes to a linked
file take effect on the project when GroIMP (re-)opens the file. In
contrast to a linked file, an added file is copied into the project,
thus, after addition, there is no connection between the original file
and the project any more.

In File Explorer, a file explorer panel is shown.
`FTree.rgg` is a source code file, `FTree.txt` a text file which
contains some explanations about the project FTree, and `index.html` is
a link (indicated by the small arrow at the lower left corner of the
icon) to an HTML file.

### Text Editor

GroIMP is equipped with a simple internal text editor. When you activate
a source code or text file entry in the file explorer, it is opened in
the text editor and can be edited. The usual editing operations
(Cut&Paste, Undo/Redo, Save) are available. Text Editor f-texteditor
shows a screenshot.

<figure id="f-texteditor">

![Text Editor](src/main/resources/doc/res/texteditor.png)
</figure>

For some file types, additional actions are triggered when a file of
that type is saved. For example, source code is compiled immediately.

### Text Editor jEdit

If the jEdit-Plugin is installed, the [jEdit](http://www.jedit.org/)
text editor is used instead of GroIMP\'s simple internal text editor.
Text Editor jEdit shows a screenshot. jEdit
supports syntax highlighting and contains various edit, search, and
print commands. Comprehensive documentation is available via the
Help-menu of jEdit.

<figure id="f-texteditor-jedit">

![Jedit texteditor](src/main/resources/doc/res/texteditor_jedit.png)
</figure>

### Image Explorer

Every image used within a GroIMP project, e.g., a colouring texture of a
3D material, is shown in the Image Explorer. Its structure is similar to
the File Explorer. New images can be added to a project via the menu
Object/New: Currently, this menu contains only the item From File which
reads an existing image file into the project. All file formats which
are supported by your installation of the Java Image I/O Framework are
readable, these are at least Portable Network Graphics (`png`), JPEG and
Graphics Interchange Format (`gif`). If a format you need is not
supported by your installation, see [Installation of Plugins for the
Java Image I/O Framework](#s-imageio). As for the File Explorer, you
have the choice to link or add image files to the project.

<figure id="f-imgmatattr">

![Image Explorer, Material Explorer, Attribute Editor](src/main/resources/doc/res/imgmatattr.png)
</figure>

### Attribute Editor

Another panel which you will encounter often is the attribute editor.
It is used to edit attributes of a variety of objects: User settings of
configuration objects, geometric attributes of scene objects, attributes
of resource objects like 3D materials, and others.

Each attribute has a type, and each type brings up specific edit
components in the attribute editor. For example, the radius attribute of
circles or spheres is a numeric attribute and is editable through a
simple input field. The orientation of a cylinder in 3D space is a 3D
vector and, thus, it is editable through a set of three numeric input
fields. Very complex attributes like 3D materials consist of several
subattributes, each of which brings up its own edit components in the
attribute editor.

In Image Explorer, Material Explorer, Attribute Editor,
you can see the attribute editor for a 3D material of type Phong. Such a
material has the attributes Channel Input, Diffuse Colour, Transparency,
Interpolated Transparency, Ambient Colour, Emissive Colour, Specular
Colour and Shininess. The Phong material shown has a Checkerboard 2D
surface map as its Diffuse Colour, the checkerboard itself has the
attributes Channel Input, Colour 1 and Colour 2. As Channel Input, an
UV-Transformation is chosen, its numeric attributes are editable through
the input fields. For angles and other attributes which represent
physical quantities, their units are shown, in the this case `deg` which
stands for degrees. As Colour 1, an RGB colour is chosen, whose values
are modified by sliders.

Colour 2 of the checkerboard is set to an image map. One could have
chosen an RGB colour as for the first colour, a Checkerboard 2D as for
the diffuse colour of the material, a simple constant colour, or some
other valid type for this attribute. In GroIMP, there are many
attributes whose values may be of a set of different types. For all
these attributes, a button is shown in the attribute editor which pops
up a menu to choose the desired type. Once a type has been chosen, the
attribute value is set to this type, and appropriate edit components are
created. They are shown within a minimizable subpanel. To minimize it
(and to maximize it later on), click on its upper border where a small
button is displayed. For example, the subpanel for the RGB components of
the Opacity attribute is minimized in Image Explorer, Material
Explorer, Attribute Editor. A double-click on the upper
border maximizes the subpanel and minimizes all other panels.

### Preferences

 In GroIMP the preferences (from java preferences) are the software options. Most GroIMP options are managed by the Preference panel, which can be open from the menu at Panel>Preferences. By default, options are managed globally. i.e. when an option is set, every workbench will use it. There is an exception though. It is possible to setup an option file for a workbench, thus, using options defined in that file instead of the global ones. 

<figure id="f-preferences">

![Preferences Panel (Metal Look &amp; Feel)](src/main/resources/doc/res/preferences.png)

</figure>

#### Global options

 Usually when a user change the options of GroIMP, he changes the global options. These options are stored when set in the java preferences (in `~/.java/.userPrefs/de/grogra/options/` + path of the option). This enables the options to be persistent between GroIMP uses. It also makes the options persistent when an other version of GroIMP is installed.

Global options are used by every workbenches that do not include an option file.

The panel of global options can be open if the currently used workbench do not have an option file. In that case, when the preference panel is open (with `Panel>Preferences`) you can see the Label on the bottom right that tells the current panel is Global (see the red circle on the Image below). 


![Global preference panel](src/main/resources/doc/res/global_preferences_panel.png)

#### Workbench options

GroIMP also support options specific for a given workbench (since version 2.1.4). The option file (called “workbench.options”) is shipped with the GroIMP project (at the root of the project.gs file, or in the .gsz archive). It enables to share a project with a specific set of options.

When the option file exists in a project, GroIMP will use its options instead of the global ones. If an option file contains more options than the current instance of GroIMP, these options are kept but not use. If the option miss some options that the current instance of GroIMP defined, these options are added to the option file when saved.

The option file can be added in a workbench from the preference panel (Panel>Preferences). On the menu bar `file>options>Create current workbench option file`. 

![menu options](src/main/resources/doc/res/panel_opt_add.png)

Once the option file is added, the view of the preference panel will change into the workbench option view In that view the name of the workbench whose options are managed is displayed in the red circle 1. On the bottom right a button to open the global options is available (see the red circle 2). 


![ workbench option panel](src/main/resources/doc/res/wb_preferences_panel.png)

 When a option file is created and added to the project it is automatically setup with all the existing current global options. When modifying workbench options, the options new value is directly updated, however the option file is not automatically updated. To push the modification to the option file the project needs to be saved.

To stop using the option file of a workbench, it can be deleted from the menu option: `file>options>Delete` current option file.

The file can be modified outside of GroIMP as plain text. To add an option file in a project outside of GroIMP, the file needs to be added *and* the registry item OptionsSource needs to be added to the project.gs. Same when deleting the option file from outside of GroiMP: both the both the file and the registry item needs to be deleted. The MANIFEST.MF file must be updated as well. 

#### Options for the Swing User Interface

The group UI Toolkit/Swing UI in the Preferences panel lets you choose
the Look & Feel of the Swing user interface and whether window
decorations (title bar, borders, etc.) should be provided by Swing or by
the native window manager of your system. The set of available Look &
Feels depends on your Java and GroIMP installations: GroIMP\'s binary
distribution is bundled with the Kunststoff, Liquid and Tonic Look &
Feels.

If you switch to another Look & Feel in the Preferences panel, the user
interface will be redrawn immediately. However, changing the window
decorations option only takes effect when a new window is opened. Also,
there are Look & Feels which do not support window decorations.

### Other Explorer Panels

Besides files and images, a variety of other object kinds is displayed
within an explorer panel. For example, Image Explorer, Material
Explorer, Attribute Editor shows the Material Explorer
panel. These panels are always similar to the file explorer or the image
explorer: Objects are shown in a hierarchical tree-like layout, and new
objects are created by the Object/New menu of the panel. The contents in
this menu depend on the explorer: The Image Explorer reads in images
from files, the Material Explorer allows the creation of new materials
of several material types, etc.

## Import and Export Formats: MTG - Multiscale Tree Graph

EXAMPLE - Importing and Using MTG data in GroIMP

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--

\*\*NOTE: In this example, a .mtg file is already imported and a .rgg
file has already been added into the project.

Begin at step 1 if you wish to find out how to load a .mtg file into
GroIMP.

Skip to step 6 if you wish to know how to modify the loaded MTG data in
this example project.

-   1\. Open an MTG file
-   2\. MTG node classes and corresponding GroIMP modules
-   3\. MTG feature attributes and GroIMP module variables
-   4\. Visualizing the MTG data
-   5\. Saving the MTG graph in a GroIMP project
-   6\. Modifying the loaded MTG graph



