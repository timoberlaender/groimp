package de.grogra.imp.objects;

import de.grogra.graph.impl.Node;

/**
 * Context of objects that are related to a node and can be transformed into a node.
 * This context do not include object that already are nodes .
 * Warning: Node objects are not a NodeContext. Only object/field/ that will be 
 * serialized and will require a Node when loading should implements this interface.
 * Or object that can be consumed by a Producer/Instantiator to produce a Node should
 * implement this interface.
 */
public interface NodeContext extends ProducingNode {
	public Node getNode();
	public void setNode(Node n);
}
