package de.grogra.imp;

/**
 * Nodes that are ResolutionSensitive can produce a different behavior when 
 * they are visited by a Visitor which is STOP by they resolution
 */
public interface ResolutionSensitive {

	public enum State {NORMAL, STOPPED, PASSED};
	public void setState(State s);
	public State getState();
}
