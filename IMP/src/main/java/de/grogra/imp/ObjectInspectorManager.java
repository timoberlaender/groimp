package de.grogra.imp;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.EventObject;
import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.JTree;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import de.grogra.graph.ChangeBoundaryListener;
import de.grogra.graph.EdgeChangeListener;
import de.grogra.graph.GraphState;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.imp.ObjectInspector.TreeNode;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.ComponentWrapper;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.edit.GraphSelection;
import de.grogra.pf.ui.event.UIPropertyEditEvent;
import de.grogra.pf.ui.util.ComponentWrapperImpl;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.util.EventListener;
import de.grogra.util.I18NBundle;
import de.grogra.util.Map;

public class ObjectInspectorManager {

	final static HashMap<GraphManager, JTree> hierarchicalObjectInspectors = new HashMap<GraphManager, JTree>();
	final static HashMap<GraphManager, JTree> flatObjectInspectors = new HashMap<GraphManager, JTree>();
	
	boolean needUpdate = false;
	
	private void createObjectInspector(Panel panel, final Context ctx, GraphManager graph,
			Map params, HashMap<GraphManager, JTree> objectInspectors, final ObjectInspector oi) {
		UIToolkit ui = UIToolkit.get(ctx);
		
//		panel.initialize(ctx.getWindow(), params);
				
		final JTree objectTree;
		
		if (objectInspectors.containsKey(graph)) {
			objectTree = objectInspectors.get(graph);
		}
		else {
			objectTree = new JTree(oi);
			objectInspectors.put(graph, objectTree);
			
			// refresh view when graph changes
			graph.addChangeBoundaryListener(new ChangeBoundaryListener() {
				public void beginChange(GraphState gs) {}
				public void endChange(GraphState gs) {
					if (!needUpdate)
						return;
					oi.buildTree();
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							objectTree.updateUI();
						}
					});
					needUpdate = false;
				}
				public int getPriority() {
					return 0;
				}
			});
			
			graph.addEdgeChangeListener(new EdgeChangeListener() {
				public void edgeChanged(Object source, Object target,
						Object edge, GraphState gs) {
					needUpdate = true;
				}
			});
					
			objectTree.setDragEnabled(false);
			objectTree.setEditable(false);
			objectTree.setExpandsSelectedPaths(true);
			objectTree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
			objectTree.addTreeSelectionListener(oi);
			
			// change selection in objectree when selection changes in GroIMP
			UIProperty.WORKBENCH_SELECTION.addPropertyListener(ctx, new EventListener() {
				public void eventOccured(EventObject event) {
					if (event instanceof UIPropertyEditEvent) {
						UIPropertyEditEvent uipee = (UIPropertyEditEvent) event;
						if (oi.isActiveTreeSelection()) {
							oi.setActiveTreeSelection(false);
							return;
						}
						if (uipee.getNewValue() instanceof GraphSelection) {
							GraphSelection gs = (GraphSelection) uipee.getNewValue();
							int selectCount = gs.size();
							TreePath[] selectionPaths = new TreePath[selectCount];
							
							for (int i = 0; i < selectCount; i++) {
								if (gs.getObject(i) instanceof Node) {
									TreeNode treeNode = oi.getTreeNodeForNode((Node) gs.getObject(i));
									if (treeNode != null) {
										LinkedList<TreeNode> path = new LinkedList<TreeNode>();
										oi.getPathToTreeNode(treeNode, path);
										TreePath selectionPath = new TreePath(path.toArray());
										selectionPaths[i] = selectionPath;
									}
								}
							}
							oi.setActiveGISelection(true);
							objectTree.setSelectionPaths(selectionPaths);
							oi.setActiveGISelection(false);
						}
						else if ((uipee.getOldValue() instanceof GraphSelection)
								&& (uipee.getNewValue() == null)) {
							oi.setActiveGISelection(true);
							objectTree.clearSelection();
							oi.setActiveGISelection(false);
						}
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								objectTree.updateUI();
							}
						});
					}
				}
			});
			
				
		}
		
		I18NBundle thisI18NBundle = ctx.getWorkbench().getRegistry().getPluginDescriptor("de.grogra.imp").getI18NBundle();
		
		// Buttons
		Object setFilterButton = ui.createButton (thisI18NBundle.getString ("setFilterButton.Name"), 
				null, null, Font.PLAIN, new Command () {
					@Override
					public void run (Object info, Context ctx)
					{
						oi.setFilter(objectTree.getSelectionPaths(), false);
						oi.buildTree();
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								objectTree.updateUI();
							}
						});
					}
					@Override
					public String getCommandName ()
					{
						return null;
					}
				}, ctx);
		
		Object setHierarchicFilterButton = ui.createButton (thisI18NBundle.getString ("setHierarchicFilterButton.Name"), 
				null, null, Font.PLAIN, new Command () {
					@Override
					public void run (Object info, Context ctx)
					{
						oi.setFilter(objectTree.getSelectionPaths(), true);
						oi.buildTree();
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								objectTree.updateUI();
							}
						});
					}
					@Override
					public String getCommandName ()
					{
						return null;
					}
				}, ctx);
		
		Object removeFilterButton = ui.createButton (thisI18NBundle.getString ("removeFilterButton.Name"), 
				null, null, Font.PLAIN, new Command () {
					@Override
					public void run (Object info, Context ctx)
					{
						oi.removeFilter();
						oi.buildTree();
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								objectTree.updateUI();
							}
						});
					}
					@Override
					public String getCommandName ()
					{
						return null;
					}
				}, ctx);

		Object expandAllButton = ui.createButton (thisI18NBundle.getString ("expandAllButton.Name"), 
				null, null, Font.PLAIN, new Command () {
					@Override
					public void run (Object info, Context ctx)
					{
						expandAll(objectTree);
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								objectTree.updateUI();
							}
						});
					}
					@Override
					public String getCommandName ()
					{
						return null;
					}
				}, ctx);

		Object collapseAllButton = ui.createButton (thisI18NBundle.getString ("collapseAllButton.Name"), 
				null, null, Font.PLAIN, new Command () {
					@Override
					public void run (Object info, Context ctx)
					{
//						expandAll(objectTree);
						collapseAll(objectTree);
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								objectTree.updateUI();
							}
						});
					}
					@Override
					public String getCommandName ()
					{
						return null;
					}
				}, ctx);
		
		// Button Panel
		Object buttonPanel = ui.createContainer(0);
		ui.setLayout(buttonPanel, new FlowLayout(FlowLayout.LEFT));
		ui.addComponent(buttonPanel, setFilterButton, null);
		ui.addComponent(buttonPanel, setHierarchicFilterButton, null);
		ui.addComponent(buttonPanel, removeFilterButton, null);
		ui.addComponent(buttonPanel, expandAllButton, null);
		ui.addComponent(buttonPanel, collapseAllButton, null);
		
		// Tree		
		ComponentWrapper tree = ui.createUITreeComponent(objectTree);
		Object scrollPane = ui.createScrollPane(tree);
		
//		JScrollPane scrollPane = new JScrollPane(objectTree);
		
		// Layout
		Object contentPane = ui.createContainer(5);
		ui.addComponent(contentPane, buttonPanel, BorderLayout.NORTH);
		ui.addComponent(contentPane, scrollPane, BorderLayout.CENTER);
		
		panel.setContent (new ComponentWrapperImpl (contentPane, tree));
	}
	
	private void expandAll(JTree objectTree) {
	    int row = 0;
	    while (row < objectTree.getRowCount()) {
	    	objectTree.expandRow(row);
	    	row++;
	    }
	}
	
	private void collapseAll(JTree objectTree) {
	    int row = objectTree.getRowCount() - 1;
	    while (row >= 0) {
	    	objectTree.collapseRow(row);
	    	row--;
	    }
	}
	
	public static Panel createHierarchicalObjectInspector (Context ctx, Map params) {
		GraphManager graph = ctx.getWorkbench().getRegistry().getProjectGraph();
		UIToolkit ui = UIToolkit.get(ctx);
		Panel p = ui.createPanel(ctx, null, params);
		ObjectInspectorManager mgr = new ObjectInspectorManager();
		mgr.createObjectInspector(p, ctx, graph, params, hierarchicalObjectInspectors, new HierarchicalObjectInspector(ctx, graph));
		return p;
	}
	
	public static Panel createFlatObjectInspector (Context ctx, Map params) {
		GraphManager graph = ctx.getWorkbench().getRegistry().getProjectGraph();
		UIToolkit ui = UIToolkit.get(ctx);
		Panel p = ui.createPanel(ctx, null, params);
		ObjectInspectorManager mgr = new ObjectInspectorManager();
		mgr.createObjectInspector(p, ctx, graph, params, flatObjectInspectors, new FlatObjectInspector(ctx, graph));
		return p;
	}
	
}
