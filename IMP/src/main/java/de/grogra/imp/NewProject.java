
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.imp;

import java.io.IOException;

import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.*;

public class NewProject implements Command
{

	public String getCommandName ()
	{
		return null;
	}

	
	protected String getProjectName ()
	{
		return IMP.I18N.getString ("project.new");
	}
	
	
	protected Workbench configure (Context ctx, Object arg)
	{
		try {
			Object id = ctx.getWorkbench().getApplication().create("newEmpty", "Project");
			Workbench wb = ctx.getWorkbench().getApplication().getWorkbenchManager().getWorkbench(id);
			if (wb !=null) {
				wb.setProperty (Workbench.INITIAL_LAYOUT, "/ui/layouts/rgg");
				return wb;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	public void run (Object arg, Context context)
	{
		Workbench wb = configure(context, arg);
		if (wb!=null) {
			wb.getMainWorkbench ().setProperty (Workbench.PROJECT_DIRECTORY,null);
			Registry.setCurrent (context.getWorkbench ());
		}
	}
}