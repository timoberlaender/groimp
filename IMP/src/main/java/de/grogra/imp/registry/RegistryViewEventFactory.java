package de.grogra.imp.registry;

import java.lang.reflect.InvocationTargetException;

import de.grogra.imp.View;
import de.grogra.imp.viewhandler.ViewEventFactory;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.registry.ChoiceGroup;
import de.grogra.pf.ui.registry.UIItem;
import de.grogra.pf.ui.tree.UINodeHandler;
import de.grogra.util.WrapException;

public class RegistryViewEventFactory extends Item implements UIItem{

	private String cls;

	

	public RegistryViewEventFactory ()
	{
		super (null);
	}

	public ViewEventFactory createViewEventFactory (Context ctx)
	{
		try
	{
			ViewEventFactory c = (ViewEventFactory) classForName (cls, true).getDeclaredConstructor().newInstance();
			c.init(ctx);
		return c;
	}
	catch (ClassNotFoundException e)
	{
		throw new WrapException (e);
	}
	catch (IllegalAccessException e)
	{
		throw new WrapException (e);
	}
	catch (InstantiationException e)
	{
		throw new WrapException (e);
	} catch (IllegalArgumentException e) {
		throw new WrapException (e);
	} catch (InvocationTargetException e) {
		throw new WrapException (e);
	} catch (NoSuchMethodException e) {
		throw new WrapException (e);
	} catch (SecurityException e) {
		throw new WrapException (e);
	}
	}

		
	public int getUINodeType ()
	{
		return UINodeHandler.NT_ITEM;
	}


	public Object invoke (Context ctx, String method, Object arg)
	{
		return null;
	}


	public boolean isAvailable (Context ctx)
	{
		return true;
	}


	public boolean isEnabled (Context ctx)
	{
		return true;
	}
	
	@Override
	protected boolean readAttribute (String uri, String name, String value)
		throws org.xml.sax.SAXException
	{
		if ("".equals (uri))
		{
			if ("class".equals (name))
			{
				cls = value;
				return true;
			}
		}
		return super.readAttribute (uri, name, value);
	}

	public static RegistryViewEventFactory get (View view, String itemPath)
	{
		Item g = Item.resolveItem (view.getWorkbench (), itemPath);
		Item i = (g instanceof ChoiceGroup) ? ((ChoiceGroup) g).getPropertyValue (view)
			: null;
		if ((i instanceof RegistryViewEventFactory)
			&& ((RegistryViewEventFactory) i).isAvailable (view))
		{
			return (RegistryViewEventFactory) i;
		}
		else
		{
			for (i = (Item) g.getBranch (); i != null; i = (Item) i.getSuccessor ())
			{ 
				if ((i instanceof RegistryViewEventFactory)
					&& ((RegistryViewEventFactory) i).isAvailable (view))
				{
					return (RegistryViewEventFactory) i;
				}
			}
		}
		return null;
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new RegistryViewEventFactory ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new RegistryViewEventFactory ();
	}

//enh:end
}
