package de.grogra.imp.viewhandler;

import java.util.EventObject;

import de.grogra.pf.ui.Context;

/**
 * Wrap event listener that happen in a view. The event is triggered if the isActivationEvent is true.
 */
public interface DisposableViewEventFactory {
	
	public boolean isActivationEvent (ViewEventHandler h, EventObject e);
	public de.grogra.util.DisposableEventListener createEvent(ViewEventHandler h, EventObject e);
	public default void init(Context ctx) {};
}
