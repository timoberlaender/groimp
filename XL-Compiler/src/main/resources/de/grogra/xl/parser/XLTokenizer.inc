	$!{INSTANCE}addToken ($!{INTERFACE}PACKAGE, "package");
	$!{INSTANCE}addToken ($!{INTERFACE}MODULE, "module");
	$!{INSTANCE}addToken ($!{INTERFACE}SCALE, "scaleclass");
	$!{INSTANCE}addToken ($!{INTERFACE}CLASS, "class");
	$!{INSTANCE}addToken ($!{INTERFACE}INTERFACE, "interface");
	$!{INSTANCE}addToken ($!{INTERFACE}SINGLE_TYPE_IMPORT, "import");
	$!{INSTANCE}addToken ($!{INTERFACE}EXTENDS, "extends");
	$!{INSTANCE}addToken ($!{INTERFACE}IMPLEMENTS, "implements");
	$!{INSTANCE}addToken ($!{INTERFACE}SUPER, "super");
	$!{INSTANCE}addToken ($!{INTERFACE}THROWS, "throws");
	$!{INSTANCE}addToken ($!{INTERFACE}SEMI, ";");
	$!{INSTANCE}addToken ($!{INTERFACE}ASSIGN, "=");
	$!{INSTANCE}addToken ($!{INTERFACE}VOID_, "void");
	$!{INSTANCE}addToken ($!{INTERFACE}BOOLEAN_, "boolean");
	$!{INSTANCE}addToken ($!{INTERFACE}BYTE_, "byte");
	$!{INSTANCE}addToken ($!{INTERFACE}SHORT_, "short");
	$!{INSTANCE}addToken ($!{INTERFACE}CHAR_, "char");
	$!{INSTANCE}addToken ($!{INTERFACE}INT_, "int");
	$!{INSTANCE}addToken ($!{INTERFACE}LONG_, "long");
	$!{INSTANCE}addToken ($!{INTERFACE}FLOAT_, "float");
	$!{INSTANCE}addToken ($!{INTERFACE}DOUBLE_, "double");
	$!{INSTANCE}addToken ($!{INTERFACE}LT, "<");
	$!{INSTANCE}addToken ($!{INTERFACE}GT, ">");
	$!{INSTANCE}addToken ($!{INTERFACE}LINE, "---");
	$!{INSTANCE}addToken ($!{INTERFACE}LEFT_RIGHT_ARROW, "<->");
	$!{INSTANCE}addToken ($!{INTERFACE}PLUS_LEFT_ARROW, "<+");
	$!{INSTANCE}addToken ($!{INTERFACE}PLUS_ARROW, "+>");
	$!{INSTANCE}addToken ($!{INTERFACE}PLUS_LINE, "-+-");
	$!{INSTANCE}addToken ($!{INTERFACE}PLUS_LEFT_RIGHT_ARROW, "<+>");
	$!{INSTANCE}addToken ($!{INTERFACE}SLASH_LEFT_ARROW, "</");
	$!{INSTANCE}addToken ($!{INTERFACE}SLASH_ARROW, "/>");
	$!{INSTANCE}addToken ($!{INTERFACE}SLASH_LINE, "-/-");
	$!{INSTANCE}addToken ($!{INTERFACE}SLASH_LEFT_RIGHT_ARROW, "</>");
	$!{INSTANCE}addToken ($!{INTERFACE}CONTEXT, "(*");
	$!{INSTANCE}addToken ($!{INTERFACE}DOT, ".");
	$!{INSTANCE}addToken ($!{INTERFACE}SUB, "-");
	$!{INSTANCE}addToken ($!{INTERFACE}LEFT_ARROW, "<-");
	$!{INSTANCE}addToken ($!{INTERFACE}ARROW, "->");
	$!{INSTANCE}addToken ($!{INTERFACE}QUESTION, "?");
	$!{INSTANCE}addToken ($!{INTERFACE}MUL, "*");
	$!{INSTANCE}addToken ($!{INTERFACE}ADD, "+");
	$!{INSTANCE}addToken ($!{INTERFACE}RULE, "==>");
	$!{INSTANCE}addToken ($!{INTERFACE}DOUBLE_ARROW_RULE, "==>>");
	$!{INSTANCE}addToken ($!{INTERFACE}EXEC_RULE, "::>");
	$!{INSTANCE}addToken ($!{INTERFACE}COM, "~");
	$!{INSTANCE}addToken ($!{INTERFACE}NOT, "!");
	$!{INSTANCE}addToken ($!{INTERFACE}DIV, "/");
	$!{INSTANCE}addToken ($!{INTERFACE}REM, "%");
	$!{INSTANCE}addToken ($!{INTERFACE}POW, "**");
	$!{INSTANCE}addToken ($!{INTERFACE}SHL, "<<");
	$!{INSTANCE}addToken ($!{INTERFACE}SHR, ">>");
	$!{INSTANCE}addToken ($!{INTERFACE}USHR, ">>>");
	$!{INSTANCE}addToken ($!{INTERFACE}LE, "<=");
	$!{INSTANCE}addToken ($!{INTERFACE}GE, ">=");
	$!{INSTANCE}addToken ($!{INTERFACE}CMP, "<=>");
	$!{INSTANCE}addToken ($!{INTERFACE}NOT_EQUALS, "!=");
	$!{INSTANCE}addToken ($!{INTERFACE}EQUALS, "==");
	$!{INSTANCE}addToken ($!{INTERFACE}OR, "|");
	$!{INSTANCE}addToken ($!{INTERFACE}XOR, "^");
	$!{INSTANCE}addToken ($!{INTERFACE}AND, "&");
	$!{INSTANCE}addToken ($!{INTERFACE}COR, "||");
	$!{INSTANCE}addToken ($!{INTERFACE}CAND, "&&");
	$!{INSTANCE}addToken ($!{INTERFACE}THIS, "this");
	$!{INSTANCE}addToken ($!{INTERFACE}IF, "if");
	$!{INSTANCE}addToken ($!{INTERFACE}RETURN, "return");
	$!{INSTANCE}addToken ($!{INTERFACE}YIELD, "yield");
	$!{INSTANCE}addToken ($!{INTERFACE}THROW, "throw");
	$!{INSTANCE}addToken ($!{INTERFACE}SYNCHRONIZED_, "synchronized");
	$!{INSTANCE}addToken ($!{INTERFACE}ASSERT, "assert");
	$!{INSTANCE}addToken ($!{INTERFACE}BREAK, "break");
	$!{INSTANCE}addToken ($!{INTERFACE}CONTINUE, "continue");
	$!{INSTANCE}addToken ($!{INTERFACE}TRY, "try");
	$!{INSTANCE}addToken ($!{INTERFACE}CATCH, "catch");
	$!{INSTANCE}addToken ($!{INTERFACE}FINALLY, "finally");
	$!{INSTANCE}addToken ($!{INTERFACE}LCLIQUE, "{$!{_grogra_de_undefined}#");
	$!{INSTANCE}addToken ($!{INTERFACE}RCLIQUE, "$!{_grogra_de_undefined}#}");
	$!{INSTANCE}addToken ($!{INTERFACE}FOR, "for");
	$!{INSTANCE}addToken ($!{INTERFACE}WHILE, "while");
	$!{INSTANCE}addToken ($!{INTERFACE}DO, "do");
	$!{INSTANCE}addToken ($!{INTERFACE}SWITCH, "switch");
	$!{INSTANCE}addToken ($!{INTERFACE}CASE, "case");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFAULT, "default");
	$!{INSTANCE}addToken ($!{INTERFACE}NULL_LITERAL, "null");
	$!{INSTANCE}addToken ($!{INTERFACE}LONG_LEFT_ARROW, "<--");
	$!{INSTANCE}addToken ($!{INTERFACE}LONG_ARROW, "-->");
	$!{INSTANCE}addToken ($!{INTERFACE}LONG_LEFT_RIGHT_ARROW, "<-->");
	$!{INSTANCE}addToken ($!{INTERFACE}INSTANCEOF, "instanceof");
	$!{INSTANCE}addToken ($!{INTERFACE}QUOTE, "`");
	$!{INSTANCE}addToken ($!{INTERFACE}ADD_ASSIGN, "+=");
	$!{INSTANCE}addToken ($!{INTERFACE}SUB_ASSIGN, "-=");
	$!{INSTANCE}addToken ($!{INTERFACE}MUL_ASSIGN, "*=");
	$!{INSTANCE}addToken ($!{INTERFACE}DIV_ASSIGN, "/=");
	$!{INSTANCE}addToken ($!{INTERFACE}REM_ASSIGN, "%=");
	$!{INSTANCE}addToken ($!{INTERFACE}POW_ASSIGN, "**=");
	$!{INSTANCE}addToken ($!{INTERFACE}SHR_ASSIGN, ">>=");
	$!{INSTANCE}addToken ($!{INTERFACE}USHR_ASSIGN, ">>>=");
	$!{INSTANCE}addToken ($!{INTERFACE}SHL_ASSIGN, "<<=");
	$!{INSTANCE}addToken ($!{INTERFACE}AND_ASSIGN, "&=");
	$!{INSTANCE}addToken ($!{INTERFACE}XOR_ASSIGN, "^=");
	$!{INSTANCE}addToken ($!{INTERFACE}OR_ASSIGN, "|=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_ASSIGN, ":=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_RATE_ASSIGN, ":'=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_ADD, ":+=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_SUB, ":-=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_MUL, ":*=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_DIV, ":/=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_REM, ":%=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_POW, ":**=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_OR, ":|=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_AND, ":&=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_XOR, ":^=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_SHL, ":<<=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_SHR, ":>>=");
	$!{INSTANCE}addToken ($!{INTERFACE}DEFERRED_USHR, ":>>>=");
	$!{INSTANCE}addToken ($!{INTERFACE}INC, "++");
	$!{INSTANCE}addToken ($!{INTERFACE}DEC, "--");
	$!{INSTANCE}addToken ($!{INTERFACE}IN, "in");
	$!{INSTANCE}addToken ($!{INTERFACE}GUARD, "::");
	$!{INSTANCE}addToken ($!{INTERFACE}NEW, "new");
	$!{INSTANCE}addToken ($!{INTERFACE}ANNOTATION, "@");
	$!{INSTANCE}addToken ($!{INTERFACE}PRIVATE_, "private");
	$!{INSTANCE}addToken ($!{INTERFACE}PUBLIC_, "public");
	$!{INSTANCE}addToken ($!{INTERFACE}PROTECTED_, "protected");
	$!{INSTANCE}addToken ($!{INTERFACE}STATIC_, "static");
	$!{INSTANCE}addToken ($!{INTERFACE}TRANSIENT_, "transient");
	$!{INSTANCE}addToken ($!{INTERFACE}FINAL_, "final");
	$!{INSTANCE}addToken ($!{INTERFACE}ABSTRACT_, "abstract");
	$!{INSTANCE}addToken ($!{INTERFACE}NATIVE_, "native");
	$!{INSTANCE}addToken ($!{INTERFACE}VOLATILE_, "volatile");
	$!{INSTANCE}addToken ($!{INTERFACE}STRICT_, "strictfp");
	$!{INSTANCE}addToken ($!{INTERFACE}CONST_, "const");
	$!{INSTANCE}addToken ($!{INTERFACE}GLOBAL_, "global");
	$!{INSTANCE}addToken ($!{INTERFACE}VARARGS_, "...");
	$!{INSTANCE}addToken ($!{INTERFACE}ELSE, "else");
	$!{INSTANCE}addToken ($!{INTERFACE}LPAREN, "(");
	$!{INSTANCE}addToken ($!{INTERFACE}RPAREN, ")");
	$!{INSTANCE}addToken ($!{INTERFACE}LBRACK, "[");
	$!{INSTANCE}addToken ($!{INTERFACE}RBRACK, "]");
	$!{INSTANCE}addToken ($!{INTERFACE}LCURLY, "{");
	$!{INSTANCE}addToken ($!{INTERFACE}RCURLY, "}");
	$!{INSTANCE}addToken ($!{INTERFACE}COLON, ":");
	$!{INSTANCE}addToken ($!{INTERFACE}COMMA, ",");
	$!{INSTANCE}addToken ($!{INTERFACE}LAMBDA, "=>");
	$!{INSTANCE}addToken ($!{INTERFACE}RCONTEXT, "*)");
