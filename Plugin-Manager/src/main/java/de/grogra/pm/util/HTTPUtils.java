package de.grogra.pm.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.ServiceUnavailableRetryStrategy;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import de.grogra.pf.boot.Main;
import de.grogra.pm.GenericCallback;
import de.grogra.pm.http.HttpRetryStrategy;
import de.grogra.pm.repo.JSONSource;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class HTTPUtils {
	
    private static final int RETRY_COUNT = 1;
    private final static ServiceUnavailableRetryStrategy retryStrategy = new HttpRetryStrategy(RETRY_COUNT, 5000);
	private static final Logger log = Logger.getLogger(JSONSource.class.getName());


	public static boolean isGZIPResponse(HttpResponse result) {
        Header encoding = result.getFirstHeader("Content-Encoding");
        return encoding != null && "gzip".equals(encoding.getValue().toLowerCase());
    }

    public static String convertGZIPToString(byte[] bytes) throws IOException {
        GZIPInputStream gzipInputStream = new GZIPInputStream(new ByteArrayInputStream(bytes));
        InputStreamReader reader = new InputStreamReader(gzipInputStream);
        BufferedReader in = new BufferedReader(reader);

        final StringBuilder buffer = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            buffer.append(line);
        }

        return buffer.toString();
    }
    
    public static long copyLarge(InputStream input, OutputStream output, GenericCallback<Long> progressCallback) throws IOException {
        byte[] buffer = new byte[4096];
        long count = 0L;

        int n;
        for (; -1 != (n = input.read(buffer)); count += (long) n) {
            output.write(buffer, 0, n);
            progressCallback.notify(count);
        }

        return count;
    }


    public static HttpResponse execute(HttpUriRequest request, AbstractHttpClient client) throws IOException {
        return execute(request, null, client);
    }

    public static HttpResponse execute(HttpUriRequest request, HttpContext context, AbstractHttpClient client) 
    		throws IOException {
        for (int c = 1; ; c++) {
            HttpResponse response = client.execute(request, context);
            try {
                if (retryStrategy.retryRequest(response, c, context)) {
                    EntityUtils.consume(response.getEntity());
                    long nextInterval = retryStrategy.getRetryInterval();
                    try {
                        Thread.sleep(nextInterval);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        throw new InterruptedIOException();
                    }
                } else {
                    return response;
                }
            } catch (RuntimeException ex) {
                try {
                    EntityUtils.consume(response.getEntity());
                } catch (IOException ioex) {
                	//
                }
                throw ex;
            }
        }
    }
    
    public static AbstractHttpClient getHTTPClient() {
    	AbstractHttpClient client = new DefaultHttpClient();
        
    	boolean useProxy = false;
    	int proxyPort = -1;
    	String proxyHost = ProxyUtils.getOption(Main.HTTPS_PROXY_HOST, "");
        if (!proxyHost.isEmpty()) {
            proxyPort = Integer.parseInt(ProxyUtils.getOption(Main.HTTPS_PROXY_PORT, "-1"));
            
            useProxy = true;
        } else {
        	proxyHost = ProxyUtils.getOption(Main.HTTP_PROXY_HOST, "");
        	if (!proxyHost.isEmpty()) {
                proxyPort = Integer.parseInt(ProxyUtils.getOption(Main.HTTP_PROXY_PORT, "-1"));
                
                useProxy = true;
            }
        }
        if (useProxy) {
        	log.info("Using proxy " + proxyHost + ":" + proxyPort);
            HttpParams params = client.getParams();
            HttpHost proxy = new HttpHost(proxyHost, proxyPort);
            params.setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

            String proxyUser = ProxyUtils.getOption(Main.PROXY_USER, null);
            if (proxyUser != null) {
                log.info("Using authenticated proxy with username: " + proxyUser);
                String proxyPass = ProxyUtils.getOption(Main.PROXY_PASSWORD, null);

                String localHost;
                try {
                    localHost = InetAddress.getLocalHost().getCanonicalHostName();
                } catch (Throwable e) {
                	log.severe("Failed to get local host name, defaulting to 'localhost'"+ e);
                    localHost = "localhost";
                }

                AuthScope authscope = new AuthScope(proxyHost, proxyPort);
                String proxyDomain = ProxyUtils.getOption(Main.PROXY_DOMAIN, "");
                NTCredentials credentials = new NTCredentials(proxyUser, proxyPass, localHost, proxyDomain);
                client.getCredentialsProvider().setCredentials(authscope, credentials);
            }
        }
        client.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(RETRY_COUNT, true));
        return client;
    }
}
