package de.grogra.pm.util;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Workbench;
import de.grogra.pm.cache.CacheBase;
import de.grogra.pm.cache.PluginsCache;
import de.grogra.pm.cache.SourcesCache;
import de.grogra.util.Utils;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class CacheUtils {
	private static final Logger log = Logger.getLogger(CacheUtils.class.getName());
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
    private static final long CACHE_MAX_AGE = 60 * 60 * 1000 * 10; // default cache expire is 10 hour since now
	public static final int REPO = 0;
    public static final int PLUGIN = 1;
    
	public static boolean isUsingCache() {
    	Item opt = Item.resolveItem (Workbench.current().getRegistry().getRootRegistry(), "/pluginmanager/options");
		if (opt!=null) {
			return Utils.getBoolean (opt, "useCache", false);
		}
		else {
			return false;
		}
    }
    
	/**
	 * Return the directory use for cache of the type. 
	 * If the directory does not exists it is created.
	 * The type is either REPO or PLUGIN
	 * @param type
	 * @return
	 */
    public static File getCacheDir(int type) {
    	String optpath, optname, cachePath=null;
    	File confDir = de.grogra.pf.boot.Main.getConfigurationDirectory();
    	switch(type) {
    	case REPO:
    		optname="sourceCachePath";
			optpath="pluginmngr_cache_repo";
    		break;
    	case PLUGIN:
    		optname="pluginCachePath";
			optpath="pluginmngr_cache_plugin";
    		break;
		default:
			return null;
    	}
    	
    	Item opt = Item.resolveItem (Workbench.current(), "/pluginmanager/options");
		if (opt!=null) {
			cachePath = (String) Utils.get (opt, optname, 
					new File(confDir, optpath).toString());
		}
		if (cachePath.isEmpty()) {
			cachePath=new File(confDir, optpath).toString();
		}
		
		File cacheFolder = new File(cachePath);
		if (!cacheFolder.isDirectory()) {
			cacheFolder.delete();
		}
		if (!cacheFolder.exists()) {
			cacheFolder.mkdirs();
		}
		return cacheFolder;
    }
    
    /**
     * Delete content of both cache directories
     */
    public static void clearCacheRepo() throws IOException {
    	PluginEntryUtils.clearRepo( getCacheDir(REPO) );
    	PluginEntryUtils.clearRepo( getCacheDir(PLUGIN) );
    }

    /**
     * G
     * @param uri
     * @param type
     * @return
     */
    public static CacheBase getCache(URI uri, int type) {
        File file = generateCacheFile(uri, type);
        if (!file.exists()) {
            return null;
        }
        switch(type) {
        case REPO:
        	return SourcesCache.fromFile(file);
        case PLUGIN:
        	return PluginsCache.fromFile(file);  
        default:
        	return null;
        }
    }
    

    public static void cacheRepo(String repoJSON, HttpResponse response, URI uri) throws IOException {
        long maxAge = CACHE_MAX_AGE;
        long date = System.currentTimeMillis();
        long lastModified = System.currentTimeMillis();

        Header[] allHeaders = response.getAllHeaders();
        for (Header header : allHeaders) {
            if ("date".equals(header.getName().toLowerCase())) {
                date = parseDateHeader(header);
            } else if ("cache-control".equals(header.getName().toLowerCase())) {
                maxAge = parseCacheControlHeader(header);
            } else if ("last-modified".equals(header.getName().toLowerCase())) {
                lastModified = parseDateHeader(header);
            }
        }

        long expirationTime = date + maxAge;
        SourcesCache repo = new SourcesCache(repoJSON, expirationTime, lastModified);
        repo.saveToFile(generateCacheFile(uri, REPO), null);
    }

    public static File generateCacheFile(URI uri, int type) {
    	switch(type) {
    	case REPO:
            return new File(getCacheDir(type), generateFileName(uri));
    	case PLUGIN:
    		File dir = new File(getCacheDir(type), uri.toString());
    		if (!dir.isDirectory()) {
    			dir.delete();
    		}
    		if (!dir.exists()) {
    			dir.mkdirs();
    		}
            return new File(dir, generateFileName(uri));
        default:
        	return null; 
    	}
    }

    private static String generateFileName(URI uri) {
        return DigestUtils.md5Hex(System.getProperty("user.name") + getAddress(uri));
    }
    
    private static String getAddress(URI uri) {
    	return uri.toString().split("\\?")[0];
    }

    private static long parseCacheControlHeader(Header header) {
        return CACHE_MAX_AGE;
    }

    private static long parseDateHeader(Header header) {
        try {
            Date d = dateFormat.parse(header.getValue());
            return d.getTime();
        } catch (ParseException e) {
            log.warning("Cannot parse date header"+ e);
            return System.currentTimeMillis();
        }
    }
    
    private static void checkCacheValidity(URI uri, HttpResponse res, int type) {
        CacheBase repo = getCache(uri, type);
        if (repo != null) {
            Header hdr = res.getFirstHeader("last-modified");
            if (hdr != null) {
                if (!repo.isActual(parseDateHeader(hdr))) {
                    File fname = generateCacheFile(uri, type);
                    log.info("Cache file is not valid anymore, will drop it: " + fname.getAbsolutePath());
                    fname.deleteOnExit();
                }
            }
        }
    }

}
