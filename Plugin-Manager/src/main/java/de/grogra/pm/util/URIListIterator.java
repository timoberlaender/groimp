package de.grogra.pm.util;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class URIListIterator implements Iterator {

	private final String list;
	private String next;
	private int pos = 0;


	public URIListIterator (String list)
	{
		this.list = list;
	}


	public void remove ()
	{
		throw new UnsupportedOperationException ();
	}


	public boolean hasNext()
	{
		getNext ();
		return next != null;
	}


	public Object next () 
	{
		return nextURI ();
	}


	public Object nextURI () 
	{
		getNext ();
		String n = next;
		if (n == null)
		{
			throw new NoSuchElementException ();
		}
		next = null;
		try {
			return new URI (n);
		}
		catch (URISyntaxException e) {
			return n;
		}
	}
	
	/**
	 * Return a non uri path
	 * @return
	 */
	public Object nextPath () 
	{
		getNext ();
		String n = next;
		if (n == null)
		{
			throw new NoSuchElementException ();
		}
		next = null;
		return n;
	}


	private void getNext ()
	{
		while ((next == null) && (list != null) && (pos < list.length ()))
		{
			int p = list.indexOf (File.pathSeparatorChar, pos);
			if (p < 0)
			{
				p = list.length ();
			}
			if (list.length()>p+2 && list.charAt(p+1)=='/' && list.charAt(p+2)=='/') {
				p = list.indexOf (File.pathSeparatorChar, p+1);
				if (p < 0)
				{
					p = list.length ();
				}
			}
			String n = list.substring (pos, p).trim ();
			if (n.length () > 0)
			{
				next = n;
			}
			pos = p + 1;
		}
	}

}