package de.grogra.pm.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

import de.grogra.pf.registry.Exclude;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Library;
import de.grogra.pf.registry.PluginDescriptor;
import de.grogra.pf.registry.PluginPrerequisite;
import de.grogra.pf.registry.Registry;
import de.grogra.util.I18NBundle;
import de.grogra.util.XPropertyResourceBundle;
import de.grogra.vfs.LocalFileSystem;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
/*
 * A set of tools for creating repo sources (jsonarray) out of groimp objects.
 */
public class RepoSourceUtils {

	public static String getDefaultPluginIdFromName(PluginDescriptor pd) {
    	return pd.getPluginProvider()+"."+pd.getName().toLowerCase();
    }    
    
    
    public static JSONObject fromPluginDescriptor(PluginDescriptor pd) {
    	JSONObject plugin = new JSONObject();
    	
    	plugin.put("id", (String) pd.get("pluginId",  
    			getDefaultPluginIdFromName(pd)));
    	
    	plugin.put("name", pd.getPluginName());
    	
    	String description;
        try {
        	description = (String) pd.getDescription(de.grogra.util.Described.NAME);
        } catch (NullPointerException e) {description="";}
    	plugin.put("description",description);
    	
    	plugin.put("helpUrl","https://gitlab.com/grogra/groimp/");
    	
    	plugin.put("vendor", pd.getPluginProvider());
    	
//    	plugin.put("mainClass", "");
    	
    	plugin.put("versions", versionsJSONfromPluginDescriptor(pd));
    	
    	return plugin;
    }
    
    
    public static JSONObject versionsJSONfromPluginDescriptor(PluginDescriptor pd) {
    	JSONObject versions = new JSONObject();
    	JSONObject v = new JSONObject();
    	JSONObject libs = new JSONObject();
    	Item[] libraries = pd.findAll (new ItemCriterion (){
			@Override
			public boolean isFulfilled (Item item, Object info){
				return (item instanceof Library);}
			@Override
			public String getRootDirectory (){
				return null;}
		}, null, false);  
    	for (Object lib : libraries) {
    		int i=0;
    		for ( Map.Entry<String[], String[]> entry : ((Library)lib).getLibrary().entrySet() ) {
    			for (String l : entry.getKey()) {
    				if (!FilenameUtils.getBaseName( l).toLowerCase().equals(pd.getPluginName().toLowerCase())) {
						libs.put(l, ( ((Library)lib).getLibraryFiles() == null )? "embeeded":
								((Library)lib).getLibraryFiles()[i].toString());
    				}
    			}
    			i++;
    		}
    	}
    	
    	JSONObject deps = new JSONObject();
    	Item[] prerequises = pd.findAll (new ItemCriterion (){
			@Override
			public boolean isFulfilled (Item item, Object info){
				return (item instanceof PluginPrerequisite);}
			@Override
			public String getRootDirectory (){
				return null;}
		}, null, false);  
    	
    	for (Item pp : prerequises) {
    		deps.put(pp.getName(), getInstalledVersion((PluginPrerequisite)pp));
    	}
    	
    	List<String> excluded = new ArrayList<String>();
    	Item[] exclusions = pd.findAll (new ItemCriterion (){
			@Override
			public boolean isFulfilled (Item item, Object info){
				return (item instanceof Exclude);}
			@Override
			public String getRootDirectory (){
				return null;}
		}, null, false);  
    	
    	for (Item pe : exclusions) {
    		excluded.add(pe.getName());
    	}
    	
    	
    	v.put("downloadUrl", pd.getFileSystem().getRoot().toString());
    	v.put("libs", libs);
    	v.put("depends", deps);
    	v.put("excludes", excluded);
    	
    	versions.put(pd.getPluginVersion(), v);
    	return versions;
    }
    
    
    public static JSONObject fromPluginXML(File file) {
    	// Read the plugin.xml file and retrieve info
    	InputStream in =null;
    	PluginDescriptor pd=null;
    	try
		{
			in = new FileInputStream (file);
			pd = PluginDescriptor.read ("todelete",
				new BufferedInputStream (in), 
				new LocalFileSystem ("local", file.getParentFile()), 
				file.getParentFile());
		}
		catch (IOException e)
		{		}
		if (in != null)
		{
			try
			{
				in.close ();
			}
			catch (IOException e)
			{
				e.printStackTrace ();
			}
		}
		if (pd==null) {
			return null;
		}
		else {
			File[] a = file.getParentFile().listFiles ();
			Locale l = Locale.getDefault ();
			String[] s = {"plugin", l.getLanguage (), l.getCountry (),
						  l.getVariant ()};
			for (int i = 1; i < 4; i++)
			{
				if (s[i] != null)
				{
					s[i] = s[i - 1] + '_' + s[i];
				}
			}
			XPropertyResourceBundle[] p = new XPropertyResourceBundle[4];
			for (int f = 0; f < a.length; f++)
			{
				String n = a[f].getName ();
				if (n.endsWith (".properties"))
				{
					n = n.substring (0, n.length () - 11);
					for (int i = 0; i < 4; i++)
					{
						if (n.equals (s[i]))
						{
							in = null;
							try
							{
								in = new FileInputStream (a[f]);
								p[i] = new XPropertyResourceBundle (in);
							}
							catch (Exception e)
							{
								e.printStackTrace ();
							}
							if (in != null)
							{
								try
								{
									in.close ();
								}
								catch (IOException e)
								{
								}
							}
						}
					}
				}
			}
			XPropertyResourceBundle b = null;
			for (int i = 0; i < 4; i++)
			{
				if (p[i] != null)
				{
					p[i].setParent (b);
					b = p[i];
				}
			}
			pd.setI18NBundle (new I18NBundle (b, pd.getName ()));
		}
		pd.setPluginState(PluginDescriptor.ERROR);
		return fromPluginDescriptor(pd);
    }

    
    private static String getInstalledVersion(PluginPrerequisite prerequisite) {
    	try {
	    	Registry r = prerequisite.getRegistry().getRootRegistry();
	    	de.grogra.graph.impl.Node d = r.getPluginDirectory ().getBranchTail();
			while (d != null)
			{
				if (d instanceof PluginDescriptor 
						&& ((PluginDescriptor) d).getPluginState () != PluginDescriptor.ERROR 
						&& d.getName ().equals(prerequisite.getName()))
				{
					return ((PluginDescriptor) d).getPluginVersion();
				}
				d = d.getPredecessor ();
			}
    	}catch (NullPointerException e) {
    		
    	}
		return "";
    }
    
    /**
     * Format a line into a uri.
     * Return null if if the line isn't correct
     */
    public static URI formatAddress(String line) throws URISyntaxException{
    	if (line == null || line.isEmpty()) {
    		return null;
    	}
    	if (line.startsWith("http")) {
    		return new URI(line);
    	}
    	if(line.startsWith("~/")) {
    		line=line.replaceFirst("~/", System.getProperty("user.home")+"/");
    	}
    	return new URI(line);
    }
    
    
    /**
     * Check if the repository is active from the URI. The repository should be deactivated if 
     * it starts by "!"
     * @param uri
     * @return
     */
    public static boolean isActiveFromString(String uri) {
    	return !uri.startsWith("!");
    }
    
    public static String removeActiveInURI(URI uri) {
    	String res = uri.toString();
    	if (res.startsWith("!")) {
    		res = res.substring(1);
    	}
    	return res;
    }
    
    public static String removeActiveInString(String uri) {
    	String res = uri.toString();
    	if (res.startsWith("!")) {
    		res = res.substring(1);
    	}
    	return res;
    }
    
    /**
     * This checks if the list of paths contains the path. Regardless of their "activeness" (usually a "!").
     * @param list
     * @param path
     * @return
     */
    public static boolean listOfRepoContains(List<String> list, String path) {
    	for (String p : list) {
    		if (removeActiveInString(p).equals( removeActiveInString(path) ) ) {
    			return true;
    		}
    	}
    	return false;
    }
}
