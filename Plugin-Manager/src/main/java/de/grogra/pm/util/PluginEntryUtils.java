package de.grogra.pm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;

import de.grogra.pf.ui.Workbench;
import de.grogra.pm.cache.PluginsCache;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
/*
 * A set of tools for managing plugin entries.
 */
public class PluginEntryUtils {

	static final Pattern libNameParser = Pattern.compile("([^=<>]+)([=<>]+[0-9.a-zA-Z]+)?");

	
	/**
     * Check if the plugin located at file param is a working groimp plugin:
     * it has a PluginName.jar and plugin.xml file
     * @return
     * @throws IOException 
     */
    public static boolean checkPlugin(File parent, PluginsCache repo) throws IOException {
    	// 2 possible cases: 1) the repo is /parent/plugin.jar /plugin.xml ...
    	// 2) /parent/target/classes/ .. /parent/target/classes/plugin.xml, ...
    	String simpleName = repo.getPluginName().split("-")[0];
    	File plugin;
    	File classes;
    	File target = new File(parent, "target");
    	if (target.exists() && target.isDirectory()) {
    		classes = new File(target, "classes");
    		plugin = new File(classes, "plugin.xml");
    	}
    	else {
    		plugin = new File(parent, "plugin.xml");
        	classes = new File(parent, simpleName.toLowerCase()+".jar");
    	}
		if (!classes.exists() || !plugin.exists()) {
			return false;
		}
		return true;
    }
    
    /**
     * Move the plugin entry described by the download result to the file directory
     * @param file
     * @param dwn
     * @param copy Should the src be copied of moved 
     */
    public static void movePlugin(File dst, File src, boolean copy) {
		try {
			createParentIfNotExist(src);
			createParentIfNotExist(dst);
			// Is the src a jar file?
        	if (isZip(src)) {
        		// move the jar + extract plugin.xml & plugin.properties
        		UnzipUtility unzipper = new UnzipUtility();
        		unzipper.extractPluginFiles(src.toString(), dst.toString());
        		String pname = extractPluginName(dst);
        		String pluginName = pname.toLowerCase();
//        				pname.isEmpty()? src.getName().toLowerCase().split("-")[0] : pname;
        		File jar = new File(dst, pluginName.toLowerCase() +".jar");

        		if (copy) {
        			Files.copy(src.toPath(), jar.toPath(), StandardCopyOption.REPLACE_EXISTING);
        		}
        		else {
        			Files.move(src.toPath(), jar.toPath(), StandardCopyOption.REPLACE_EXISTING);
        		}
        	}
        	else { 
        		if (src.isDirectory()) {
        			// Is the src a compiled directory ? (aka eclipse directories) 
        			if (src.getName().equals("target")) {
        				dst = new File(dst, "target");
        				createParentIfNotExist(dst);
        			}
        			// Else it is a cached directory
        			if (copy) {
        				FileUtils.copyDirectory(src, dst);
            		}
            		else {
            			moveDirectoryContent(src, dst);
//            			System.out.println("Delete : "+ Paths.get(dst.getParent(), src.getName()).toString());
            			Files.delete(Paths.get(dst.getParent(), src.getName()));
            		}
        		}
        		else { 
        			if (copy) {
        				FileUtils.copyDirectory(src.getParentFile(), dst);
            		}
            		else {
            			moveDirectoryContent(src.getParentFile(), dst);
//            			System.out.println("Delete : "+ Paths.get(dst.getParent(), src.getName()).toString());
            			Files.delete(Paths.get(dst.toString(), src.getName()));
            		}
        		}
        	}
        } catch (NoSuchFileException e) {
            System.out.println("Remote libs are downloaded and moved, but not from cache..." + e);
    	} catch (Exception e) {
            System.out.println("Cannot move " + src + " because of " + e.toString());
            e.printStackTrace(System.out);
        }
    }
    
    
    public static void moveDirectoryContent(File src, File dst) {
    	List<Path> allFiles=new ArrayList<Path>();
    	try (Stream<Path> stream = Files.walk(src.toPath(), Integer.MAX_VALUE)) {
    		allFiles = stream
    				.filter(Files::isRegularFile)
    				.collect(Collectors.toList());    				
        } catch (IOException e) {
			e.printStackTrace();
		}
    	for (Path path : allFiles) {
    		try {
    			Path result = Paths.get(dst.toString(), src.toPath().relativize(path).toString());
				createParentIfNotExist(result.toFile());
				Files.move(path, result, 
						StandardCopyOption.REPLACE_EXISTING);
    		} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    	// once every files have been moved, the directories are deleted
    	try {
    		Files.walk(src.toPath())
    	      .sorted(Comparator.reverseOrder())
    	      .map(Path::toFile)
    	      .forEach(File::delete);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    
    private static void createParentIfNotExist(File file) throws IOException {
    	if (file.isDirectory()){
    		Files.createDirectories(file.toPath());
    	}else {
    		Files.createDirectories(file.getParentFile().toPath());
    	}
    }
        
    /**
     * Read the plugin property file to get the pluginName
     * @param parent
     * @return
     * @throws IOException
     */
    private static String extractPluginName(File parent) throws IOException {
    	File pluginproperties = new File(parent, "plugin.properties");
    	if (!pluginproperties.exists()) {return "";}
    	java.util.Properties prop = new java.util.Properties();
    	prop.load(new FileInputStream(pluginproperties));
    	return prop.getProperty("pluginName");
    }
    
    
    /** 
     * test if the File is a type of zip, jar, ...
     * @param f
     * @return
     */
    public static boolean isZip(File f) {
        int fileSignature = 0;
        try (RandomAccessFile raf = new RandomAccessFile(f, "r")) {
            fileSignature = raf.readInt();
        } catch (IOException e) {
            // handle if you like
        }
        return fileSignature == 0x504B0304 || fileSignature == 0x504B0506 || fileSignature == 0x504B0708;
    }
    
    /**
     * Delete all contents of a directory ONLY IF the directory is under the groimp config directory
     * @throws IOException 
     */
    public static void clearRepo(File directory) throws IOException {
    	if (!isConfigChildren(directory)) {
    		Workbench.current().logGUIInfo("Cannot delete content outside of GroIMP configuration directory");;
    		return;
    	}
    	FileUtils.cleanDirectory(directory);
    }
    
    private static boolean isConfigChildren(File dir) {
    	File parent = dir.getParentFile();
    	File confDir = de.grogra.pf.boot.Main.getConfigurationDirectory();
    	while (parent != null) {
    		if (parent.equals(confDir)) 
    			return true;
    	}
    	return false;
    }
    
}
