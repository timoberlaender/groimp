package de.grogra.pm.repo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.grogra.pf.boot.Main;
import de.grogra.pm.util.HTTPUtils;
import de.grogra.pm.util.RepoSourceUtils;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
/*
 * A source repository that includes one or more sources. Usually a ".list" file with addresses. 
 */
public class ListSource extends RepoSource {

	private static final Logger log = Logger.getLogger(JSONSource.class.getName());
    private int timeout = Integer.parseInt(Main.getProperty(Main.PLUGIN_MANAGER_CACHE_TIMEOUT, "30000"));
	private List<RepoSource> repositories=new ArrayList<RepoSource>();
	
	public ListSource(URI listFile) {
		this.file=listFile;
	}
	
	@Override
	public JSONArray getRepo(Object c) throws IOException {
		//TODO: add cache
		String[] addresses = getAddressesList(file, 
				(c instanceof AbstractHttpClient) ? (AbstractHttpClient) c : null);
		
		for (String address : addresses) {
			URI repo;
			try {
				repo = new URI(address);
				File f = new File(repo.toString());
				boolean toActivate=RepoSourceUtils.isActiveFromString(repo.toString());
				if (!repo.equals(file)){
					if (f.getName().endsWith(".list")) {
						repositories.add(new ListSource(repo).setActive(toActivate));
			    	}
					else if (f.isDirectory() && f.exists()) {
						repositories.add( new DirectorySource(repo).setActive(toActivate));
					} else {
						repositories.add( new JSONSource(repo).setActive(toActivate));
					}
				}
			} catch (URISyntaxException e) {
				log.warning("Repository " + address + " couldn't be read : " + e);
			}
		}
		
		final JSONArray result = new JSONArray();
        final List<String> pluginsIDs = new ArrayList<>();

        for (RepoSource source : repositories) {
        	try {
        	JSONArray json = source.getRepo(c);
            for (Object elm : json) {
                // resolve plugin-id conflicts //TODO: add versions if exists from 2 json
                String id = ((JSONObject) elm).getString("id");
                if (!pluginsIDs.contains(id)) {
                    pluginsIDs.add(id);
                    result.put(elm);
                } 
            }
        	}catch (JSONException e) {
        		
        	}
        }
        return result;
	}

	@Override
	public void setTimeout(int timeout) {
	}
	
	protected String[] getAddressesList(URI uri, AbstractHttpClient client) {
    	try {
    	if(new File(uri.toString()). isFile()) {
				return getLocalAddressesList(uri);
			} 
    	return getRemoteAddressesList(uri, client);
    	}
    	catch (IOException e) {
			e.printStackTrace();
		}
    	return null;
    }
    
        
    protected String[] getLocalAddressesList(URI path) throws IOException {
    	return loadAddressesFromRepo(new FileInputStream(new File(path.toString())));
    }
    
    protected String[] getRemoteAddressesList(URI uri, AbstractHttpClient client) throws IOException {
    	HttpRequestBase get = new HttpGet(uri);
        HttpParams requestParams = get.getParams();
        get.setHeader("Accept-Encoding", "gzip");
        requestParams.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, timeout);
        requestParams.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout);

        HttpResponse result = HTTPUtils. execute(get, client);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HttpEntity entity = result.getEntity();
        try {
            entity.writeTo(bos);
            byte[] bytes = bos.toByteArray();
            if (bytes == null) {
                bytes = "null".getBytes();
            }

            String response = HTTPUtils.isGZIPResponse(result) ? HTTPUtils.convertGZIPToString(bytes) : new String(bytes);
            int statusCode = result.getStatusLine().getStatusCode();
            if (statusCode >= 300) {
            	log.warning("Response with code " + result + ": " + response);
                throw new IOException("Repository responded with wrong status code: " + statusCode);
            } else {
            	log.info("Response with code " + result + ": " + "\n" + response);
            }

            return loadAddressesFromRepo( bos.toInputStream());
        } finally {
            get.abort();
            try {
                entity.getContent().close();
            } catch (IOException | IllegalStateException e) {
                log.warning("Exception in finalizing request"+ e);
            }
        }
    }
    
    protected String[] loadAddressesFromRepo(InputStream is) throws IOException {
    	List<String> addressesList = new ArrayList<>();
    	BufferedReader reader;
    	reader = new BufferedReader(new InputStreamReader(is));
    	String line = reader.readLine();
    	URI address;
    	while (line !=null) {
    		try {
				address=RepoSourceUtils.formatAddress(line);
    		if (address!=null) {
        		addressesList.add(address.toString());
    		}
    		} catch (URISyntaxException e) {
				log.warning("Failed to load: "+line);
			}
    		line = reader.readLine();
    	}
    	
    	reader.close();
    	is.close();
    	
    	return addressesList.toArray(new String[0]);
    }
    
    public boolean accept(URI uri) {
    	return uri.toString().endsWith(".list");
    }
    
}
