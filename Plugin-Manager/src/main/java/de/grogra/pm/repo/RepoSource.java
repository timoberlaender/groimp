package de.grogra.pm.repo;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;

import de.grogra.pm.util.PluginEntryUtils;
import de.grogra.util.ProgressMonitor;
import de.grogra.util.Utils;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
abstract public class RepoSource implements Cloneable {
	
	protected URI file;
	protected boolean active=true;
	
	private boolean autoProgress = true;
	private String progressText;
	float AUTO_PROGRESS = -2;
	private float progressValue = AUTO_PROGRESS;
	private float progressResolution = 0.01f;
	private ProgressMonitor monitor;
	
	/**
	 * returns an jsonarray that includes all information of plugin in the repo. 
	 * See PluginEntry to have the list of metadata suported
	 */
    public abstract JSONArray getRepo(Object connexion) throws IOException;

    public abstract void setTimeout(int timeout);

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    public class DownloadResult {
        private final String tmpFile;
        private final String filename;

        public DownloadResult(String tmpFile, String filename) {
            this.tmpFile = tmpFile;
            this.filename = filename;
        }

        public String getTmpFile() {
            return tmpFile;
        }

        public String getFilename() {
            return filename;
        }
        
        public String getName() {
        	File tmp = new File (tmpFile);
        	if (PluginEntryUtils.isZip(tmp))
        		return FilenameUtils.getBaseName(filename).toLowerCase();
        	else return FilenameUtils.getName(filename).toLowerCase();
        }
    }
    
    public URI getFile() {
    	return file; 
    }
    
    /*
     * By default a source is active. It can be deactivated by the user
     */
    public boolean isActive() {
    	return active;
    }
    
    public RepoSource setActive(boolean active) {
    	this.active=active;
    	return this;
    }
    
    /**
     * Does this type of repo accept this type of URI?
     * @param uri
     * @return
     */
    abstract public boolean accept(URI uri);
    
    
    /*
     * ---------------------------------------
     * Monitor repository loading
     * ---------------------------------------
     */
    public void initProgressMonitor (ProgressMonitor monitor)
	{
		this.monitor = monitor;
	}


	protected final boolean useAutoProgress ()
	{
		return autoProgress;
	}


	public void setProgress (String text, float progress)
	{
		if (progress != AUTO_PROGRESS)
		{
			autoProgress = false;
			setProgress0 (text, progress);
		}
		else
		{
			setProgress0 (text, (progressValue == AUTO_PROGRESS)
						  ? ProgressMonitor.INDETERMINATE_PROGRESS
						  : progressValue);
		}
	}


	protected void setProgress0 (String text, float progress)
	{
		if (text == null)
		{
			text = progressText;
		}
		if (Utils.equal (text, progressText)
			&& (Math.abs (progress - progressValue) < progressResolution))
		{
			return;
		}
		progressText = text;
		progressValue = progress;
		setProgressImpl (text, progress);
	}


	protected void setProgressImpl (String text, float progress)
	{
		if (monitor != null)
		{
			monitor.setProgress (text, progress);
		}
	}
}
