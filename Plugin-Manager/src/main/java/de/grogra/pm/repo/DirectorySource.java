package de.grogra.pm.repo;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.json.JSONArray;
import org.json.JSONObject;

import de.grogra.pm.util.RepoSourceUtils;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class DirectorySource extends RepoSource {

    public DirectorySource(URI file) {
    	if (file == null) {
    		this.file=file;
    	}
    	else {
    		File f = new File(file.toString());
    		if (!f.exists()) {
    			this.file=file;
    		}
    		else {
            	if (f.isDirectory() ) {
            		this.file = file;
            	}
            	else {
            		this.file=file.resolve("..");
            	}
    		}
    	}
    }

    /**
     * Read the directory and subdirectories to look for plugin.xml files 
     * From these files plugin information is gathered and put into a JSONArray
     */
    @Override
    public JSONArray getRepo(Object connexion) throws IOException {
    	JSONArray result = new JSONArray();
    	if (file!=null) {
			//TODO: before testing the plugin.xml, look for plugin.json -
	    	Collection<File> pluginFiles = FileUtils.listFiles (new File(file.toString()), new TrueFileFilter() {
			    public boolean accept(File file) {
			        return file.getName().equals("plugin.xml");
			    }
			}, TrueFileFilter.INSTANCE );
	    	
	    	for (File f : pluginFiles) {
	    		try {
	    			JSONObject plugin = RepoSourceUtils.fromPluginXML(f);
	    			result.put(plugin);
	    		}catch(NullPointerException e) {
	    			// 
	    		}
	        }
		}
    	
        return result;
    }

    @Override
    public void setTimeout(int timeout) {
    }

	
    public boolean accept(URI uri) {
    	File f = new File(uri.toString());
    	return (f.isDirectory() && f.exists());
    }
}
