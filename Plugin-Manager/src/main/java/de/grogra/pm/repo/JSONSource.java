package de.grogra.pm.repo;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.logging.Logger;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;

import de.grogra.pf.boot.Main;
import de.grogra.pm.cache.SourcesCache;
import de.grogra.pm.util.CacheUtils;
import de.grogra.pm.util.HTTPUtils;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class JSONSource extends RepoSource {
	private static final Logger log = Logger.getLogger(JSONSource.class.getName());
    private int timeout = Integer.parseInt(Main.getProperty(Main.PLUGIN_MANAGER_CACHE_TIMEOUT, "30000"));

    public JSONSource(URI jsonFile) {
      this.file = jsonFile;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        RepoSource clone = (RepoSource) super.clone();
        return clone;
    }

    @Override
    public JSONArray getRepo(Object connexion) throws IOException {
        String path = "?installID=" + getInstallID();
        URI address = file;
        
        SourcesCache repo=null;
        if (CacheUtils.isUsingCache()) {
			try {
				repo = (SourcesCache) CacheUtils.getCache(new URI(address.toString()+ path), CacheUtils.REPO);
			} catch (URISyntaxException e) {
				log.warning("Error looking for cache of: "+ address.toString()+ path);
			}
        }
        if (repo != null && repo.isActual() && repo.getCachedObject()!=null) {
        	log.info("Found cached repo");
            return new JSONArray(repo.getCachedObject());
        } else {
        	File tmpf = new File(address.toString());
            if (tmpf.isFile()) {  // if address is a local file : open it
            	return getLocalRepo(tmpf);
            }
            else {  // otherwise look for it with internet
            	try {
                	return getJSON(new URI(address.toString()+path), (AbstractHttpClient)connexion);
            	}
            	catch (IOException e) {
            		log.warning(" Repository at : "+ address + " could not have been reached - " + e);
            	}
            	catch (java.lang.IllegalStateException e) {
            		log.warning("Repository at : "+ address + " could not have been reached - " + e);
            	}
            	catch (org.json.JSONException e) {
            		log.warning("Error reading json: "+ address +" "+ e);
            	} catch (URISyntaxException e) {
            		log.warning(" Repository at : "+ address + " could not have been reached - " + e);
				}
            }
        }
        return new JSONArray();
    }
    

    protected JSONArray getJSON(URI uri, AbstractHttpClient client) throws IOException {
        HttpRequestBase get = new HttpGet(uri);
        HttpParams requestParams = get.getParams();
        get.setHeader("Accept-Encoding", "gzip");
        requestParams.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, timeout);
        requestParams.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout);

        HttpResponse result = HTTPUtils. execute(get, client);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HttpEntity entity = result.getEntity();
        try {
            entity.writeTo(bos);
            byte[] bytes = bos.toByteArray();
            if (bytes == null) {
                bytes = "null".getBytes();
            }

            String response = HTTPUtils.isGZIPResponse(result) ? 
            		HTTPUtils.convertGZIPToString(bytes) : new String(bytes);
            int statusCode = result.getStatusLine().getStatusCode();
            if (statusCode >= 300) {
            	log.warning("Response with code " + result + ": " + response);
                throw new IOException("Repository responded with wrong status code: " + statusCode);
            } else {
            	log.info("Repo at : "+ uri.toString() + " successfully loaded");
            }
            
            if (CacheUtils.isUsingCache()) {
            	CacheUtils.cacheRepo(response, result, uri);
            }
            return new JSONArray (response);
        } finally {
            get.abort();
            try {
                entity.getContent().close();
            } catch (IOException | IllegalStateException e) {
                log.warning("Exception in finalizing request"+ e);
            }
        }
    }

    
    public JSONArray getLocalRepo(File jsonFile) throws IOException {
        return new JSONArray(FileUtils.readFileToString(jsonFile));
    }


    /**
     * This function makes sure anonymous identifier sent
     *
     * @return unique ID for installation
     */
    public String getInstallID() {
        StringBuilder str = new StringBuilder();
        str.append(getClass().getProtectionDomain().getCodeSource().getLocation().getFile());
        try {
            str.append("\t").append(InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
        	log.warning("Cannot get local host name"+ e);
        }

        try {
            Enumeration<NetworkInterface> ifs = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface netint : Collections.list(ifs)) {
                str.append("\t").append(Arrays.toString(netint.getHardwareAddress()));
            }
        } catch (SocketException e) {
        	log.warning("Failed to get network addresses"+ e);
        }

        return getPlatformName() + '-' + DigestUtils.md5Hex(str.toString()) + '-' + getGuiMode();
    }

    private String getGuiMode() {
        return (Main.getProperty (Main.HEADLESS) != null) ? "nongui" : "gui";
    }

    protected String getPlatformName() {
        if (containsEnvironment("JENKINS_HOME")) {
            return "jenkins";
        } else if (containsEnvironment("TRAVIS")) {
            return "travis";
        } else if (containsEnvironmentPrefix("bamboo")) {
            return "bamboo";
        } else if (containsEnvironment("TEAMCITY_VERSION")) {
            return "teamcity";
        } else if (containsEnvironment("DOCKER_HOST")) {
            return "docker";
        } else if (containsEnvironmentPrefix("AWS_")) {
            return "amazon";
        } else if (containsEnvironment("GOOGLE_APPLICATION_CREDENTIALS") || containsEnvironment("CLOUDSDK_CONFIG")) {
            return "google_cloud";
        } else if (containsEnvironment("WEBJOBS_NAME")) {
            return "azure";
        } else {
            return getOSName();
        }
    }

    private boolean containsEnvironment(String key) {
        return System.getenv().containsKey(key);
    }

    private boolean containsEnvironmentPrefix(String prefix) {
        for (String key : System.getenv().keySet()) {
            if (key.toLowerCase().startsWith(prefix.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    private String getOSName() {
        return System.getProperty("os.name").toLowerCase().replace(' ', '_');
    }

    @Override
    public void setTimeout(int timeout) {
    }

    
    
    public boolean accept(URI uri) {
    	File f = new File(uri.toString());
    	if (f.getName().endsWith(".list")) {
    		return false;
    	}
    	if (f.isDirectory() && f.exists()) {
    		return false;
    	}
    	return true;
    }

}
