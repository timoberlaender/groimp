package de.grogra.pm.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
//import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
//import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

import de.grogra.pf.boot.Main;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Option;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.ComponentWrapper;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.swing.PanelSupport;
import de.grogra.pf.ui.swing.SwingPanel;
import de.grogra.pf.ui.swing.WindowSupport;
import de.grogra.pf.ui.util.ComponentWrapperImpl;
import de.grogra.pm.DependencyResolver;
import de.grogra.pm.GenericCallback;
import de.grogra.pm.PluginEntry;
import de.grogra.pm.PluginManager;
import de.grogra.pm.PluginManagerContext;
import de.grogra.pm.exception.DownloadException;
import de.grogra.pm.exception.PluginException;
import de.grogra.pm.gui.PluginCheckbox.StateListener;
import de.grogra.pm.repo.RepoManager;
import de.grogra.pm.repo.RepoSource;
import de.grogra.pm.util.CacheUtils;
import de.grogra.pm.util.RepoSourceUtils;
import de.grogra.util.I18NBundle;
import de.grogra.util.Map;
import de.grogra.util.Utils;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class PluginManagerDialog extends PanelSupport implements ActionListener, 
	ComponentListener, HyperlinkListener, PluginManagerContext {
    /**
     *
     */
	
	public static final I18NBundle I18N = I18NBundle.getInstance(PluginManagerDialog.class);
	
    public static final Border SPACING = BorderFactory.createEmptyBorder(5, 5, 5, 5);
    private final PluginManager manager;
//    private static PluginManager mgr=null;
    private final JTextPane modifs = new JTextPane();
    private final JButton apply = new JButton("Apply Changes and Restart GroIMP");
    private final PluginsList installed;
    private final PluginsList available;
    private final PluginUpgradesList upgrades;
    private final JPanel repositories;
    private final JSplitPane topAndDown = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    private final JLabel statusLabel = new JLabel("");
    private final JEditorPane failureLabel = new JEditorPane();
    private final JScrollPane failureScrollPane = new JScrollPane(failureLabel);
    private final StateListener cbNotifier;
    private final StateListener cbUpgradeNotifier;
    private ComponentWrapper table;
    
    public static final String RELOAD="Reload";
    
    GenericCallback<Object> lazyRefresh = new GenericCallback<Object>() {
		@Override
		public void notify(Object ignored) {
			if (ignored instanceof String) { // This has to be the ugliest way to handle it. - the dialog need to refresh 
				if (((String) ignored).contentEquals(RELOAD)) {
					loadPlugins();
				}
				else {
					modifs.setText((String)ignored);
				}
			}
			else {
				String changeText;
				if (ignored instanceof DependencyResolver) {
					try {
					changeText = manager.getChangesAsText((DependencyResolver)ignored );
					modifs.setText(changeText);
					apply.setEnabled(!changeText.isEmpty() && installed.isEnabled());
					}
					catch (PluginException ex) {
					changeText = "Cannot install plugin dependency failing : " + ex.getMessage();
					modifs.setText(changeText);
					}
				}
				else {
					try {
					changeText = manager.getChangesAsText(false);
					modifs.setText(changeText);
					apply.setEnabled(!changeText.isEmpty() && installed.isEnabled());
					}
					catch (PluginException ex) {
					changeText = "Cannot install plugin dependency failing : " + ex.getMessage();
					modifs.setText(changeText);
					}
				}
			}
		}
	};

    public PluginManagerDialog(PluginManager aManager) {
    	//pf
//    	super(); // call if extends panelDecorator - aka pf
//    	initPanel(this);
    	//java swing
//      super((JFrame) null, "GroIMP Plugins Manager", true); //default call - not from GI

    	//pf swing
    	// The content should not be null, it should be the disposable created - checkboxes and list
    	super (new SwingPanel (null)); // call if extends PanelSupport -aka pf.swing
		Container c = ((SwingPanel) getComponent ()).getContentPane ();
		
        c.setLayout(new BorderLayout());
        c.addComponentListener(this);
        manager = aManager;
        Dimension size = new Dimension(1024, 768);
        c.setSize(size);
        c.setPreferredSize(size);

        failureLabel.setContentType("text/html");
        failureLabel.addHyperlinkListener(this);

        final GenericCallback<Object> statusRefresh = new GenericCallback<Object>() {
            @Override
            public void notify(Object ignored) {
            	String changeText;
                try {
                	changeText = manager.getChangesAsText();
                	modifs.setText(changeText);
                    apply.setEnabled(!changeText.isEmpty() && installed.isEnabled());
                }
                catch (PluginException ex) {
                	changeText = "Cannot install plugin dependency failing : " + ex.getMessage();
                	modifs.setText(changeText);
                }
                
            }
        };

        cbNotifier = new StateListener() {
            @Override
            public void stateChanged(EventObject e) {
                if (e.getSource() instanceof PluginCheckbox) {
                    PluginCheckbox checkbox = (PluginCheckbox) e.getSource();
                    PluginEntry plugin = checkbox.getPlugin();
                    manager.toggleInstalled(plugin, checkbox.getPluginState());
                    statusRefresh.notify(this);
                }
            }
        };

        cbUpgradeNotifier = new StateListener() {
            @Override
            public void stateChanged(EventObject e) {
                if (e.getSource() instanceof PluginCheckbox) {
                    PluginCheckbox checkbox = (PluginCheckbox) e.getSource();
                    PluginEntry plugin = checkbox.getPlugin();
                    if (checkbox.getState()==PluginCheckbox.SELECTED) {
                        plugin.setCandidateVersion(checkbox.getPlugin().getMaxVersion());
                        checkbox.setSelectedVersion(checkbox.getPlugin().getMaxVersion());
                    } else {
                        plugin.setCandidateVersion(checkbox.getPlugin().getInstalledVersion());
                        checkbox.setSelectedVersion(checkbox.getPlugin().getInstalledVersion());
                    }
                    statusRefresh.notify(this);
                }
            }
        };

        installed = new PluginsList(statusRefresh, true);
        available = new PluginsList(statusRefresh, false);
        upgrades = new PluginUpgradesList(statusRefresh, false);
        repositories = (JPanel) makeRepositoryPanel(statusRefresh);
        // locally existing (cache?)

        topAndDown.setResizeWeight(.75);
        topAndDown.setDividerSize(5);
        topAndDown.setTopComponent(getTabsPanel());

        topAndDown.setBottomComponent(getBottomPanel());
        c.add(topAndDown, BorderLayout.CENTER);
        statusRefresh.notify(this); // to reflect upgrades
        manager.changeCallBack(lazyRefresh);
    }
    
    private void initPlugins() {
    	if (manager.hasPlugins()) {
            setPlugins();
        } else {
            loadPlugins();
        }
    }

    private void setPlugins() {
        installed.setPlugins(manager.getInstalledPlugins(), cbNotifier);
        available.setPlugins(manager.getAvailablePlugins(), cbNotifier);
        upgrades.setPlugins(manager.getUpgradablePlugins(), cbUpgradeNotifier);
    }

    private void loadPlugins() {
    	Container c = ((SwingPanel) getComponent ()).getContentPane ();
        if (!manager.hasPlugins()) {
            try {
                manager.load();
                setPlugins();
            } catch (Throwable e) {
                Main.getLogger().warning("Failed to load plugins manager "+ e);
                ByteArrayOutputStream text = new ByteArrayOutputStream(4096);
                e.printStackTrace(new PrintStream(text));
                String msg = "<p>Failed to download plugins repository.<br/>";
                msg += "One of the possible reasons is that you have proxy requirement for Internet connection.</p>" +
                        " Please read the instructions on this page: " +
                        "<a href=\"https://www.gitlab.com/grogra/groimp/PluginsManagerNetworkConfiguration/\">" +
                        "https://www.gitlab.com/grogra/groimp/PluginsManagerNetworkConfiguration/</a>" +
                        " <br><br>Error's technical details: <pre>" + text.toString() + "</pre><br>";
                failureLabel.setText("<html>" + msg + "</html>");
                failureLabel.setEditable(false);
                c.add(failureScrollPane, BorderLayout.CENTER);
                failureLabel.setCaretPosition(0);
            }
        }
    }

    private Component getTabsPanel() {
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Installed Plugins", installed);
        tabbedPane.addTab("Available Plugins", available);
        tabbedPane.addTab("Upgrades", upgrades);
        tabbedPane.addTab("Repositories", repositories);
        return tabbedPane;
    }

    private JPanel getBottomPanel() {
		Container c = ((SwingPanel) getComponent ()).getContentPane ();
		apply.setEnabled(false);
		modifs.setEditable(false);
		statusLabel.setFont(statusLabel.getFont().deriveFont(Font.ITALIC));
		
		JPanel panel = new JPanel(new BorderLayout());
		
		JPanel modifsPanel = new JPanel(new BorderLayout());
		modifsPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, c.getHeight() / 3));
		modifsPanel.setPreferredSize(new Dimension(c.getWidth(), c.getHeight() / 3));
		modifsPanel.setBorder(SPACING);
		modifsPanel.setBorder(BorderFactory.createTitledBorder("Review Changes"));
		
		modifs.setEditable(false);
		modifsPanel.add(new JScrollPane(modifs), BorderLayout.CENTER);
		
		panel.add(modifsPanel, BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.add(apply);
		
		
		JPanel btnPanel = new JPanel(new BorderLayout());
		btnPanel.setBorder(SPACING);
		btnPanel.add(buttonPanel, BorderLayout.EAST);
		btnPanel.add(statusLabel, BorderLayout.CENTER);
		panel.add(btnPanel, BorderLayout.SOUTH);
		  
		apply.addActionListener(this);
		return panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	Container c = ((SwingPanel) getComponent ()).getContentPane ();
        statusLabel.setForeground(Color.BLACK);
        enableComponents(false);
        new Thread() { // Shouldn't use UI.execute? 
            @Override
            public void run() {
                // FIXME: what to do when user presses "cancel" on save test plan dialog?
                GenericCallback<String> statusChanged = new GenericCallback<String>() {
                    @Override
                    public void notify(final String s) {
                        SwingUtilities.invokeLater(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        statusLabel.setText(s);
                                        c.repaint();
                                    }
                                });
                    }
                };
                try {
                	Workbench.setCurrent(getWorkbench());
                    LinkedList<String> options = null;
                    
                    manager.applyChanges(statusChanged, true, options);
                } catch (DownloadException ex) {
                    enableComponents(true);
                    statusLabel.setForeground(Color.RED);
                    statusChanged.notify("Failed to apply changes: " + ex.getMessage());
                } catch (Exception ex) {
                    statusLabel.setForeground(Color.RED);
                    statusChanged.notify("Failed to apply changes: " + ex.getMessage());
                    throw ex;
                }
            }
        }.start();
    }

    private void enableComponents(boolean enable) {
        installed.setEnabled(enable);
        available.setEnabled(enable);
        upgrades.setEnabled(enable);
        apply.setEnabled(enable);
    }

    @Override
    public void componentResized(ComponentEvent e) {

    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent evt) {
        loadPlugins();
        topAndDown.setVisible(!manager.getAllPlugins().isEmpty());
        failureLabel.setVisible(manager.getAllPlugins().isEmpty());
        // pack();
    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            PluginsList.openInBrowser(e.getURL().toString());
        }
    }
    
    
    private void initTableRepositories() {
		Item opt = Item.resolveItem (Workbench.current(), "/pluginmanager/options");
        ArrayList<Repository> listofRepo = new ArrayList<Repository>();
        RepoManager repoManager = manager.getRepoManager();
        List<RepoSource> sources = repoManager.getSources();
        UIToolkit ui = Workbench.current().getToolkit();
        int type;
        for (RepoSource source : sources) {
        	try {
	        	if (RepoSourceUtils.listOfRepoContains(manager.getMainRepo(), source.getFile().toString())) {
	        		type=Repository.MAIN;
	        	}
	        	else if(new URI(CacheUtils.getCacheDir(CacheUtils.PLUGIN).toString()).equals(source.getFile())) {
	        		boolean useCache=false;
	        		if (opt!=null) {
	        			useCache = (Boolean) Utils.get (opt, "useCache", false);
	        		}
	        		if (useCache) {
		        		type=Repository.CACHE;
	        		}else {
	        			type=-1;
	        		}
	        	}
	        	else {
	        		type=Repository.USER;
	        	}
        		if (type!=-1) {
    	        	listofRepo.add(new Repository(type , RepoSourceUtils.removeActiveInURI( source.getFile()), source));	
        		}
        	}catch (URISyntaxException e) {
        	}
    }
    ((RepositoryTableModel) ui.getTable(table) ).setRepositoriesList(listofRepo);
        
     // update the preferences when some new data is added
     		ui.getTable(table).addTableModelListener(new TableModelListener() {
                 public void tableChanged(TableModelEvent e) {
                 	// UPDATE the user's repo
                 	List<Repository> userRepos = ((RepositoryTableModel)ui.getTable(table))
                 			.getRepositoriesListOfType(Repository.USER);
                 	// check if one of the Repository need to update the RepoSource 
                 	for (Repository repo : userRepos) {
                 		if (repo.getSource() == null) {
                 			if (!repo.getValue().isEmpty()) {
                 				try {
                 					repo.setSource( repoManager.addRepo(new URI(repo.getValue())) );
     							} catch (URISyntaxException e1) {
     								//skip for now
     							}
                 			}
                 		}
                 		else {
                 			try {
                 				repoManager.changeFile(repo.getSource(), new URI(repo.getValue()));
                 			} catch (Exception e2) {
//                 				the update failed
                 			}
                 		}
                 	}
                 	
                 	// update the option
                 	String paths = createNewPathString(userRepos);
                 	if (opt!=null) {
             			Option o = Option.get (opt, "userDefinedRepo");
             			if (o != null)
             			{
             				o.setOptionValue (paths);
             			}
             		}
                 	
                 	// UPDATE the main repo & cache repo (only "activate/deactivate")
                 	List<Repository> mainRepos = ((RepositoryTableModel)ui.getTable(table))
                 			.getRepositoriesListOfType(Repository.MAIN);
                 	// check if one of the Repository need to update the RepoSource 
                 	// update the option
                 	String pathsMain = createNewPathString(mainRepos);
                 	if (opt!=null) {
             			Option o = Option.get (opt, "mainRepo");
             			if (o != null)
             			{
             				o.setOptionValue (pathsMain);
             			}
             		}
                 	            	
                 }
             });
    }
    
    private Component makeRepositoryPanel(GenericCallback<Object> cb) {
		Workbench wb = Workbench.current();
        UIToolkit ui = wb.getToolkit();
        ArrayList<Repository> listofRepo = new ArrayList<Repository>();
		Item opt = Item.resolveItem (Workbench.current(), "/pluginmanager/options");
        RepoManager repoManager = manager.getRepoManager();
        
		table = ui.createTable(new RepositoryTableModel(listofRepo), wb);
		Object c = ui.createContainer(10);
		ui.addComponent(c, table.getComponent(), BorderLayout.CENTER);
		
		// add and delete button 
		Object b1 = ui.createButton(I18N, "repository.btn.add", UIToolkit.MENU_ICON_SIZE, 0, new Command() {
			@Override
			public String getCommandName() {
				return null;
			}

			@Override
			public void run(Object info, Context ctx) {
				((RepositoryTableModel)ui.getTable(table)).addRow(new Repository(Repository.USER, "", null));
			}
		}, wb);
		Object b2 = ui.createButton(I18N, "repository.btn.del", UIToolkit.MENU_ICON_SIZE, 0, new Command() {
			@Override
			public String getCommandName() {
				return null;
			}

			@Override
			public void run(Object info, Context ctx) {
				int r = ui.getSelectedRow(table);
				Repository repo = ((RepositoryTableModel)ui.getTable(table)).delRow(r);
				if ( repo !=null && repo.getSource() != null) {
					repoManager.removeRepo( repo.getSource());
				}
			}
		}, wb);
		((AbstractButton)b1).setToolTipText(I18N.msg("repository.btn.add.tooltip"));
		((AbstractButton)b2).setToolTipText(I18N.msg("repository.btn.del.tooltip"));
		
		Object x = ui.createContainer(0);
		Object y = ui.createContainer(0);
		ui.addComponent(c, x, BorderLayout.SOUTH);
		ui.addComponent(y, b1, BorderLayout.EAST);
		ui.addComponent(y, b2, BorderLayout.WEST);
		ui.addComponent(x, y, BorderLayout.EAST);
		
		
		
		// add copy and paste from keyboard to cell -- awful way to get the table. But groimp toolkit do not support 
		// to get the table - and not the model :o
		JTable t = (JTable)((ComponentWrapperImpl) table).toDispose;
		t.getActionMap().put("copy", new AbstractAction()
	    {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				String cellValue = ui.getTable(table).getValueAt(t.getSelectedRow(), t.getSelectedColumn()).toString();
				StringSelection stringSelection = new StringSelection(cellValue);
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, stringSelection);
			}
		});
		t.getActionMap().put("paste", new AbstractAction()
	    {
			@Override
			public void actionPerformed(ActionEvent e)
			{		
				try {
					Transferable data = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(t);
					String val = (String) data.getTransferData(DataFlavor.stringFlavor);
					ui.getTable(table).setValueAt(val, t.getSelectedRow(), t.getSelectedColumn());
				} catch (UnsupportedFlavorException | IOException e1) {
					
				}
			}
		});
		
		// some layout parameters for the table. For some reason the formating doesn't last afer adding/removing rows.
		String [] column_tooltips = {
				I18N.msg("table.column.active.tooltip"),
				I18N.msg("table.column.path.tooltip")
				
		};
		class TableHeader extends JTableHeader {

		    String[] tooltips;

		    TableHeader(TableColumnModel columnModel, String[] columnTooltips) {
		      super(columnModel);//do everything a normal JTableHeader does
		      this.tooltips = columnTooltips;//plus extra data
		    }

		    public String getToolTipText(MouseEvent e) {
		        java.awt.Point p = e.getPoint();
		        int index = columnModel.getColumnIndexAtX(p.x);
		        int realIndex = columnModel.getColumn(index).getModelIndex();
		        return this.tooltips[realIndex];
		    }
		}
		TableHeader th = new TableHeader(t.getColumnModel(), column_tooltips);
		t.setTableHeader(th);
		t.getColumnModel().getColumn(0).setMaxWidth(50);
		
		return (Component) c;
    }
    
    /**
     * Transform a the list of element from the repository gui table into a string of paths.
     * Paths are separated by the standard separator character ":". Paths that are non active
     * are preceded by the char "!".
     * @param list a list of repository (the gui table object).
     * @return A string of concatenated paths separated with ":" (e.g. "path1:path2:!path3").
     */
    public String createNewPathString(List<Repository> list) {
    	StringBuffer paths = new StringBuffer();
    	for (Repository r : list) {
    		if (!r.getValue().isEmpty()) {
	    		if (!r.getActive()) {
	    			paths.append("!");
	    		}
	    		paths.append(r.getValue());
	    		paths.append(File.pathSeparatorChar);
    		}
    	}
    	return paths.toString();
    }
    
    
    public static Panel createPanel(Context ctx, Map params) {
		PluginManagerDialog c = new PluginManagerDialog(PluginManager.getInstance());
		c.initialize ((WindowSupport) ctx.getWindow (), params);    
		c.getPluginManager().initSources();
		c.initPlugins();
		c.initTableRepositories();
		return c;
    }
    
    @Override
    public void dispose() {
    	super.dispose();
        try {
        	installed.disposeOld();
        	available.disposeOld();
        	upgrades.disposeOld();
        }catch(NullPointerException e) {
        	e.printStackTrace();
        }
    }

	@Override
	public PluginManager getPluginManager() {
		return manager;
	}

}
