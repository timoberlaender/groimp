package de.grogra.pm.gui;

import java.util.List;
import java.util.stream.Collectors;

import javax.swing.table.AbstractTableModel;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class RepositoryTableModel extends AbstractTableModel {

	private static final long serialVersionUID = -4964889524470642466L;

	private List<Repository> repositoriesList;
	
	private final String[] columnName = new String[] {
			"Active", "Path"
    };
	private final Class[] columnClass = new Class[] {
	        Boolean.class, String.class
	    };
	
	public RepositoryTableModel(List<Repository> repoList) {
		this.repositoriesList = repoList;
	}
	
	
	@Override
    public String getColumnName(int column)
    {
        return columnName[column];
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
 
    @Override
    public int getColumnCount()
    {
        return columnName.length;
    }
    
	@Override
	public int getRowCount() {
		return repositoriesList.size();
	}
	
	public void addRow(Repository r) {
		if (r == null) {
            throw new IllegalArgumentException("Repository cannot be null");
        }
		repositoriesList.add(r);
		this.fireTableRowsInserted(getRowCount()-1, getRowCount());
	}
	
	/**
	 * return the element that was deleted. Null if nothing changed.
	 * @param rowIndex
	 * @return
	 */
	public Repository delRow(int rowIndex) {
		if (rowIndex < 0) {
            return null;
        }
		Repository repo = repositoriesList.get(rowIndex);
		if (repo.getType()!=Repository.USER) {
			return null;
		}		
		repositoriesList.remove(rowIndex) ;
		this.fireTableRowsDeleted(rowIndex, rowIndex);
		return repo;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Repository row = repositoriesList.get(rowIndex);
		if (columnIndex == 0) {
			return row.getActive();
		}
		if (columnIndex == 1) {
			return row.getValue();
		}
		return null;
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (rowIndex<0 ) {return;}
		Repository repo = repositoriesList.get(rowIndex);
		if (columnIndex == 0 && aValue instanceof Boolean) {
			repo.setActive((Boolean)aValue);
		}
		else if (columnIndex == 1 && aValue instanceof String) {
			repo.setValue((String)aValue);
		}
		this.fireTableRowsUpdated(rowIndex, rowIndex);
    }
	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex<0) {
			return false;
		}
		if (columnIndex==0) {return true;}
		return (repositoriesList.get(rowIndex).getType()==Repository.USER)? true : false;
    }
	
	public List<Repository> getRepositoriesList(){
		return repositoriesList;
	}
	
	public void setRepositoriesList(List<Repository> l) {
		repositoriesList=l;
		this.fireTableRowsInserted(0, l.size());
	}
	
	public List<Repository> getRepositoriesListOfType(int type){
		return repositoriesList.stream()
				.filter(r -> r.getType()==type)
				.collect(Collectors.toList())
				;
	}
}
