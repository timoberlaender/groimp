package de.grogra.pm.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.EventObject;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;

import de.grogra.pf.ui.UI;
import de.grogra.pm.PluginEntry;
import de.grogra.pm.PluginManager;
import de.grogra.pm.Version.VersionListener;
import de.grogra.util.Disposable;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
class PluginCheckbox extends JCheckBox implements VersionListener, Disposable, Comparable<PluginCheckbox> {

	public interface StateListener extends EventListener {
		public void stateChanged(EventObject e) ;
	}
	
    /**
     *
     */
    private static final long serialVersionUID = 3604852617806921883L;
    private PluginEntry plugin;
    private boolean canHalfstate;
    // the version candidate selected by this checkbox 
    private String selectedVersion=PluginEntry.VER_STOCK;
    static public final int UNSELECTED = 0;
    static public final int SELECTED = 1;
    static public final int HALFSELECTED = 2;
    private int state=UNSELECTED;
    private List<StateListener> listeners = new ArrayList<StateListener>();
    
    private static Icon selected = new ImageIcon(
    		UI.getIcon("states/cb_selected").getIcon(new Dimension(16,16), de.grogra.icon.Icon.DEFAULT).getImage()
    		);
    private static Icon unselected = new ImageIcon(
    		UI.getIcon("states/cb_unselected").getIcon(new Dimension(16,16), de.grogra.icon.Icon.DEFAULT).getImage()
    		);
    private static Icon halfselected = new ImageIcon( 
    		UI.getIcon("states/cb_halfselected").getIcon(new Dimension(16,16), de.grogra.icon.Icon.DEFAULT).getImage()
    		);
    
    /*
     * ---------------------------------------
     * Version management
     * ---------------------------------------
     */
    
	@Override
	public void versionChanged(VersionListener origin) {
	}
	@Override
    public String getVersion() {
    	if (getState()==SELECTED)
    		return this.selectedVersion;
    	else
    		return PluginEntry.VER_STOCK;
    }
    

	/*
     * ---------------------------------------
     * Initiate object
     * ---------------------------------------
     */
	
    public PluginCheckbox(String name, boolean canHalfstate) {
        super(name);
        this.canHalfstate=canHalfstate;
    }

    public void setPlugin(PluginEntry plugin) {
        this.plugin = plugin;
        this.plugin.addCandidateVersionListener(this);
        if (!plugin.canUninstall()) {
            super.setEnabled(false);
        }
    }

    public PluginEntry getPlugin() {
        return plugin;
    }

    @Override
    public void setEnabled(boolean b) {
        if (!plugin.canUninstall()) {
            super.setEnabled(false);
        } else {
            super.setEnabled(b);
        }
    }
    
    public String getSelectedVersion() {
    	return this.selectedVersion;
    }
    
    public void setSelectedVersion(String version) {
    	this.selectedVersion=version;
    }
	@Override
	public void dispose() {
		plugin.removeCandidateVersionListener(this);
		removeStateListeners();
	}

	@Override
    public void paint(Graphics g) {
        setIcon(isHalfSelected() ? halfselected : getState()==SELECTED ? selected : unselected);
        super.paint(g);
    }
	
	/*
     * ---------------------------------------
     * State management
     * ---------------------------------------
     */

    public boolean isHalfSelected() {
        return this.state == HALFSELECTED;
    }

    @Override
    public void setSelected(boolean b) {
    	super.setSelected(b);
    	if (b) {this.setState(SELECTED);}
    	else {this.setState(UNSELECTED);}
    }
    
    @Override 
    public boolean isSelected() {
    	return this.state==SELECTED;
    }
    
    /**
     * Get the state of the box - UNSELECTED, SELECTED or HAFLSELECTED
     * @return
     */
    public int getState() {
    	return this.state;
    }
    
    /**
     * Set the state of this box
     * @param newState
     */
    public void setState(int newState) {
    	this.state = newState;
    	for (StateListener l : listeners) {
    		l.stateChanged(new EventObject(this));
    	}
    }
    
    /**
     * Change the state of the box. It rotates as follows: unselected, halfselected, selected
     */
    public void changeState() {
    	if (this.canHalfstate) {
    		setState( (getState()+1) %3 );
    	}
    	else {
    		setState( getState()==SELECTED? UNSELECTED : SELECTED);
    	}
    }
    
    public void addStateListener(StateListener l) {
    	if (!listeners.contains(l)) {
    		listeners.add(l);
    	}
    }
    
    private void removeStateListeners() {
    	listeners.clear();
    }
    
    
    /**
     * Return the state of the plugin depending on the state of the checkbox.
     * The states are cb.SELECTED -> pm.INSTALL; cb.UNSELECTED -> pm.UNINSTALL; cb.HALFSELECTED -> pm.DISABLE
     * Return -1 is there is an error.
     * @return
     */
    public int getPluginState() {
    	switch(state) {
    	case SELECTED:
    		return PluginManager.INSTALL;
    	case UNSELECTED:
    		return PluginManager.UNINSTALL;
    	case HALFSELECTED:
    		return PluginManager.DISABLE;
    	default:
    		return -1;
    	}
    }
    
    @Override
	public int compareTo(PluginCheckbox o) {
    	if (plugin.canUninstall() == o.plugin.canUninstall()) {
    		return plugin.getName().compareTo(o.plugin.getName());
    	}
    	else {
    		return plugin.canUninstall() ? -1 : 1;
    	}
	}
    
}
