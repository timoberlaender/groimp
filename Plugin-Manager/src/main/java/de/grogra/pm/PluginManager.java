package de.grogra.pm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.grogra.pf.boot.Main;
import de.grogra.pf.registry.Directory;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Plugin;
import de.grogra.pf.registry.PluginDescriptor;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.FileChooserResult;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.PanelFactory;
import de.grogra.pf.ui.util.ComponentWrapperImpl;
import de.grogra.pm.exception.DownloadException;
import de.grogra.pm.exception.PluginException;
import de.grogra.pm.gui.PluginManagerDialog;
import de.grogra.pm.repo.RepoManager;
import de.grogra.pm.util.CacheUtils;
import de.grogra.pm.util.DisplayUtils;
import de.grogra.pm.util.URIListIterator;
import de.grogra.util.Disposable;
import de.grogra.util.I18NBundle;
import de.grogra.util.ResourceConverter;
import de.grogra.util.Utils;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class PluginManager extends Plugin implements Disposable, ResourceConverter {
	
	public static final I18NBundle I18N = I18NBundle.getInstance(PluginManager.class);

//	private Workbench workbench;
	// static manager speedup reloading the manager when closed as it remains in cache
	private static PluginManager PLUGIN;
//	private static PluginManager staticManager = new PluginManager();
	// The list of repositories from where look for the available plugins (remote json, local cache dir, ...)
	private final RepoManager repoManager;
	// the set of every available plugin available from the jsonsources
	protected Map<PluginEntry, Integer> allPlugins = new HashMap<>();
	// local directory where the plugin will be installed
	private String pluginsDirectory;
	private static Logger log;
	// the differents states of the plugin - used to resolve changes. 
	public final static int UNINSTALL = 0;
	public final static int INSTALL = 1;
	public final static int DISABLE = 2;
	// list of paths used as main repository (= path to the groimp main .list and groimp installation repo)
//	private List<String> mainPathsRepo= new ArrayList<String>();
	public final static String REPO_PLUGIN_LIST = "repo-plugin-list";
	public final static String CACHE_REPO = "cache-repository";
	public final static String CACHE_SOURCE_PATH = "cache-repo-sources";
	public final static String CACHE_PLUGIN_PATH = "cache-repo-plugins";
//	public final static String DEFAULT_PLUGIN_LIST =
//			"https://gitlab.com/grogra/groimp-plugins/Plugin-Manager/-/raw/master/plugin_repo.list";
	static GenericCallback<Object> callback = new GenericCallback<Object>() {
		@Override
		public void notify(Object ignore) {}
	};
	
	
	public static PluginManager getInstance() {
		return PLUGIN;
	}
	
	@Override
	public void startup() {
		super.startup();
		I18NBundle.addResourceConverter(this);
	}
	
	
	public boolean canHandleConversion(String name) {
		return "pluginentry".equals(name);
	}
	
	public Object convert(String name, String argument, I18NBundle bundle) {
		if ("pluginentry".equals(name)) {
			Set<PluginEntry> pluginFromJSON=null;
			InputStream in=null;
			try {
				in =bundle.getClassLoader().getResourceAsStream(argument);
				if (in != null) {
					pluginFromJSON = getPluginFromRepo(new JSONArray(IOUtils.toString(in, "UTF-8")));
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			} finally {
				if (in !=null){
					try{in.close();}
					catch (IOException e){}
				}
			}
			return pluginFromJSON;
		} else {
			throw new IllegalArgumentException(name);
		}
	}
	

	public PluginManager() {

		assert PLUGIN == null;
		PLUGIN = this;
		
		log = de.grogra.pf.boot.Main.getLogger();
		pluginsDirectory = Main.getProperty(de.grogra.pf.boot.Main.PLUGIN_ROOT_PATH);
		repoManager = new RepoManager();
	}
		
	public void initSources() {
		repoManager.cleanRepo();

		// groimpProp is the list of paths provided by default & by the user on start up
		for (String path : getMainRepo() )	{
			repoManager.addRepo(path);
		}			
		// add the cache repository as well:
		try {
			repoManager.addRepo(new URI( CacheUtils.getCacheDir(CacheUtils.PLUGIN).getPath()) );
		} catch (URISyntaxException e) {
			log.info("Cache repo has failed to load : " + CacheUtils.getCacheDir(CacheUtils.PLUGIN));
		}
		
		// add the user repo (defined in the preferences)
		Item opt = Item.resolveItem (Workbench.current(), "/pluginmanager/options");
		String userDefinedRepo="";
		if (opt!=null) {
			userDefinedRepo = (String) Utils.get (opt, "userDefinedRepo", "");
		}
		for (URIListIterator pathList
				 = new URIListIterator (userDefinedRepo);
				 pathList.hasNext (); )	{
				Object repo = pathList.nextURI ();
				if (repo instanceof URI)
					repoManager.addRepo( (URI)repo );
				else if (repo instanceof String)
					repoManager.addRepo( (String) repo);
		}
		
	}

	public boolean hasPlugins() {
		return allPlugins.size() > 0;
	}

	public synchronized void load() throws Throwable {
//		detectJARConflicts();

		if (hasPlugins()) {
			return;
		}
		
		allPlugins.putAll(getAlreadyInstalledPlugins());

		JSONArray json = repoManager.getRepo();
		loadPluginFromRepo(json);
	}

	public void addMissingVersions(PluginEntry pluginFromJSON) {
		PluginEntry existingPlugin = getPluginByID(pluginFromJSON.getID());
		for (String v : pluginFromJSON.versions.keySet()) {
			if (!existingPlugin.versions.has(v)) {
				existingPlugin.versions.put(v, pluginFromJSON.versions.get(v));
			}
		}
	}

	public Boolean containsPlugin(String id) {
		for (PluginEntry plugin : allPlugins.keySet()) {
			if (plugin.getID().equals(id)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Check writing permission. 
	 * 
	 */
	private void checkRW(DependencyResolver resolver) throws UnsupportedEncodingException, AccessDeniedException {
		// if at least one plugin is in a non writable path - throw an error
		List<String> delPaths= new ArrayList<String>();
		for (PluginEntry p : resolver.getDeletions()) {
			if (p.getInstalledPath() != null) {
				File libext = new File(URLDecoder.decode(p.getInstalledPath(), "UTF-8"));
				if (!isWritable(libext)) {
					String msg = I18N.getString("error.writepermission.deletion");
					throw new AccessDeniedException(msg + libext);
				}
			}	
		}
	}

	private boolean isWritable(File path) {
		if (!path.isDirectory()) {
			path = path.getParentFile();
		}
		File sample = new File(path, "empty.txt");
		try {
			sample.createNewFile();
			sample.delete();
			return true;
		} catch (IOException e) {
			log.severe(I18N.getString( "error.writepermission.deletion"));
			return false;
		}
	}

	public void startModifications(Set<PluginEntry> delPlugins, Set<PluginEntry> installPlugins,
			Set<PluginEntry> disablePlugins, Set<PluginEntry> ablePlugins,
			boolean doRestart, LinkedList<String> additionalGroIMPptions) 
					throws IOException {
		ChangesMaker maker = new ChangesMaker(allPlugins);

		// if use black list, deletions are added to the file but not deleted
		Item opt = Item.resolveItem (Workbench.get(getRegistry()), "/pluginmanager/options");
		boolean useBlacklist=false;
		if (opt!=null) {
			useBlacklist = (Boolean) Utils.get (opt, "deleteAsBlacklist", false);
		}		
		if (useBlacklist) {
			maker.blacklist(delPlugins);
			delPlugins.clear();
		}
		
		File moveFile = maker.getMovementsFile(delPlugins, installPlugins);
		File installFile = maker.getInstallFile(installPlugins);
		File restartFile;
		if (doRestart) {
			restartFile = maker.getRestartFile(additionalGroIMPptions);
		} else {
			restartFile = null;
		}
		maker.changeDisable(disablePlugins, ablePlugins);
		final ProcessBuilder builder = maker.getProcessBuilder(moveFile, installFile, restartFile);
		log.info(I18N.msg("info.logpath", builder.redirectOutput().file().getPath()) );
		builder.start();
	}

	public void applyChanges(GenericCallback<String> statusChanged, boolean doRestart,
			LinkedList<String> additionalGroIMPoptions) {		
		DependencyResolver resolver = new DependencyResolver(allPlugins);		

		Item opt = Item.resolveItem (Workbench.get(getRegistry()), "/pluginmanager/options");
		boolean useBlacklist=false;
		if (opt!=null) {
			useBlacklist = (Boolean) Utils.get (opt, "deleteAsBlacklist", false);
		}
		
		if (!useBlacklist) {
			try {
				checkRW(resolver);
			} catch (Throwable e) {
				callback.notify(I18N.msg("error.writepermission.msg", 
						Main.getConfigurationDirectory()+System.lineSeparator()+"blacklisted"));
				throw new RuntimeException(I18N.msg("error.failchange", e.getMessage()), e);
			}
		}
		
		Set<PluginEntry> additions = resolver.getAdditions();
		for (PluginEntry plugin : additions) {
			try {
				plugin.download(repoManager, statusChanged);
			} catch (IOException | URISyntaxException e) {
				String msg = I18N.msg("error.download", plugin.getID());
				log.warning(msg + e);
				statusChanged.notify(msg);
				throw new DownloadException(msg, e);
			} 
		}

		if (doRestart) {
			log.info(I18N.getString("info.restart"));
			statusChanged.notify(I18N.getString("info.restart"));
		}

		modifierHook(resolver.getDeletions(), additions, resolver.getToDisable(), resolver.getToAble(),
				doRestart, additionalGroIMPoptions);
	}

	private void modifierHook(final Set<PluginEntry> deletions, final Set<PluginEntry> additions,
			final Set<PluginEntry> toDisable, final Set<PluginEntry> toAble,
			final boolean doRestart, final LinkedList<String> additionalGroIMPptions) {
		if (deletions.isEmpty() && additions.isEmpty() 
				&& toDisable.isEmpty() && toAble.isEmpty()) {
			log.info(I18N.getString("info.nochanges"));
		} else {
			log.info(I18N.getString("info.changes"));
			Workbench wb = Workbench.current();
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					try {
						Workbench.setCurrent(wb);
						log.info(I18N.getString("info.startchanges"));
						startModifications(deletions, additions, 
								toDisable, toAble,
								doRestart, additionalGroIMPptions);
					} catch (Exception e) {

					}
				}
			});
			// change spot
			if (doRestart) {
				wb.getMainWorkbench().getApplication().exit();
			}
		}
	}
	
	public String getChangesAsText() {
		return getChangesAsText(true);
	}
	
	public String getChangesAsText(DependencyResolver resolver) {
		StringBuilder text = new StringBuilder();

		for (PluginEntry pl : resolver.getDeletions()) {
			text.append(I18N.msg("description.plugin.uninstall", pl, pl.getInstalledVersion()) )
				.append("\n");
		}

		for (PluginEntry pl : resolver.getAdditions()) {
			text.append(I18N.msg("description.plugin.install", pl, pl.getCandidateVersion()) )
				.append("\n");
		}

		for (Map.Entry<PluginEntry, PluginEntry> entry : resolver.getPluginExclusions().entrySet()) {
			if (entry.getValue() != null) {
				text.append(I18N.msg("description.plugin.error.excludes", 
						entry.getKey().getName(), entry.getValue().getName()) )
						.append("\n");
			} else {
				text.append(I18N.msg("description.plugin.error.excludesinstalled", entry.getKey().getName()))
						.append("\n");
			}
		}
		
		for (PluginEntry pl : resolver.getToDisable()) {
			text.append(I18N.msg("description.plugin.disable", pl, pl.getCandidateVersion()) )
				.append("\n");
		}

		for (PluginEntry pl : resolver.getToAble()) {
			text.append(I18N.msg("description.plugin.able", pl, pl.getCandidateVersion()) )
				.append("\n");
		}

		return text.toString();
	}

	public String getChangesAsText(boolean reload) {
		DependencyResolver resolver = new DependencyResolver(allPlugins, reload);
		return getChangesAsText(resolver);
	}

	public void loadPluginFromLocalRepo(File path) {
		if (path!=null) {
			//TODO: before testing the plugin.xml, look for plugin.json -
	    	Collection<File> pluginFiles = FileUtils.listFiles (path, new TrueFileFilter() {
			    public boolean accept(File file) {
			        return file.getName().equals("plugin.xml");
			    }
			}, TrueFileFilter.INSTANCE );
	    	
	    	for (File f : pluginFiles) {
	    		try {
	    		PluginEntry plugin = PluginEntry.fromJSON(new JSONObject());
	    		if (plugin.getName().isEmpty()) {
	              log.warning(I18N.msg("info.skipname", plugin.getID()) );
	              continue;
	    		}
				plugin.detectInstalled();
				if ( !containsPlugin( plugin.getID())){ // if the plugin is not already included (aka installed) it is added
					allPlugins.put(plugin, plugin.getState());
				}
				else {
					addMissingVersions(plugin);
				}
	    		}catch(NullPointerException e) {
	    			// 
	    		}
	        }
		}
	}
	
	public void loadPluginFromRemoteRepo(JSONArray json) {
		if (!(json instanceof JSONArray)) {
			throw new RuntimeException(I18N.getString("error.notarray"));
		}

		for (Object elm : (JSONArray) json) {
			if (elm instanceof JSONObject) {
				PluginEntry plugin = PluginEntry.fromJSON((JSONObject) elm);
				if (plugin.getName().isEmpty()) {
					log.warning(I18N.msg("info.skipname", plugin.getID()));
					continue;
				}

				plugin.detectInstalled();
				if (!containsPlugin(plugin.getID())) { // if the plugin is not already included (aka installed) it is added
					allPlugins.put(plugin, plugin.getState());
				} else {
					addMissingVersions(plugin);
				}
			} else {
				log.warning(I18N.msg("error.invalidarray", elm));
			}
		}
	}
	
	/**
	 * Load the plugins from the json array to the set of allPlugins
	 * @param json
	 */
	public void loadPluginFromRepo(JSONArray json) {
		loadPluginFromRemoteRepo(json);
	}
	
	/**  
	 * Get the plugin entries from a json array
	 * @param json
	 */
	public Set<PluginEntry> getPluginFromRemoteRepo(JSONArray json) {
		if (!(json instanceof JSONArray)) {
			throw new RuntimeException(I18N.getString("error.notarray"));
		}
		Set<PluginEntry> res = new TreeSet<>(new PluginComparator()); 

		for (Object elm : (JSONArray) json) {
			if (elm instanceof JSONObject) {
				PluginEntry plugin = PluginEntry.fromJSON((JSONObject) elm);
				if (plugin.getName().isEmpty()) {
					log.warning(I18N.msg("info.skipname", plugin.getID()));
					continue;
				}
				res.add(plugin);
			} else {
				log.warning(I18N.msg("error.invalidarray", elm));
			}
		}
		return res;
	}
	
	public Set<PluginEntry> getPluginFromRepo(JSONArray json) {
		return getPluginFromRemoteRepo(json);
	}

	public HashMap<PluginEntry, Integer> getAlreadyInstalledPlugins() {
		HashMap<PluginEntry, Integer> result = new HashMap<>();

		Registry r = Main.getRegistry();
		de.grogra.graph.impl.Node d = r.getPluginDirectory();
		result.putAll(scanPluginsDirectory(d));
		return result;
	}
	
	public HashMap<PluginEntry, Integer> scanPluginsDirectory(de.grogra.graph.impl.Node dir){
		HashMap<PluginEntry, Integer> result = new HashMap<>();
		de.grogra.graph.impl.Node d = dir.getBranchTail();
		
		while (d != null) {
			if (d instanceof Directory ) {
				result.putAll( scanPluginsDirectory(d));
			}
			if (d instanceof PluginDescriptor 
					&& ((PluginDescriptor) d).getPluginState () != PluginDescriptor.ERROR 
					&& ! ((PluginDescriptor) d).getPluginName().equals("Core Classes")) {
				try {
					PluginEntry pe = PluginEntry.fromPluginDescriptor( ((PluginDescriptor) d));
					int status = ( ((PluginDescriptor) d).getPluginState()==PluginDescriptor.DISABLED ) ?
							DISABLE : INSTALL;
					result.put(pe, status);
				} catch (Exception e) {
					Main.logSevere(e);
				}
			}
			d= d.getPredecessor();
		}
		
		return result;
	}

	public Set<PluginEntry> getInstalledPlugins() {
		Set<PluginEntry> result = new TreeSet<>(new PluginComparator());
		for (PluginEntry plugin : allPlugins.keySet()) {
			if (plugin.isInstalled()) {
				result.add(plugin);
			}
		}
		return result;
	}

	public static Set<PluginEntry> getInstalledPlugins(Map<PluginEntry, Integer> allPlugins) {
		Set<PluginEntry> result = new HashSet<>();
		for (PluginEntry plugin : allPlugins.keySet()) {
			if (plugin.isInstalled()) {
				result.add(plugin);
			}
		}
		return result;
	}

	public Set<PluginEntry> getAvailablePlugins() {
		Set<PluginEntry> result = new TreeSet<>(new PluginComparator());
		for (PluginEntry plugin : allPlugins.keySet()) {
			if (!plugin.isInstalled()) {
				result.add(plugin);
			}
		}
		return result;
	}

	public Set<PluginEntry> getUpgradablePlugins() {
		Set<PluginEntry> result = new TreeSet<>(new PluginComparator());
		for (PluginEntry plugin : allPlugins.keySet()) {
			if (plugin.isUpgradable()) {
				result.add(plugin);
			}
		}
		return result;
	}

	public void togglePlugins(Set<PluginEntry> pluginsToInstall, int isInstall) {
		for (PluginEntry plugin : pluginsToInstall) {
			toggleInstalled(plugin, isInstall);
		}
	}

	/**
	 * Change the state of a plugin. States are checked when the modifications are resolved.
	 * The states are INSTALL, UNINSTALL, DISABLE.
	 * @param plugin
	 * @param peState
	 */
	public void toggleInstalled(PluginEntry plugin, int peState) {
		if (peState==UNINSTALL && !plugin.canUninstall()) {
			log.warning(I18N.msg("error.plugin.cannotuninstall", plugin.getID()));
			peState = INSTALL;
		}
		allPlugins.put(plugin, peState);
	}

	public boolean hasAnyUpdates() {
		for (PluginEntry p : allPlugins.keySet()) {
			if (p.isUpgradable()) {
				return true;
			}
		}
		return false;
	}

	public PluginEntry getPluginByID(String key) {
		for (PluginEntry p : allPlugins.keySet()) {
			if (p.getID().equals(key)) {
				return p;
			}
		}
		throw new IllegalArgumentException(I18N.msg("error.plugin.notfound", key));
	}
	
	public PluginEntry getPluginByName(String key) {
		for (PluginEntry p : allPlugins.keySet()) {
			if (p.getName().equals(key)) {
				return p;
			}
		}
		throw new IllegalArgumentException(I18N.msg("error.plugin.notfound", key));
	}

	private class PluginComparator implements java.util.Comparator<PluginEntry> {
		@Override
		public int compare(PluginEntry o1, PluginEntry o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}

	public void setTimeout(int timeout) {
		repoManager.setTimeout(timeout);
	}
	

	/**
	 * @return Static instance of manager, used to spare resources on repo loading
	 */
//	public static PluginManager getStaticManager() {
//		try {
//			staticManager.load();
//		} catch (Throwable e) {
//			throw new RuntimeException(I18N.getString("error.repository.load"), e);
//		}
//		return staticManager;
//	}

	/**
	 * @param id ID of the plugin to check
	 * @return Version name for the plugin if it is installed, null otherwise
	 */
	public static String getPluginStatus(String id) {
		PluginManager manager = getInstance();

		for (PluginEntry plugin : manager.allPlugins.keySet()) {
			if (plugin.id.equals(id)) {
				return plugin.getInstalledVersion();
			}
		}
		return null;
	}

	/**
	 * @return Status for all plugins
	 */
	public static String getAllPluginsStatus() {
		PluginManager manager = getInstance();
		return manager.getAllPluginsStatusString();
	}

	private String getAllPluginsStatusString() {
		ArrayList<String> res = new ArrayList<>();
		for (PluginEntry plugin : getInstalledPlugins()) {
			res.add(plugin.getID() + "=" + plugin.getInstalledVersion());
		}
		return Arrays.toString(res.toArray());
	}

	public Map<PluginEntry, Integer> getAllPlugins() {
		return allPlugins;
	}

	/**
	 * @return Available plugins
	 */
	public static String getAvailablePluginsAsString() {
		PluginManager manager = getInstance();
		return manager.getAvailablePluginsString();
	}

	private String getAvailablePluginsString() {
		ArrayList<String> res = new ArrayList<>();
		for (PluginEntry plugin : getAvailablePlugins()) {
			List<String> versions = new ArrayList<>(plugin.getVersions());
			Collections.reverse(versions);
			res.add(plugin.getID() + "=" + Arrays.toString(versions.toArray()));
		}
		return Arrays.toString(res.toArray());
	}

	/**
	 * @return Upgradable plugins
	 */
	public static String getUpgradablePluginsAsString() {
		PluginManager manager = getInstance();
		return manager.getUpgradablePluginsString();
	}

	private String getUpgradablePluginsString() {
		ArrayList<String> res = new ArrayList<>();
		for (PluginEntry plugin : getUpgradablePlugins()) {
			res.add(plugin.getID() + "=" + plugin.getMaxVersion());
		}
		return (res.size() != 0) ? Arrays.toString(res.toArray()) : I18N.getString("info.noupdate");
	}


	public void logPluginComponents() {
		StringBuilder report = new StringBuilder("Plugin Components:\n");
		for (PluginEntry plugin : getInstalledPlugins()) {
			String[] searchPaths = { plugin.installedPath };
		}
	}
	
	public void changeCallBack(GenericCallback<Object> newCallBack) {
		this.callback=newCallBack;
	}

	@Override
	public void dispose() {
		allPlugins = null;
	}
	
	public void clearCache() throws IOException {
		CacheUtils.clearCacheRepo();
	}
	
	public void reloadRepo() throws Throwable {
		allPlugins.clear();
		load();
	}
	
	public void clearRepo() throws Throwable {
		allPlugins.clear();
	}
	
	public List<String> getMainRepo() {
		List<String> mainRepos = new ArrayList<String>();
		Properties config = new Properties();
		Properties p = new Properties(config);
		
		Item opt = Item.resolveItem (Workbench.get(getRegistry()), "/pluginmanager/options");
		String mainRepo="";
		if (opt!=null) {
			mainRepo = (String) Utils.get (opt, "mainRepo", "");
		}
		
		String defaultProp = p.getProperty(REPO_PLUGIN_LIST, mainRepo);
		String groimpProp = de.grogra.pf.boot.Main.getProperty(REPO_PLUGIN_LIST, defaultProp);
		for (URIListIterator pathList
				 = new URIListIterator (groimpProp);
				 pathList.hasNext (); )	{
				Object repo = pathList.nextPath ();
				if (repo instanceof String)
					mainRepos.add((String)repo);
		}
		return mainRepos;
	}
	
	public void exportPluginList(File output, GenericCallback<Object> statusChange) {
		try (FileOutputStream fout = new FileOutputStream(output);
			OutputStreamWriter out = new OutputStreamWriter(fout)) {
            FileUtils.touch(output);
            Set<PluginEntry> plugins = getInstalledPlugins();
            JSONObject el = new JSONObject();
            for (PluginEntry plugin : plugins) {
            	el.put(plugin.getID(), plugin.getInstalledVersion());
            }
            out.write(el.toString(2));
        } catch (IOException ex) {
        	Main.getLogger().warning(I18N.getString("error.export")+ ex);
        }
	}
	
	public static void exportPluginList(Item item, Object info, Context ctx) {
		FileChooserResult fc = ctx.getWindow().chooseFile("", null, null, 0, false, null);
		if (fc!=null)
			getInstance().exportPluginList(fc.file, callback);
	}
	
	/**
	 * This set the plugin entries to match the set of plugin from the file input.
	 * I.e. it deactivate plugins that are not in the list, and 
	 * @param input
	 */
	public void importPluginList(File input, GenericCallback<Object> statusChanged) {
		Map<String, String> plugins = new HashMap<String, String>();		
		byte[] bytes;
		allPlugins.replaceAll((k,v) -> v= UNINSTALL);
		try {
			bytes = Files.readAllBytes(input.toPath());
			String content = new String(bytes);
			JSONObject json = new JSONObject(content);
			for (String k : ((JSONObject)json).keySet()) {
				PluginEntry plugin = getPluginByID( k );
				allPlugins.replace(plugin, INSTALL);
				plugin.setCandidateVersion(((JSONObject)json).getString(k));
			}
			DependencyResolver resolver = new DependencyResolver(allPlugins, false);
			statusChanged.notify(resolver);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (PluginException e) {
			statusChanged.notify( "Cannot install plugin dependency failing : " + e.getMessage());
		}
	}
	
	public void importPluginList(GenericCallback<Object> statusChanged, Context ctx) {
		FileChooserResult fc = ctx.getWindow().chooseFile("", null, null, 0, true, null);
		if (fc!=null)
			importPluginList(fc.file, statusChanged);
	}
	
	public static void importPluginList(Item item, Object info, Context ctx) {
		FileChooserResult fc = ctx.getWindow().chooseFile("", null, null, 0, true, null);
		if (fc!=null)
			getInstance().importPluginList(fc.file, callback);
	}
	
	public static void clearCache(Item item, Object info, Context ctx) {
		try {
			getInstance().clearCache();
		} catch (Throwable e) {
			callback.notify("Error :'(");
		}
	}
	
	public static void reloadRepositories(Item item, Object info, Context ctx) {
		try {
			((PluginManagerContext)ctx.getPanel()).getPluginManager() .clearRepo();
			callback.notify(PluginManagerDialog.RELOAD);
		} catch (Throwable e) {
			callback.notify("Error :'(");
		}
	}
	
	public RepoManager getRepoManager() {
		return this.repoManager;
	}
	
	
	public static Panel createPluginPanel(Context ctx, de.grogra.util.Map params) {
		try {
			UIToolkit ui = ctx.getWorkbench().getToolkit();
			getInstance().initSources();
			getInstance().load();
			Panel p = ui.createPanel(ctx, null, params);
			return p;
		} catch (Throwable e) {
			callback.notify("Error :'(");
		}
		return null;
	}
	
	public static void describeAllPlugins(Item item, Object info, Context ctx) {
		try {
			Panel p = ctx.getWindow().getPanel("/ui/panels/pluginpanel");
			if (p==null) {
				p = PanelFactory.createPanel(ctx, "/ui/panels/pluginpanel", de.grogra.util.Map.EMPTY_MAP);
			} if (p==null) {
				return;
			}
			
			UIToolkit ui = ctx.getWorkbench().getToolkit();
			Map<PluginEntry, Integer> plugins = getInstance().getAllPlugins();
			Object viewer = ui.createTextViewer (null, null, null, null, false);
			ui.setContent(viewer, "text/plain", DisplayUtils.pluginListToString(plugins.keySet()));
			p.setContent(new ComponentWrapperImpl(viewer, (Disposable)viewer));
			p.show(true, p);
		} catch (Throwable e) {
			callback.notify("Error :'(");
		}
	}
	
	public static void uninstallCMD(Item item, Object info, Context ctx) throws Throwable {
		 toggleStringList((String)info ,UNINSTALL);
	}
	public static void disableCMD(Item item, Object info, Context ctx) throws Throwable {
		 toggleStringList((String)info ,DISABLE);
	}
	/**
	 * a static command to run from cmd:
	 *   java -jar core.jar --headless -- -cmd /pluginmanager/commands/installCMD=de.grogra.gltf;de.grogra.cli
	 * @param item
	 * @param info
	 * @param ctx
	 * @throws Throwable
	 */
	
	public static void installCMD(Item item, Object info, Context ctx) throws Throwable {
		 toggleStringList((String)info ,INSTALL);
	}
	
	/**
	 * takes a string of packages ids sepetrated by semicolon and changes their plugin status to the value of isInstall.
	 * @param list
	 * @param isInstall
	 * @throws Throwable
	 */
	public static void toggleStringList(String list ,int isInstall) throws Throwable {
		getInstance().initSources();
		getInstance().load();
		Set<PluginEntry> toInstall = new HashSet<PluginEntry>();
		if(getInstance().hasPlugins()) {
			for(String pluginID : list.split(";")) {
			PluginEntry pe = getInstance().getPluginByID(pluginID);
				if(pe!=null) {
					toInstall.add(pe);
				}
			}
			getInstance().togglePlugins(toInstall,isInstall);
			LinkedList<String> options = null;
			GenericCallback<String> cb = new GenericCallback<String>() {
				@Override
				public void notify(String ignore) {}
			};
			getInstance().applyChanges(cb, true, options);
		}	
	}
	
	
	public static void describePlugin(Item item, Object info, Context ctx) {
		try {
			String name = ctx.getWindow().showInputDialog("Choose a plugin", "Enter a plugin name : ", "");
			Panel p = ctx.getWindow().getPanel("/ui/panels/pluginpanel");
			if (p==null) {
				p = PanelFactory.createPanel(ctx, "/ui/panels/pluginpanel", de.grogra.util.Map.EMPTY_MAP);
			} if (p==null) {
				return;
			}
			UIToolkit ui = ctx.getWorkbench().getToolkit();
			PluginEntry pe = getInstance().getPluginByName(name);
			Object viewer = ui.createTextViewer (null, null, null, null, false);
			ui.setContent(viewer, "text/plain", DisplayUtils.getDescriptionHTML(pe));
			p.setContent(new ComponentWrapperImpl(viewer, (Disposable)viewer));
			p.show(true, p);
		} catch (Throwable e) {
			callback.notify("Error :'(");
		}
	}
	
	
	public static void selectVersion(Item item, Object info, Context ctx) {
		try {
			Panel p = ctx.getWindow().getPanel("/ui/panels/pluginpanel");
			if (p==null) {
				p = PanelFactory.createPanel(ctx, "/ui/panels/pluginpanel", de.grogra.util.Map.EMPTY_MAP);
			} if (p==null) {
				return;
			}
			UIToolkit ui = ctx.getWorkbench().getToolkit();
			Object viewer = ui.createTextViewer (null, null, null, null, false);;
			String msg;
			
			String name = ctx.getWindow().showInputDialog("Choose a plugin", "Enter a plugin name : ", "");
			PluginEntry pe = getInstance().getPluginByName(name);
			ui.setContent(viewer, "text/plain", DisplayUtils.pluginVersions(pe));
			p.setContent(new ComponentWrapperImpl(viewer, (Disposable)viewer));
			p.show(true, p);

			String version = ctx.getWindow().showInputDialog("Choose a plugin", "Select a version : ", "");
			if (pe.getVersions().contains(version)) {
				pe.setCandidateVersion(version);
				msg = "The version has been set as candidate. Apply the change to perform the update.";
			} else {
				msg = "The version: "+version+" is not available.";
			}
			ui.setContent(viewer, "text/plain", msg);
			p.setContent(new ComponentWrapperImpl(viewer, (Disposable)viewer));
			p.show(true, p);
			
		} catch (Throwable e) {
			callback.notify("Error :'(");
		}
	}
	
	public static void applyChanges(Item item, Object info, Context ctx) {
		try {
			Panel p = ctx.getWindow().getPanel("/ui/panels/pluginpanel");
			if (p==null) {
				p = PanelFactory.createPanel(ctx, "/ui/panels/pluginpanel", de.grogra.util.Map.EMPTY_MAP);
			} if (p==null) {
				return;
			}
			LinkedList<String> options = null;
			GenericCallback<String> cb = new GenericCallback<String>() {
				@Override
				public void notify(String ignore) {}
			};
			getInstance().applyChanges(cb, true, options);
		} catch (Throwable e) {
			callback.notify("Error :'(");
		}
	}
	
	public static void setAllUpgrade(Item item, Object info, Context ctx) {
		try {
			Panel p = ctx.getWindow().getPanel("/ui/panels/pluginpanel");
			if (p==null) {
				p = PanelFactory.createPanel(ctx, "/ui/panels/pluginpanel", de.grogra.util.Map.EMPTY_MAP);
			} if (p==null) {
				return;
			}
			for (PluginEntry pe : getInstance().getUpgradablePlugins()) {
				pe.setCandidateVersion(pe.getMaxVersion());
			}
		} catch (Throwable e) {
			callback.notify("Error :'(");
		}
	}
	
	public static void reloadHTTPClient(Item item, Object info, Context ctx) {
		PLUGIN.getRepoManager().reloadHTTPClient();
	}
}
