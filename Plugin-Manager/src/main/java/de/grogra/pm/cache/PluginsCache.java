package de.grogra.pm.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.commons.io.FileUtils;

import de.grogra.pf.boot.Main;
import de.grogra.pm.repo.RepoManager;
import de.grogra.pm.util.PluginEntryUtils;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class PluginsCache extends CacheBase implements Serializable {
    private static final long serialVersionUID = 1L;

    public PluginsCache(String file, long expirationTime, long lastModified) {
        this.cachedObject = file;
        this.expirationTime = expirationTime;
        this.lastModified = lastModified;
    }

    public String getPluginName() {
        return new File(cachedObject).getParentFile().getName().toString();
    }
    
    public String getCache() {
    	
        return cachedObject;
    }

    /**
     * Save plugin to local directory. The plugin directory should contains plugin.xml, 
     * plugin.properties & plugin.jar, & the cache file (a serialized file that contains this)
     * Includes the copy of LIBS
     * @param file
     * @throws IOException 
     */
    public void saveToFile(File file, RepoManager.DownloadResult dwn) throws IOException {
    	// 1)
		try (FileOutputStream fout = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fout)) {
			FileUtils.touch(file);
			out.writeObject(this);
			} catch (IOException ex) {
				Main.getLogger().warning("Failed for serialize repo"+ ex);
			}
		// 2)
		File dst = file.getParentFile();
		File src = new File(dwn.getTmpFile());
		PluginEntryUtils.movePlugin(dst, src, true);
		
		// 3)
		PluginEntryUtils.checkPlugin(dst, this);
	}

    public static PluginsCache fromFile(File file) {
        // Deserialization
        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream in = new ObjectInputStream(fis);) {

            // Method for deserialization of object
            PluginsCache tmp = (PluginsCache) in.readObject();
            if (PluginEntryUtils.checkPlugin(file.getParentFile(), tmp)) {
            	return tmp;
            }
        } catch (IOException | ClassNotFoundException ex) {
            Main.getLogger().warning("Failed for deserialize repo"+ ex);
            return null;
        }
		return null;
    }
    
    
}
