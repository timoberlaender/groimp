# Vecmath plugin

Vecmath contains an implementation of the javax.vecmath library and an implementation of 3D geometry (primitives, polygon meshes, constructive solid geometry, octree) with the focus on ray-object intersections for the purpose of ray-tracing.
