package de.grogra.persistence;

/**
 * Object that might need to be disposed when the Node that hold the field is
 * removed from the graph. If the object is not a field, ignore.
 */
public interface DisposableField {
	public void disposeField();
}
