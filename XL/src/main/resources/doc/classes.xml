<chapter id="c-classes">
<title>Classes and Interfaces</title>

<para>
On a large scale, classes and interfaces are declared as defined in the
Java Language Specification:
</para>
<productionset><title>Type Declaration</title>
<production id="ebnf.typedecl">
  <lhs>TypeDeclaration</lhs>
  <rhs><nonterminal def="#ebnf.class">ClassDeclaration
       </nonterminal> |
       <nonterminal def="#ebnf.interface">InterfaceDeclaration
       </nonterminal>
  </rhs>
</production>
<production id="ebnf.class">
  <lhs>ClassDeclaration</lhs>
  <rhs><nonterminal def="#ebnf.modifiers">Modifiers</nonterminal>
       <literal>class</literal> Identifier
       [ <literal>extends</literal> <nonterminal def="#ebnf.name">Name</nonterminal> ]
       [ <literal>implements</literal> <nonterminal def="#ebnf.name">Name</nonterminal> {',' <nonterminal def="#ebnf.name">Name</nonterminal> } ]
       '{' { <nonterminal def="#ebnf.classbody">ClassBodyDeclaration</nonterminal> } '}'
  </rhs>
</production>
<production id="ebnf.classbody">
  <lhs>ClassBodyDeclaration</lhs>
  <rhs><nonterminal def="#ebnf.typedecl">TypeDeclaration
       </nonterminal> |
       <nonterminal def="#ebnf.amethod">AbstractMethodDeclaration
       </nonterminal> |
       <nonterminal def="#ebnf.cmethod">ConcreteMethodDeclaration
       </nonterminal> |
       <nonterminal def="#ebnf.imethod">InstancingMethodDeclaration
       </nonterminal> |
       <nonterminal>FieldDeclaration
       </nonterminal> |
       <nonterminal>ConstructorDeclaration
       </nonterminal> |
       <nonterminal>InstanceInitializer
       </nonterminal> |
       <nonterminal>StaticInitializer
       </nonterminal> |
       ';'
  </rhs>
</production>
<production id="ebnf.interface">
  <lhs>InterfaceDeclaration</lhs>
  <rhs><nonterminal def="#ebnf.modifiers">Modifiers</nonterminal>
       <literal>interface</literal> Identifier
       [ <literal>extends</literal> <nonterminal def="#ebnf.name">Name</nonterminal> {',' <nonterminal def="#ebnf.name">Name</nonterminal> } ]
       '{' { <nonterminal def="#ebnf.interfacebody">InterfaceBodyDeclaration</nonterminal> } '}'
  </rhs>
</production>
<production id="ebnf.interfacebody">
  <lhs>InterfaceBodyDeclaration</lhs>
  <rhs><nonterminal def="#ebnf.typedecl">TypeDeclaration
       </nonterminal> |
       <nonterminal def="#ebnf.amethod">AbstractMethodDeclaration
       </nonterminal> |
       <nonterminal>FieldDeclaration
       </nonterminal> |
       ';'
  </rhs>
</production>
</productionset>

<para>
However, when looking at the details,
the XL programming language defines three extensions:
</para>
<itemizedlist>
<listitem><para>
Classes, interfaces, and their members, can be marked
with annotations (<xref linkend="s-annotations"/>)
similarly to annotations as specified by the Java Language
Specification, Third Edition.
</para></listitem>
<listitem><para>
Classes and interfaces may contain declarations of generator
methods (<xref linkend="s-generator-methods"/>).
Generator methods successively yield values to their invoker, instead
of returning just a single value.
</para></listitem>
<listitem><para>
Classes and interfaces may contain declarations of operator
methods (<xref linkend="s-operator-methods"/>).
Operator methods follow a special syntax and are used for the
overloading of operators.
</para></listitem>
<listitem><para>
Classes may contain declarations of instancing
methods (<xref linkend="s-instancing-methods"/>). They are useful
for the implementation of object instancing which is common in
3D computer graphics.
</para></listitem>
</itemizedlist>

<section id="s-annotations"><title>Annotations</title>

<para>
The purpose of an <firstterm>annotation</firstterm>
is to associate information with
a program element. Annotations are used as modifiers in declarations
of classes, interfaces, fields, methods, and constructors. The
associated information is an instance of an
<firstterm>annotation type</firstterm>.
The annotations of the XL programming language are similar to the annotations
defined in the Java Language Specification, Third Edition; the syntax
of annotations is exactly the same.
</para>
<para>
An annotation type is a concrete, public class
<replaceable>A</replaceable> with a public constructor with no parameters
which implements the interface
<classname>de.grogra.reflect.Annotation</classname>.
An annotation type defines a number of <firstterm>elements</firstterm>:
</para>
<itemizedlist>
<listitem><para>
<replaceable>A</replaceable> has an element named
<replaceable>e</replaceable> if and only if <replaceable>A</replaceable>
has as member a public and non-static method with a single parameter and the
name <methodname>set_</methodname><replaceable>e</replaceable>
or <methodname>init_</methodname><replaceable>e</replaceable>. It is
a compile-time error if several such methods with
differing parameter types exist; <replaceable>A</replaceable> is no
legal annotation type in this case.
</para></listitem>
<listitem><para>
The type of the element <replaceable>e</replaceable> is the type
of the method parameter. If a method named
<methodname>set_</methodname><replaceable>e</replaceable> exists
as a member of <replaceable>A</replaceable>, the element is an
<firstterm>element with default-value</firstterm>. Otherwise, only a method
named <methodname>init_</methodname><replaceable>e</replaceable> exists
as a member of <replaceable>A</replaceable>, and the element is an
<firstterm>element without default-value</firstterm>.
</para></listitem>
</itemizedlist>
<para>
Now an <firstterm>annotation</firstterm> is a modifier consisting of the
token <literal>@</literal>, the
name of an <firstterm>annotation type</firstterm>, and zero or more
element-value pairs, each of which associates a value with a different
element of the annotation type:
</para>
<productionset><title>Modifiers and Annotations</title>
<production id="ebnf.modifiers">
  <lhs>Modifiers</lhs>
  <rhs>{ <nonterminal def="#ebnf.modifier">Modifier</nonterminal> }
  </rhs>
</production>
<production id="ebnf.modifier">
  <lhs>Modifier</lhs>
  <rhs><nonterminal def="#ebnf.annotation">Annotation
       </nonterminal> |
       <literal>public</literal> |
       <literal>protected</literal> |
       <literal>private</literal> |
       <literal>abstract</literal> |
       <literal>static</literal> |
       <literal>final</literal> |
       <literal>synchronized</literal> |
       <literal>native</literal> |
       <literal>strictfp</literal> |
       <literal>transient</literal> |
       <literal>volatile</literal> |
       <literal>const</literal>
  </rhs>
</production>
<production id="ebnf.annotation">
  <lhs>Annotation</lhs>
  <rhs><nonterminal def="#ebnf.normalannot">NormalAnnotation
       </nonterminal> |
       <nonterminal def="#ebnf.markerannot">MarkerAnnotation
       </nonterminal> |
       <nonterminal def="#ebnf.singleannot">SingleElementAnnotation
       </nonterminal>
  </rhs>
</production>
<production id="ebnf.normalannot">
  <lhs>NormalAnnotation</lhs>
  <rhs>'@' <nonterminal def="#ebnf.name">Name</nonterminal>
       '(' [ <nonterminal def="#ebnf.evpairs">ElementValuePairs</nonterminal> ] ')'
  </rhs>
</production>
<production id="ebnf.markerannot">
  <lhs>MarkerAnnotation</lhs>
  <rhs>'@' <nonterminal def="#ebnf.name">Name</nonterminal>
  </rhs>
</production>
<production id="ebnf.singleannot">
  <lhs>SingleElementAnnotation</lhs>
  <rhs>'@' <nonterminal def="#ebnf.name">Name</nonterminal>
       '(' <nonterminal def="#ebnf.elementvalue">ElementValue</nonterminal> ')'
  </rhs>
</production>
<production id="ebnf.evpairs">
  <lhs>ElementValuePairs</lhs>
  <rhs><nonterminal def="#ebnf.evpair">ElementValuePair</nonterminal>
       { ',' <nonterminal def="#ebnf.evpair">ElementValuePair</nonterminal> }
  </rhs>
</production>
<production id="ebnf.evpair">
  <lhs>ElementValuePair</lhs>
  <rhs>Identifier '=' <nonterminal def="#ebnf.elementvalue">ElementValue</nonterminal>
  </rhs>
</production>
<production id="ebnf.elementvalue">
  <lhs>ElementValue</lhs>
  <rhs><nonterminal def="#ebnf.annotation">Annotation
       </nonterminal> |
       <nonterminal def="#ebnf.exprnoassignment">ExpressionNoAssignment
       </nonterminal> |
       <nonterminal def="#ebnf.evarrayinit">ElementValueArrayInitializer
       </nonterminal>
  </rhs>
</production>
<production id="ebnf.evarrayinit">
  <lhs>ElementValueArrayInitializer</lhs>
  <rhs>'{' [ <nonterminal def="#ebnf.elementvalues">ElementValues
       </nonterminal> ] [ ',' ] '}'
  </rhs>
</production>
<production id="ebnf.elementvalues">
  <lhs>ElementValues</lhs>
  <rhs><nonterminal def="#ebnf.elementvalue">ElementValue
       </nonterminal>
       { ',' <nonterminal def="#ebnf.elementvalue">ElementValue
       </nonterminal> }
  </rhs>
</production>
</productionset>

<para>
The identifier in an element-value pair of a normal annotation
must be the simple name of one of
the elements of the annotation type. The value
may only be composed of constants of primitive type
or <classname>String</classname>, class literals, annotations, and arrays
of those values (using the production
<link linkend="ebnf.evarrayinit">ElementValueArrayInitializer</link>).
The value is associated with the element, or,
if the element type is an array type and the value is not an
array, the associated value is an array whose sole element is the
original value. It is a compile-time error if the value to associate
is not assignable to the element type.
</para>
<para>
A marker annotation is treated as a normal annotation with no element-value
pairs. A single-element annotation is treated as a normal annotation
with a single element-value pair where the element identifier is
<literal>value</literal> and the value is the provided value.
</para>
<para>
It is a compile-time error if more than one value is associated with a single
element, or if no value is associated with an element without default-value.
</para>

<para>
As examples for a normal annotation, a marker annotation, and a single-element
annotation, consider the following annotated declarations of fields, which
could be interpreted by a graphical user interface in order to generate
suitable input components:
<programlisting>
@Range(min=0, max=360) private float angle;
@Editable String name;
@Choice({"Center", "Left", "Right"}) int alignment;
</programlisting>
</para>


</section>

<section><title>Method Declarations</title>

<productionset><title>Method Declaration</title>
<production id="ebnf.amethod">
  <lhs>AbstractMethodDeclaration</lhs>
  <rhs><nonterminal def="#ebnf.modifiers">Modifiers</nonterminal>
       <nonterminal def="#ebnf.result">ResultType</nonterminal>
       Identifier [ <nonterminal def="#ebnf.overloadop">OverloadableOperator</nonterminal> ]
       '(' <nonterminal>FormalParameterList</nonterminal> ')'
       [ <nonterminal>Throws</nonterminal> ] ';'
  </rhs>
</production>
<production id="ebnf.cmethod">
  <lhs>ConcreteMethodDeclaration</lhs>
  <rhs><nonterminal def="#ebnf.modifiers">Modifiers</nonterminal>
       <nonterminal def="#ebnf.result">ResultType</nonterminal>
       Identifier [ <nonterminal def="#ebnf.overloadop">OverloadableOperator</nonterminal> ]
       '(' <nonterminal>FormalParameterList</nonterminal> ')'
       [ <nonterminal>Throws</nonterminal> ]
       <nonterminal def="#ebnf.block">Block</nonterminal>
  </rhs>
</production>
<production id="ebnf.imethod">
  <lhs>InstancingMethodDeclaration</lhs>
  <rhs><nonterminal def="#ebnf.modifiers">Modifiers</nonterminal>
       <literal>void</literal>
       Identifier
       '(' <nonterminal>FormalParameterList</nonterminal> ')'
       [ <nonterminal>Throws</nonterminal> ]
       '==>' { <nonterminal def="#ebnf.instantiation">Instantiation</nonterminal> } ';'
  </rhs>
</production>
<production id="ebnf.result">
  <lhs>ResultType</lhs>
  <rhs><nonterminal def="#ebnf.type">Type</nonterminal> [ '*' ]
       | <literal>void</literal>
  </rhs>
</production>
<production id="ebnf.overloadop">
  <lhs>OverloadableOperator</lhs>
  <rhs> '!' | '~' | '+' | '-' | '*' | '/' | '%' | '**'
  | '+=' | '-=' | '*=' | '/=' | '%='
  | '&lt;&lt;' | '>>' | '>>>'
  | '&lt;&lt;=' | '>>=' | '>>>='
  | '^' | '|' | '&amp;' | '||' | '&amp;&amp;'
  | '^=' | '|=' | '&amp;='
  | '++' | '--'
  | '==' | '!=' | '>' | '>=' | '&lt;' | '&lt;=' | '&lt;=>'
  | <literal>in</literal> | '::' | ':' | '[' ']'
  </rhs>
</production>
</productionset>
<para>
Methods are declared as members of classes and interfaces. The declaration
is similar to a declaration in the Java programming language, with
the following differences:
</para>
<itemizedlist>
<listitem><para>
The Java Language Specification allows a declaration for a method
that returns an array to place some or all of the empty bracket pairs
of the return type after the parameter list, as in
<programlisting>
String getNames() [] {...}
</programlisting>
As the Java Language Specification says, this feature should not be used
in new code. The XL programming language does not allow the placing
of bracket pairs after the parameter list, it would be confused
with transformation blocks.
</para></listitem>
<listitem><para>
<link linkend="s-generator-methods">Generator method declarations</link>
are indicated by an asterisk after the result type.
Generator methods successively yield values to their invoker, instead
of returning just a single value.
</para></listitem>
<listitem><para>
<link linkend="s-operator-methods">Operator method declarations</link>
are indicated by the identifier <literal>operator</literal>, followed
by the token of one of the overloadable operators. It is a compile-time
error if an operator token follows an identifier which is not equal to
the character sequence <literal>operator</literal>.
</para></listitem>
<listitem><para>
<link linkend="s-instancing-methods">Instancing method declarations</link>
are allowed in classes, they are indicated by the arrow <literal>==></literal>
as the first token of their body. They are useful
for the implementation of object instancing which is common in
3D computer graphics.
</para></listitem>
</itemizedlist>

<section id="s-generator-methods"><title>Generator Method Declarations</title>

<para>
<firstterm>Generator method declarations</firstterm> are possible within
classes and interfaces. A generator method declaration is indicated
by an asterisk after the result type as in the following example:
</para>
<programlisting>
char* chars(CharSequence s) {
  for (int i = 0; i &lt; s.length(); i++) {
    yield s.charAt(i);
  }
}
</programlisting>
<para>
Generator methods successively yield values to their invoker via
the <literal>yield</literal>-statement
(<xref linkend="s-yield"/>, <xref linkend="s-gmethod-invocation"/>),
in contrast to normal (non-generator) methods which return a single value
via the <literal>return</literal>-statement. The asterisk can be seen
as meaning &quot;zero or more values&quot; as in regular expressions.
The body of a generator method must not contain any
<literal>return</literal>-statement that returns a value.
</para>
<para>
The declaration of a generator method is not as difficult as it may seem:
Basically, it is syntactic sugar. A declaration of a generator method
is equivalent to the declaration of a normal method, where
its header is the header of the generator method,
plus an additional parameter <replaceable>c</replaceable> inserted at
the beginning of the formal parameter list. The type of
<replaceable>c</replaceable> is a consumer class in the package
<classname>de.grogra.xl.lang</classname>: If <replaceable>T</replaceable>
is the result type of the generator method, let <replaceable>S</replaceable>
be the type affix (<xref linkend="s-typeaffix"/>) of
<replaceable>T</replaceable>, then the consumer class is
<replaceable>S</replaceable>Consumer. For the example, the
equivalent method header is
<literal>char chars(CharConsumer c, CharSequence s)</literal>,
and a complete and equivalent method declaration looks as follows:
<programlisting>
char chars(CharConsumer c, CharSequence s) {
  for (int i = 0; i &lt; s.length(); i++) {
    c.consume(s.charAt(i));
  }
  return 0;
}
</programlisting>
</para>
<para>
In fact, when this specification refers to the signature of a method,
the signature of this equivalent method declaration has to be used
in the case of a generator method declaration.
</para>

</section>

<section id="s-operator-methods"><title>Operator Method Declarations</title>

<para>
<firstterm>Operator method declarations</firstterm> are possible within
classes and interfaces. An operator method declaration is indicated
by the identifier <literal>operator</literal>, followed
by the token (or tokens in case of <literal>[]</literal>)
of one of the overloadable operators as in the following example:
</para>
<programlisting>
Complex operator+ (Complex b) {
  return new Complex(this.real + b.real, this.imag + b.imag);
}
</programlisting>
<para>
Note that regarding the lexical structure,
<literal>operator</literal> is an identifier, not a keyword.
However, it is the only identifier for a method declaration
which is allowed to be followed by the token of an overloadable
operator. Thus, it is a compile-time
error if an operator token follows an identifier which is not equal to
the character sequence <literal>operator</literal>.
</para>
<para>
The declaration of an operator method is equivalent to the declaration
of a normal method, its name being derived from the operator by
the following rules. At first, the arity of the method
declaration is determined as follows: If the declared method
is static, the arity is the number of its parameters.
Otherwise, the arity is the number of its parameters plus one.
An operator method declaration whose arity is one is called a
<firstterm>unary method declaration</firstterm>, a declaration
whose arity is two is called a
<firstterm>binary method declaration</firstterm>.
</para>
<para>
For unary method declarations, the equivalent method name is derived
as defined by the following table.
If no identifier for the operator token is defined,
a compile-time error occurs.
</para>
<table frame="all"><title>Names of unary operator methods</title>
<tgroup cols="2" align="left" colsep='1' rowsep='1'>
<thead>
<row>
  <entry>Operator token</entry>
  <entry>Derived name</entry>
</row>
</thead>
<tbody>
<row>
  <entry><literal>!</literal></entry>
  <entry><literal>operator$not</literal></entry>
</row>
<row>
  <entry><literal>~</literal></entry>
  <entry><literal>operator$com</literal></entry>
</row>
<row>
  <entry><literal>+</literal></entry>
  <entry><literal>operator$pos</literal></entry>
</row>
<row>
  <entry><literal>-</literal></entry>
  <entry><literal>operator$neg</literal></entry>
</row>
<row>
  <entry><literal>++</literal></entry>
  <entry><literal>operator$inc</literal></entry>
</row>
<row>
  <entry><literal>--</literal></entry>
  <entry><literal>operator$dec</literal></entry>
</row>
</tbody>
</tgroup>
</table>
<para>
For binary method declarations, the equivalent method name is derived
as defined by the following table.
If no identifier for the operator token is defined,
a compile-time error occurs.
</para>
<table frame="all"><title>Names of binary operator methods</title>
<tgroup cols="2" align="left" colsep='1' rowsep='1'>
<thead>
<row>
  <entry>Operator token</entry>
  <entry>Derived name</entry>
</row>
</thead>
<tbody>
<row>
  <entry><literal>+</literal></entry>
  <entry><literal>operator$add</literal></entry>
</row>
<row>
  <entry><literal>-</literal></entry>
  <entry><literal>operator$sub</literal></entry>
</row>
<row>
  <entry><literal>*</literal></entry>
  <entry><literal>operator$mul</literal></entry>
</row>
<row>
  <entry><literal>/</literal></entry>
  <entry><literal>operator$div</literal></entry>
</row>
<row>
  <entry><literal>%</literal></entry>
  <entry><literal>operator$rem</literal></entry>
</row>
<row>
  <entry><literal>&lt;&lt;</literal></entry>
  <entry><literal>operator$shl</literal></entry>
</row>
<row>
  <entry><literal>>></literal></entry>
  <entry><literal>operator$shr</literal></entry>
</row>
<row>
  <entry><literal>>>></literal></entry>
  <entry><literal>operator$ushr</literal></entry>
</row>
<row>
  <entry><literal>^</literal></entry>
  <entry><literal>operator$xor</literal></entry>
</row>
<row>
  <entry><literal>|</literal></entry>
  <entry><literal>operator$or</literal></entry>
</row>
<row>
  <entry><literal>&amp;</literal></entry>
  <entry><literal>operator$and</literal></entry>
</row>
<row>
  <entry><literal>||</literal></entry>
  <entry><literal>operator$cor</literal></entry>
</row>
<row>
  <entry><literal>&amp;&amp;</literal></entry>
  <entry><literal>operator$cand</literal></entry>
</row>
<row>
  <entry><replaceable>op</replaceable><literal>=</literal> where
<replaceable>op</replaceable> is one of the preceding tokens</entry>
  <entry>derived name of <replaceable>op</replaceable>,
suffixed by the character sequence <literal>Assign</literal></entry>
</row>
<row>
  <entry><literal>**</literal></entry>
  <entry><literal>operator$pow</literal></entry>
</row>
<row>
  <entry><literal>==</literal></entry>
  <entry><literal>operator$eq</literal></entry>
</row>
<row>
  <entry><literal>!=</literal></entry>
  <entry><literal>operator$neq</literal></entry>
</row>
<row>
  <entry><literal>></literal></entry>
  <entry><literal>operator$gt</literal></entry>
</row>
<row>
  <entry><literal>>=</literal></entry>
  <entry><literal>operator$ge</literal></entry>
</row>
<row>
  <entry><literal>&lt;</literal></entry>
  <entry><literal>operator$lt</literal></entry>
</row>
<row>
  <entry><literal>&lt;=</literal></entry>
  <entry><literal>operator$le</literal></entry>
</row>
<row>
  <entry><literal>&lt;=></literal></entry>
  <entry><literal>operator$cmp</literal></entry>
</row>
<row>
  <entry><literal>in</literal></entry>
  <entry><literal>operator$in</literal></entry>
</row>
<row>
  <entry><literal>::</literal></entry>
  <entry><literal>operator$guard</literal></entry>
</row>
<row>
  <entry><literal>:</literal></entry>
  <entry><literal>operator$range</literal></entry>
</row>
<row>
  <entry><literal>[]</literal></entry>
  <entry><literal>operator$index</literal></entry>
</row>
<row>
  <entry><literal>++</literal></entry>
  <entry><literal>operator$postInc</literal></entry>
</row>
<row>
  <entry><literal>--</literal></entry>
  <entry><literal>operator$postDec</literal></entry>
</row>
</tbody>
</tgroup>
</table>
<para>
The last parameter of binary method declarations for the operators
<literal>++</literal> and <literal>--</literal> must have type
<classname>int</classname>, or a compile-time error occurs.
</para>
<para>
If the arity is greater than two and the operator tokens
are <literal>[]</literal>, the equivalent method name is
<literal>operator$index</literal>. Any case which is not handled
by the previous definitions leads to a compile-time error.
</para>

</section>

<section id="s-instancing-methods">
<title>Instancing Method Declarations</title>

<para>
<firstterm>Instancing method declarations</firstterm> are possible
within classes. Such a declaration provides syntactic sugar for the
implementation of object instancing, which is a common technique
in 3D computer graphics (but this specification
makes no reference to this concrete usage). An instancing method
declaration is indicated by the arrow <literal>==></literal>, the
result type has to be <literal>void</literal>.
</para>
<para>
At class level,
a declaration of an instancing method is equivalent to a declaration
of a normal (non-instancing) method, where its header is the header
of the instancing method, plus an additional parameter
<replaceable>s</replaceable> inserted at the beginning of the
formal parameter list. The type of
<replaceable>s</replaceable> is the type returned by the method
<methodname>getInstantiationStateClass</methodname> of the current
compile-time model (<xref linkend="s-ctmodel"/>). The compile-time
model has to return a subclass of
<classname>de.grogra.xl.lang.InstantiationState</classname>.
</para>
<para>
In fact, when this specification refers to the signature of methods,
the signature of this equivalent method declaration has to be used
in the case of an instancing method declaration.
</para>

</section>

</section>

</chapter>
