package de.grogra.pf.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.List;

/**
 * Probably shouldn't exists. The implementations of UITransferHandler 
 * are in additional plugins. This one is useless
 */
public class UITransferHandlerImpl implements UITransferHandler {

	List<UIDataHandler> dh;
	
	@Override
	public void setDataHandlers(List<UIDataHandler> d) {
		this.dh = d;		
	}

	@Override
	public DataFlavor[] getDataFlavors() {
		return dh.stream().map(c -> c.getDataFlavors()).toArray(DataFlavor[]::new);
	}

	@Override
	public UITransferable createTransferable(Object source) {
		return null;
	}

	@Override
	public Object loadData(Object t) {
		return null;
	}

	@Override
	public UIDataHandler getMostSuitedHandler(Transferable t) {
		return null;
	}
	
	@Override
	public List<UIDataHandler> getHandlers(){
		return new ArrayList<UIDataHandler>();
	}

}
