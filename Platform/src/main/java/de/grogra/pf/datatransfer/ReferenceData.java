package de.grogra.pf.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import de.grogra.graph.impl.Node;
import de.grogra.persistence.ShareableBase;
import de.grogra.util.Described;

/**
 * Handle Items that will be turned into String wrapped with the 
 * ReferenceName string.
 */
public class ReferenceData extends UIDataHandler {

	String reference;
	//enh:field 
	
	@Override
	public Object loadData(Object t) {
		if (t instanceof UITransferable) {
			Object i = ((UITransferable)t).getContent();
			return reference + ((i instanceof Node) ? ((Node)i).getName() : "")+"\")";
		}
		else if (t instanceof Transferable) {
			try {
				String name= (String) ((Transferable)t).getTransferData(UITransferable.DISPLAYNAME);
				return reference + "(\"" +name +"\")";
			} catch (UnsupportedFlavorException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			for (DataFlavor d : getDataFlavors()) {
				if (!(d.equals(UITransferable.SOURCE)) && ((Transferable)t).isDataFlavorSupported(d)) {
					try {
						Object i = ((Transferable)t).getTransferData(d);
						String name = null;
						if (i instanceof Described) {
							name = (String) ((Described)i).getDescription(NAME);
						} else if (i instanceof ShareableBase &&((ShareableBase)i).getProvider() instanceof Described) {
									name = (String) ((Described)((ShareableBase)i).getProvider()).getDescription(NAME);							
						} else {
							name = ((i instanceof Node) ? ((Node)i).getName() : "");
						}
						return reference + "(\"" +name +"\")";
					} catch (UnsupportedFlavorException | IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field reference$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (ReferenceData.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((ReferenceData) o).reference = (String) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((ReferenceData) o).reference;
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new ReferenceData ());
		$TYPE.addManagedField (reference$FIELD = new _Field ("reference", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 0));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new ReferenceData ();
	}

//enh:end
}
