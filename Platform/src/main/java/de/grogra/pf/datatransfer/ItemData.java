package de.grogra.pf.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import de.grogra.pf.registry.Item;

/**
 * The data handler that manage swing tree nodes. The data loaded is type Item.
 */
public class ItemData extends UIDataHandler {

	public ItemData() {
		super(null);
	}
	
	@Override
	public Object loadData(Object t) {
		if (t instanceof UITransferable) {
			Object i = ((UITransferable)t).getContent();
			return (i instanceof Item)? i : null;
		}
		else if (t instanceof Transferable) {
			for (DataFlavor d : getDataFlavors()) {
				if (!(d.equals(UITransferable.SOURCE)) && ((Transferable)t).isDataFlavorSupported(d)) {
					try {
						Object i = ((Transferable)t).getTransferData(d);
						return (i instanceof Item)? i : null;
					} catch (UnsupportedFlavorException | IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new ItemData ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new ItemData ();
	}

//enh:end

}
