package de.grogra.pf.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.util.Comparator;

public class DataFlavorComparator implements Comparator<DataFlavor> {

	final public static DataFlavorComparator COMPARATOR = new DataFlavorComparator();
	
	@Override
	public int compare(DataFlavor o1, DataFlavor o2) {
		if(o1.getRepresentationClass().isAssignableFrom(o2.getRepresentationClass()) ) {
			return -1;
		} else if(o2.getRepresentationClass().isAssignableFrom(o1.getRepresentationClass()) ) {
			return 1;
		}
		return 0;
	}

}
