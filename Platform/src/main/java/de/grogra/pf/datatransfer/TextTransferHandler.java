package de.grogra.pf.datatransfer;

import java.awt.Component;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.im.InputContext;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.TransferHandler;

import de.grogra.pf.ui.autocomplete.AutoCompletableTextArea;

/**
 * TransferHandler that manage in data for text area
 */
public class TextTransferHandler extends TransferHandler implements UITransferHandler {

	List<UIDataHandler> handlers;
	
	@Override
	public void setDataHandlers(List<UIDataHandler> d) {
		this.handlers = d;
	}

	@Override
	public DataFlavor[] getDataFlavors() {
		return handlers.stream().map(c -> c.getDataFlavors()).toArray(DataFlavor[]::new);
	}

	@Override
	public UITransferable createTransferable(Object source) {
		return null;
	}

	@Override
	public List<UIDataHandler> getHandlers(){
		return handlers;
	}
	
	@Override
	public boolean canImport(TransferHandler.TransferSupport support) {
		if (handlers == null) { return false; }
		if(!support.isDrop()) {
			return false;
		}
		support.setShowDropLocation(true);

		boolean canImport = false;
		for (UIDataHandler h : handlers) {
			if (h.isDataFlavorSupported(support.getDataFlavors())) {
				canImport = true;
				break;
			}
		}
		if(!canImport) {
			return false;
		}
		return true;
	}

	public boolean importData(TransferHandler.TransferSupport support) {
		if(!canImport(support)) {
			return false;
		}
		if (!(support.getTransferable() instanceof Transferable)) {
			return false;
		}
		Object text = loadData((Transferable) support.getTransferable());
		if (!(text instanceof String)) { return false; }

		Component c = support.getComponent();
		InputContext ic = c.getInputContext();
        if (ic != null) {
            ic.endComposition();
        }
		if (c instanceof AutoCompletableTextArea) {
			//TODO: with jEdit there is a small offset (few characters shift when this is used. 
			// fix it and the drop can be at the mouse location (and not the caret location).
			// aka move the caret before inserting
			if (!(c instanceof JTextArea)) { //JTextArea manage the caret position automatically
				Point p =support.getDropLocation().getDropPoint();
	        	((AutoCompletableTextArea)c).moveCaretPositionFromXY(p.x, p.y);
	        }
			((AutoCompletableTextArea)c).replaceSelection((String)text);
		}
		
		return true;
	}

}
