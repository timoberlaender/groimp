/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.poi.hpsf.Property;
import org.apache.poi.hpsf.PropertySet;
import org.apache.poi.hpsf.Section;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hpsf.Variant;
import org.apache.poi.hpsf.WritingNotSupportedException;
import org.apache.poi.hpsf.wellknown.PropertyIDMap;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFooter;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.OutputStreamSource;
import de.grogra.util.MimeType;
//import de.grogra.util.MimeType;

public class ExcelWriter extends FilterBase implements OutputStreamSource {

	public static final IOFlavor FLAVOR = new IOFlavor (new MimeType ("application/vnd.ms-excel", null), IOFlavor.OUTPUT_STREAM, null);

	public ExcelWriter(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(FLAVOR);
	}

	private static String makeValidString (String s)
	{
		StringBuffer b = new StringBuffer (s);
		for (int i = 0; i < b.length (); i++)
		{
			switch (b.charAt (i))
			{
				case '/':
				case '\\':
				case '*':
				case '?':
				case '[':
				case ']':
					b.setCharAt (i, '_');
					break;
			}
		}
		return b.toString ();
	}

	public void write(OutputStream out) throws IOException {
		Dataset ds = (Dataset) ((ObjectSource) source).getObject();
		ByteArrayOutputStream baos;
		int column = ds.getColumnCount();
		int row = ds.getRowCount();
		
		HSSFWorkbook document = new HSSFWorkbook();
		baos=new ByteArrayOutputStream();
		HSSFSheet sheet1 = document.createSheet(makeValidString (ds.getTitle()));
		sheet1.setZoom(9,10);
		sheet1.setAutobreaks(true);
		sheet1.setFitToPage(true);
		HSSFPrintSetup ps=sheet1.getPrintSetup();
		ps.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
		ps.setLandscape(true);
		ps.setFitWidth((short)1);
		HSSFFooter footer=sheet1.getFooter();
		footer.setLeft("Created by GroIMP");
		footer.setCenter(ds.getTitle());
		footer.setRight("Page "+HSSFFooter.page()+"/"+HSSFFooter.numPages());
		HSSFCellStyle style=document.createCellStyle();
		HSSFFont font=document.createFont();
		style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_25_PERCENT.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setBorderBottom(BorderStyle.THIN);
		style.setBottomBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
		style.setBorderTop(BorderStyle.THIN);
		style.setTopBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
		style.setBorderLeft(BorderStyle.THIN);
		style.setLeftBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
		style.setBorderRight(BorderStyle.THIN);
		style.setRightBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
		font.setBold(true);
		font.setColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
		style.setFont(font);
		
		HSSFRow rowHead = sheet1.createRow((short)0);
		HSSFCell cell;
		for(int i = 0; i < column; i++) {
			cell=rowHead.createCell((short)i);
			sheet1.setColumnWidth((short)i,(short)((30)/((double)1/256)));
			cell.setCellStyle(style);
			cell.setCellValue(new HSSFRichTextString(ds.getColumnKey(i).toString()));
		}
		
		HSSFCellStyle standardCellStyle=document.createCellStyle();
		standardCellStyle.setBorderBottom(BorderStyle.THIN);
		standardCellStyle.setBottomBorderColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
		standardCellStyle.setBorderLeft(BorderStyle.THIN);
		standardCellStyle.setLeftBorderColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
		standardCellStyle.setBorderRight(BorderStyle.THIN);
		standardCellStyle.setRightBorderColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
		style=document.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBottomBorderColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
		style.setBorderLeft(BorderStyle.THIN);
		style.setLeftBorderColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
		style.setBorderRight(BorderStyle.THIN);
		style.setRightBorderColor(HSSFColor.HSSFColorPredefined.GREY_40_PERCENT.getIndex());
		//style.setDataFormat(HSSFDataFormat.getBuiltinFormat("d-mmm-yy"));
		short rowCounter = 1;
		
		for(int i = 0; i < row; i++) {
			rowHead=sheet1.createRow(rowCounter++);
			for(int j = 0; j < column; j++) {
				cell=rowHead.createCell((short)j);
				cell.setCellValue(ds.getCell(i, j).getX());
				cell.setCellStyle(standardCellStyle);
			}
		}
		
		document.write(baos);
		
		try {
			PropertySet mps=new PropertySet();
			Section ms=(Section)mps.getFirstSection();
			ms.setFormatID(SummaryInformation.FORMAT_ID);
			Property[] p=new Property[5];
			for(int i=0;i<5;i++)
				p[i]=new Property();
			p[0].setID(PropertyIDMap.PID_TITLE);
			p[0].setType(Variant.VT_LPWSTR);
			p[0].setValue(ds.getTitle());

			p[1].setID(PropertyIDMap.PID_AUTHOR);
			p[1].setType(Variant.VT_LPWSTR);
			p[1].setValue("GroIMP");

			p[2].setID(PropertyIDMap.PID_SUBJECT);
			p[2].setType(Variant.VT_LPWSTR);
			p[2].setValue(ds.getTitle());

			p[3].setID(PropertyIDMap.PID_LASTAUTHOR);
			p[3].setType(Variant.VT_LPWSTR);
			p[3].setValue("GroIMP");

			p[4].setID(PropertyIDMap.PID_APPNAME);
			p[4].setType(Variant.VT_LPWSTR);
			p[4].setValue("Microsoft Excel");

			ms.setProperties(p);
			
			POIFSFileSystem poiFs=new POIFSFileSystem();
			InputStream is;
			is = mps.toInputStream();
			poiFs.createDocument(is,SummaryInformation.DEFAULT_STREAM_NAME);
			
			poiFs.writeFilesystem(baos);
		} catch (WritingNotSupportedException e) {}
		
		baos.writeTo(out);
		baos.close();
		
	}

}
