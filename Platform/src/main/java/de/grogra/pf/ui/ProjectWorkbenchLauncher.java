package de.grogra.pf.ui;

import de.grogra.pf.registry.Registry;


public interface ProjectWorkbenchLauncher {
	

	/**
	 * Create a new ProjectWorkbench with the child Registry of the Project and set the Project
	 */
	public ProjectWorkbench create(Project p);
	/**
	 * create a new ProjectWorkbench with a given Registry
	 */
	public ProjectWorkbench create(Registry r);
}
