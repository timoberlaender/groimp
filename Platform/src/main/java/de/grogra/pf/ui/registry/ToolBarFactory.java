
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.ui.registry;

import de.grogra.pf.registry.*;
import de.grogra.pf.ui.*;
import de.grogra.util.Map;
import de.grogra.util.StringMap;

public class ToolBarFactory extends PanelFactory
{
//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new ToolBarFactory ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new ToolBarFactory ();
	}

//enh:end

	ToolBarFactory ()
	{
		super ();
	}


	@Override
	protected Panel configure (Context ctx, Panel p, Item menu)
	{
		if (p == null)
		{
			StringMap sm = new StringMap( this);
			sm = addDataHandlerToMap(sm);

			p = UIToolkit.get (ctx).createToolBar (ctx, sm);
		}
		if (menu != null)
		{
			UI.setMenu (p, menu, null);
		}
		return p;
	}

}
