package de.grogra.pf.ui.autocomplete;

import java.lang.reflect.InvocationTargetException;

import org.xml.sax.SAXException;

import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.autocomplete.impl.AbstractAutoCompletor;
import de.grogra.util.Utils;

/**
 * Registry Item that create an AutoCompletor. Which can then be installed on a TextPanel
 */
public class AutoCompletorFactory extends Item
{
	String create;

	//enh: insert
	
	public static final NType $TYPE
		= (NType) new NType (new AutoCompletorFactory ()).validate (); 



	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new AutoCompletorFactory ();
	}


	AutoCompletorFactory ()
	{
		super (null);
	}


	@Override
	protected boolean readAttribute (String uri, String name, String value)
		throws SAXException
	{
		if ("".equals (uri))
		{
			if ("create".equals (name))
			{
				create = value;
				return true;
			}
		}
		return super.readAttribute (uri, name, value);
	}


	public AbstractAutoCompletor getAutoCompletor () throws ClassNotFoundException, IllegalAccessException,
		InstantiationException, InvocationTargetException,
		NoSuchMethodException
	{
		return (AbstractAutoCompletor) Utils.invoke (create, null, getClassLoader ());
	}
}