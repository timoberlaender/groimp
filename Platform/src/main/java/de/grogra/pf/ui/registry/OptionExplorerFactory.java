/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.ui.registry;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.nio.charset.StandardCharsets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.tree.TreePath;

import de.grogra.pf.registry.Directory;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Option;
import de.grogra.pf.registry.OptionFileBase;
import de.grogra.pf.registry.Registry;

import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.ComponentWrapper;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.edit.OptionsSelection;
import de.grogra.pf.ui.event.ActionEditEvent;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.pf.ui.Widget;
import de.grogra.pf.ui.tree.HierarchyFlattener;
import de.grogra.pf.ui.tree.RegistryAdapter;
import de.grogra.pf.ui.tree.SyncMappedTree;
import de.grogra.pf.ui.tree.UITreePipeline;
import de.grogra.pf.ui.tree.UITreePipeline.Node;
import de.grogra.pf.ui.util.ComponentWrapperImpl;
import de.grogra.util.DisposableWrapper;
import de.grogra.util.MappedTree;
import de.grogra.util.StringMap;
import de.grogra.xl.util.ObjectList;

public class OptionExplorerFactory extends PanelFactory
{

	private ComponentWrapper t;
	private UITreePipeline tp;
	private FilterTransformer ft;
	private OptionTree opt;
	private String path;
	//enh:field
	
//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field path$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (OptionExplorerFactory.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((OptionExplorerFactory) o).path = (String) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((OptionExplorerFactory) o).path;
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new OptionExplorerFactory ());
		$TYPE.addManagedField (path$FIELD = new _Field ("path", _Field.PRIVATE  | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 0));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new OptionExplorerFactory ();
	}

//enh:end

	class OptionTree extends RegistryAdapter
	{
		private Object editorComponent;
		private DisposableWrapper dw;
		private UIToolkit ui;
		private  Object container ;
		OptionTree (Context c, Registry r,DisposableWrapper dw,UIToolkit ui, Object container )
		{
			super (c, r);
			this.dw=dw;
			this.ui=ui;
			this.container=container;
		}

		@Override
		public int getType (Object node)
		{
			int t = super.getType (node);
			return (t == NT_UNDEFINED) ? NT_ITEM : t;
		}

		@Override
		public void eventOccured (final Object node,
								  java.util.EventObject event)
		{
			if (!(event instanceof ActionEditEvent))
			{
				super.eventOccured (node, event);
				return;
			}
			Item item = (Item) node;
			if (((ActionEditEvent) event).isConsumed ()
				|| !item.hasEditableOptions ())
			{
				return;
			}
			((ActionEditEvent) event).consume ();
			setItem (item);
		}
		
		void setItem (Item item)
		{
			if(item instanceof Directory && ((Directory)item).isOptionCategory()) {
			
			
			}else {
			if (dw.disposable != null)
			{
				ui.removeComponent (editorComponent);
				dw.dispose ();
				editorComponent = null;
			}
			ComponentWrapper w = new OptionsSelection
				 (getContext (), item, true)
				 .createPropertyEditorComponent ();
			if (w != null)
			{
				dw.disposable = w;
				ui.addComponent (container, editorComponent = w.getComponent (), null);
			}
			ui.revalidate (container);
		}
		}
	}
	
	
	OptionExplorerFactory ()
	{
		super ();
	}


	@Override
	protected Panel configure (Context ctx, Panel p, Item menu)
	{
		if (p == null)
		{
			Registry rr = ctx.getWorkbench ().getRegistry ().getRootRegistry ();
			tp = new UITreePipeline ();
			tp.add (new HierarchyFlattener (null, false)
			{
				@Override
				protected boolean hasContent (Node node)
				{
					return ((Item) node.getNode ()).hasEditableOptions ();
				}

				@Override
				protected boolean flattenGroup (Node node)
				{
					return (node.parent != null)
						&& !((Item) node.getNode ()).isOptionCategory ();
				}
			});
			ft = new FilterTransformer("");
			tp.add(ft);
			final UIToolkit ui = UIToolkit.get (ctx);
			final Object split = ui.createSplitContainer (JSplitPane.HORIZONTAL_SPLIT);
			final Object split2 = ui.createSplitContainer (JSplitPane.VERTICAL_SPLIT);

			final Object container = ui.createContainer (1, 1, 1);
			final DisposableWrapper dw = new DisposableWrapper ();
			
			
			Registry r = ctx.getWorkbench().getRegistry();
			boolean localOption=false;
			if (r != null && r.getProjectName() != null) {
				Item opts = r.getItem(Option.PROJECT_OPTIONS_PATH+"/"+Option.OPTIONS_FILE);
				if (opts !=null && opts instanceof OptionFileBase) {
					localOption=true;
				}
			}
			

			opt= new OptionTree (ctx, rr,dw,ui, container);
			tp.initialize (opt, rr.getRoot (), this);

			StringMap sm = new StringMap(this);
			String pname = ctx.getWorkbench().getRegistry().getProjectName();
			
			if (localOption) {
				pname = "Workbench: "+ pname;
			}
			else {
				pname = "Global";
			}
			sm.putObject("panelTitle", pname + " Preferences");
			p = ui.createPanel (ctx, dw, sm);
			t = ui.createTreeInSplit (tp, split);
//			ComponentWrapper t = ui.createTree (tp);
			Widget search = ui.createStringWidget(null);
			
			search.addPropertyChangeListener(new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if(evt.getNewValue()!=null) {
						showOnly(evt.getNewValue().toString());
					}else {
						showOnly("");
					}
				}});
			
			ui.addComponent(split2,search.getComponent() ,null);
			ui.addComponent (split2, ui.createScrollPane (t.getComponent ()), null);
		
			ui.addComponent (split, split2, null);

			
			ui.addComponent (split, container, null);
			
			// buttons 
			final Object buttonPanel = ui.createContainer(0);
			ui.setLayout(buttonPanel, new FlowLayout(FlowLayout.RIGHT));
			Object getGlobalOptions = ui.createButton ("Get Global Options", 
					null, null, Font.PLAIN, new Command () {
						@Override
						public void run (Object info, Context ctx)
						{
							PanelFactory.getAndShowPanel(
									ctx.getWorkbench().getMainWorkbench(), "/ui/panels/preferences", new StringMap());
						}
						@Override
						public String getCommandName ()
						{
							return null;
						}
					}, ctx);
			
//			Object applyWorkbenchButton = ui.createButton ("Apply to workbench", 
//					null, null, Font.PLAIN, new Command () {
//						@Override
//						public void run (Object info, Context ctx)
//						{
//							FileSystem fs = ctx.getWorkbench().getRegistry().getFileSystem();
//							Object optionsFile = fs.getFile(fs.getRoot(), Option.WORKBENCH_OPTIONS);
//							
//							if (optionsFile == null) {
//								optionsFile = OptionsSource.create(ctx);
//							}
//						}
//						@Override
//						public String getCommandName ()
//						{
//							return null;
//						}
//					}, ctx);
//			
			if (localOption) {
				ui.addComponent(buttonPanel, getGlobalOptions, null);
			} else {
				ui.addComponent(buttonPanel, ui.createLabel("YOU ARE IN THE GLOBAL OPTIONS", 0), null);
			}

			// Layout
			final Object contentPane = ui.createContainer(5);
			ui.addComponent(contentPane, split, BorderLayout.CENTER);
			ui.addComponent(contentPane, buttonPanel, BorderLayout.SOUTH);

			p.setContent (new ComponentWrapperImpl (contentPane, t));
			
		}
		if (menu != null)
		{
			UI.setMenu (p, menu, this);
		}
		return p;
	}
	public void showOnly(String newValue) {
		ft.filter=newValue;
		tp.update();

	}
	public void run(Object info, Context ctx) {
		if (ctx.getWorkbench().isHeadless()) {
			return;
		}
		Panel p = ctx.getWindow().getPanel(getAbsoluteName());
		if (p == null) {
			p = createPanel(ctx, null);
		}
		if (p != null) {
			
			ObjectList pathList=new ObjectList();
			if(path!=null) {
				pathList.addAll(path.split(","),0,path.split(",").length);
			}
			JTree st = (JTree) t;
			SyncMappedTree o = (SyncMappedTree) st.getModel();

//			Item x = Item.resolveItem(ctx.getWorkbench(), this.getAbsoluteName());
//			Item[] list = Item.findAll (getRegistry (),  this.getAbsoluteName(),
//					  ItemCriterion.INSTANCE_OF, Link.class,
//					  false);
//			for(Item li :list) {
//				Item i = li.resolveLink(ctx.getWorkbench());
//				for (int j = 0; j < o.getChildCount(o.getRoot()); j++) {
//					MappedTree.Node m = (de.grogra.util.MappedTree.Node) o.getChild(o.getRoot(), j);
//					if (((UITreePipeline.Node) m.getSourceNode()).getNode().equals(i)) {
//						st.expandPath(m.getTreePath());

			if(info instanceof String) {
				pathList.add((String)info);
			}
			pathList.add((String) this.getDescription("ItemPath"));
			for( Object pa : pathList) {
				if(pa!=null) {
					String pat = pa.toString();
					Item i = Item.resolveItem(this,pat);
					if(i!=null) {
					opt.setItem(i);
					int ind = pat.lastIndexOf("/");
					if(ind>0) {
						pat=pat.substring(0,ind);
						i = Item.resolveItem(this, pat);
					}
					for (int j = 0; j < o.getChildCount(o.getRoot()); j++) {
						MappedTree.Node m = (de.grogra.util.MappedTree.Node) o.getChild(o.getRoot(), j);
						if (((UITreePipeline.Node) m.getSourceNode()).getNode().equals(i)) {
							st.expandPath(m.getTreePath());
						}
					}
					
				}
			}
			}
				p.show(true, null);
		}
	}

	public static void showOption(Item item, Object info, Context ctx) {
		if(info instanceof String) {
			Item i =Item.resolveItem(item, (String)info);
			if(i==null) {
				String path= java.net.URLDecoder.decode((String)info, StandardCharsets.UTF_8);
				i = getRef(path, Item.resolveItem(item, "/"));
				if(i==null) {
					return;
				}
				info = i.getAbsoluteName();
			}
			OptionExplorerFactory oef = (OptionExplorerFactory )Item.resolveItem(item, "/ui/panels/preferences");
			oef.run(info, ctx);
		}
		
	}
	public static Item getRef(String path, Item tmp) {
		for(String p :path.split("/")) {
			tmp = Item.findFirst(tmp, new ItemCriterion() {

				@Override
				public String getRootDirectory() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean isFulfilled(Item arg0, Object arg1) {
					// TODO Auto-generated method stub
					return arg0.getDescription("Name").equals((String)arg1);
				}}, p, false);
			
			}
	return tmp;
	}
	
	class FilterTransformer implements UITreePipeline.Transformer{

		public String filter;
		
		public FilterTransformer(String filter) {
			this.filter=filter;	
		}
		
		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void initialize(UITreePipeline pipeline) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isAffectedBy(TreePath path) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void transform(de.grogra.pf.ui.tree.UITreePipeline.Node root) {
			isRelevant(root);
		}
		
		
		private boolean isRelevant( de.grogra.pf.ui.tree.UITreePipeline.Node root) {
			boolean relevant=false;
			if(((Item)root.getNode()).getName().toLowerCase().contains(filter.toLowerCase()) || ((String)((Item)root.getNode()).getDescription("Name")).toLowerCase().contains(filter.toLowerCase()) ){
			return true;
			}
			for (de.grogra.pf.ui.tree.UITreePipeline.Node n = (de.grogra.pf.ui.tree.UITreePipeline.Node) root.children; n != null; n = (de.grogra.pf.ui.tree.UITreePipeline.Node)n.next) {
				if(isRelevant(n)) {
					relevant=true;
				}else {
					n.remove();
					n.dispose ();
				}
			}
			return relevant;
		}
		
	}
	
	
	
	
}
