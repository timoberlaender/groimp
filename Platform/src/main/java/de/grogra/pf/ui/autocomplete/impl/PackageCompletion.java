package de.grogra.pf.ui.autocomplete.impl;

import java.util.List;

/**
 * A completion for package names.
 */
public class PackageCompletion extends BasicCompletion {


	String module;
	
	public PackageCompletion(CompletionProvider provider, String replacementText){
		this(provider, replacementText, null);
	}

	public PackageCompletion(CompletionProvider provider, String replacementText,
							String shortDesc) {
		this(provider, replacementText, shortDesc, null);
	}

	public PackageCompletion(CompletionProvider provider, String replacementText,
							String shortDesc, String summary) {
		super(provider, replacementText, shortDesc, summary);
	}

	public List<AbstractCompletion> getContainedClasses(){
		return null;
	}
	
	public void setModule(String m) {
		this.module = m;
	}
	public String getModule() {
		return module;
	}
}
