package de.grogra.pf.ui.registry;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.swing.JSplitPane;
import javax.swing.tree.TreePath;

import de.grogra.pf.registry.Directory;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.ComponentWrapper;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.pf.ui.Widget;
import de.grogra.pf.ui.tree.RegistryAdapter;
import de.grogra.pf.ui.tree.UITree;
import de.grogra.pf.ui.tree.UITreePipeline;
import de.grogra.pf.ui.util.ComponentWrapperImpl;

public class SearchExplorerFactory extends ExplorerFactory {

	private String filter = "";

	SearchExplorerFactory() {
		this(null, null, null, null);
	}

	SearchExplorerFactory(String name, String baseName, String factoryDir, String objectDir) {
		super(name, baseName, factoryDir, objectDir);

	}
	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new SearchExplorerFactory ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new SearchExplorerFactory ();
	}

//enh:end

	@Override
	protected Panel configure(Context ctx, Panel p, Item menu) {
		if (p == null) {
			Item i = Item.resolveItem(ctx.getWorkbench(), objectDir);
			if (i == null) {
				throw new NullPointerException(objectDir + " cannot be resolved");
			}
			UIToolkit ui = UIToolkit.get(ctx);
			p = UIToolkit.get(ctx).createPanel(ctx, null, this);
			UITree tree = createExplorerTree(new RegistryAdapter(ctx, i.getRegistry()), new TreePath(i.getPath()));

			FilterTransformer ft = new FilterTransformer(ctx);
			UITreePipeline tp = new UITreePipeline();
			tp.add(ft);
			tp.initialize(tree, i, this);
			ComponentWrapper t = UIToolkit.get(ctx).createTree(tp);
			UIToolkit.get(ctx).setTransferHandler(t, getTransferHandler());
			UIToolkit.get(ctx).setDataHandlers(t, getDataHandlers());
			Object split = ui.createSplitContainer(JSplitPane.VERTICAL_SPLIT);
			Widget search = ui.createStringWidget(null);
			search.addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if (evt.getNewValue() != null) {
						ft.filter = evt.getNewValue().toString();
					} else {
						ft.filter="";
					}
					tp.update();
				}
			});
			ui.addComponent(split, search.getComponent(), null);
			ui.addComponent(split, UIToolkit.get(ctx).createScrollPane(t.getComponent()), null);
			p.setContent(new ComponentWrapperImpl(split, t));
		}
		if (menu != null) {
			UI.setMenu(p, menu, this);
		}
		return p;
	}

	class FilterTransformer implements UITreePipeline.Transformer {
		Context ctx;
		Registry reg;
		String filter = "";

		public FilterTransformer(Context ctx) {
			this.ctx = ctx;
			reg = ctx.getWorkbench().getRegistry();
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub

		}

		@Override
		public void initialize(UITreePipeline pipeline) {
			// TODO Auto-generated method stub

		}

		@Override
		public void transform(de.grogra.pf.ui.tree.UITreePipeline.Node root) {
			isRelevant(root);
		}

		private boolean isRelevant(de.grogra.pf.ui.tree.UITreePipeline.Node node) {
			boolean relevant = false;
			if (node.getChildCount()==0 && node.getNode() instanceof Directory) {
				return false;
			}
			
			if (((String) ((Item) node.getNode()).getDescription("Name")).toLowerCase()
					.contains(filter.toLowerCase())) {
				relevant = true;
			} else if (node.getNode() instanceof SourceFile) {
				try {

					InputStream s = reg.getFileSystem()
							.getInputStream(reg.getProjectFile((((SourceFile) node.getNode()).getSystemId())));
					String text = new String(s.readAllBytes(), StandardCharsets.UTF_8);
					s.close();
					relevant = (text.contains(filter));

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			
			for (de.grogra.pf.ui.tree.UITreePipeline.Node n = (de.grogra.pf.ui.tree.UITreePipeline.Node) node.children; n != null; n = (de.grogra.pf.ui.tree.UITreePipeline.Node) n.next) {
				if (isRelevant(n)) {
					relevant = true;
				} else {
					n.remove();
					n.dispose();
				}
			}
			return relevant;
		}

		@Override
		public boolean isAffectedBy(TreePath path) {
			// TODO Auto-generated method stub
			return false;
		}

	}

}
