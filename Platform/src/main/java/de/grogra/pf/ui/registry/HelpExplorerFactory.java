package de.grogra.pf.ui.registry;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.tree.TreePath;

import de.grogra.pf.registry.Directory;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.PluginDescriptor;
import de.grogra.pf.registry.expr.Resource;
import de.grogra.pf.ui.ComponentWrapper;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.pf.ui.Widget;
import de.grogra.pf.ui.event.ActionEditEvent;
import de.grogra.pf.ui.tree.RegistryAdapter;
import de.grogra.pf.ui.tree.UINodeHandler;
import de.grogra.pf.ui.tree.UITree;
import de.grogra.pf.ui.tree.UITreePipeline;
import de.grogra.pf.ui.util.ComponentWrapperImpl;
import de.grogra.util.StringMap;

public class HelpExplorerFactory extends PanelFactory {

	ComponentWrapper t;
	FilterTransformer ft;
	UITree reg;
	UITreePipeline tp;
	Object viewer;
	UIToolkit ui;
	boolean asButton = false;
	// enh:field

	String path;
	// enh:field

	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field asButton$FIELD;
	public static final NType.Field path$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (HelpExplorerFactory.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setBoolean (Object o, boolean value)
		{
			switch (id)
			{
				case 0:
					((HelpExplorerFactory) o).asButton = (boolean) value;
					return;
			}
			super.setBoolean (o, value);
		}

		@Override
		public boolean getBoolean (Object o)
		{
			switch (id)
			{
				case 0:
					return ((HelpExplorerFactory) o).asButton;
			}
			return super.getBoolean (o);
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 1:
					((HelpExplorerFactory) o).path = (String) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 1:
					return ((HelpExplorerFactory) o).path;
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new HelpExplorerFactory ());
		$TYPE.addManagedField (asButton$FIELD = new _Field ("asButton", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 0));
		$TYPE.addManagedField (path$FIELD = new _Field ("path", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 1));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new HelpExplorerFactory ();
	}

//enh:end

	public HelpExplorerFactory() {
		super();
		setName("?");
	}

	@Override
	protected Panel configure(Context ctx, Panel p, Item menu) {
		// if (p == null)
		{

			ui = UIToolkit.get(ctx);
			viewer = ui.createTextViewer(null, null, null, null, true);
			ft = new FilterTransformer("");
			
			
			if(viewer instanceof JComponent) {
				JComponent c = (JComponent) ((JComponent)viewer).getClientProperty ("de.grogra.pf.ui.swing.EDITOR_PANE");
				c.addPropertyChangeListener(new PropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent evt) {
						if(evt.getPropertyName().equals("page")) {
							ui.hightliteContent(viewer,ft.filter);
						}
					}
				});
			}
			Item help = Item.resolveItem(ctx.getWorkbench(), "/help/doc");

			reg = new RegistryAdapter(ctx, ctx.getWorkbench().getRegistry()) {
				@Override
				public void eventOccured(final Object node, java.util.EventObject event) {
					if (node instanceof Resource) {

						java.net.URL u = (java.net.URL) ((Resource) node).evaluate(this,
								new StringMap().putBoolean("pluginURL", true));

						String path = u.getPath();

						if ("plugin".equals(u.getProtocol()) && u.getRef() != null) {

							int i = path.indexOf('/');
//							String[] parts = path.split("/");
							PluginDescriptor p = getRegistry().getPluginDescriptor(path.substring(0, i));
							ClassLoader c = p.getClassLoader();
							java.net.URL tmp = c.getResource(path.substring(i + 1));

							java.net.URL u2 = u;
							try {
								u2 = new java.net.URL(tmp.toString() + "#" + u.getRef());
							} catch (MalformedURLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							u = u2;

						}

						ui.setContent(viewer, u);
//						}

//						((ActionEditEvent) event).consume();
//						((JEditorPane)((JPanel)viewer).getClientProperty("de.grogra.pf.ui.swing.EDITOR_PANE")).scrollToReference(u.getRef());

					} else if (node instanceof Directory) {
						Item child = Item.findFirst((Item) node, ItemCriterion.INSTANCE_OF, Resource.class, true);
						if (child != null) {
							ui.setContent(viewer, (java.net.URL) ((Resource) child).evaluate(this,
									new StringMap().putBoolean("pluginURL", true)));
						}
					}
					((ActionEditEvent) event).consume();
//					ui.hightliteContent(viewer,ft.filter);
				
				}

				@Override
				public Object invoke(Object node, String method, Object arg) {
					if (UIToolkit.EXPLORER_ACTION.equals(method) && (node instanceof Resource)) {
						ui.setContent(viewer, (java.net.URL) ((Resource) node).evaluate(this,
								new StringMap().putBoolean("pluginURL", true)));
						UI.consume(arg);
						return null;

					}
					return super.invoke(node, method, arg);
				}
			};
			tp = new UITreePipeline();
			tp.add(ft);
			tp.initialize(reg, help, this);
			Object split = ui.createSplitContainer(JSplitPane.HORIZONTAL_SPLIT);
			t = ui.createTreeInSplit(tp, split);
			// ComponentWrapper t = ui.createTree(createExplorerTree (reg, new TreePath
			// (help.getPath ())));

			Object split2 = ui.createSplitContainer(JSplitPane.VERTICAL_SPLIT);
			Widget search = ui.createStringWidget(null);

			search.addPropertyChangeListener(new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if (evt.getNewValue() != null) {
						showOnly(evt.getNewValue().toString());
					} else {
						showOnly("");
					}
				}
			});

			ui.addComponent(split2, search.getComponent(), null);
			ui.addComponent(split2, ui.createScrollPane(t.getComponent()), null);

			ui.addComponent(split, split2, null);
			ui.addComponent(split, viewer, null);
			// final DisposableWrapper dw = new DisposableWrapper ();
			p = ui.createPanel(ctx, null, this);
			p.setContent(new ComponentWrapperImpl(split, t));
		}
		return p;
	}

	public void showOnly(String newValue) {
		ft.filter = newValue;
		tp.update();
		ui.hightliteContent(viewer,ft.filter);

	}

	public static void showHelp(Item item, Object info, Context ctx) {
		HelpExplorerFactory hef = (HelpExplorerFactory) Item.resolveItem(item, "/ui/doc/help");
		hef.run(info, ctx);
	}

	public void run(Object info, Context ctx) {
		Panel p = ctx.getWindow().getPanel(getAbsoluteName());
		if (p == null) {
			p = createPanel(ctx, null);

		}

		if (info instanceof String) {
			path = info.toString();
		}

		Object docLink = null;
		if (path == null) { // check if Path was given as registry attribute
			docLink = getDescription("docLink"); // read path from helpbutton description
		}
		String key = ctx.getPanel().getPanelId();
		//System.err.println(key);
		if (key != null && docLink == null) {
			Item pf = Item.resolveItem(ctx.getWorkbench(), key);
			docLink = pf.getDescription("docLink"); // read path from panel description
			//System.err.println(pf.getPluginDescriptor());
			if (pf != null) {
				if (docLink == null && pf.getI18NBundle().containsKey("docLink")) {
					docLink = pf.getI18NBundle().getObject("docLink"); // read path from Plugin description
				}
			}
		}
		if (docLink != null) {
			path = (String) docLink;
		}

		// find Path and select
		if (path != null) {
			Item i = getRef(path);
			if (i != null) {
				reg.eventOccured(i, new ActionEditEvent("x", 0));
			}
		}
		path = null;
		p.show(true, null);

	}

	private Item getRef(String path) {
		Item tmp = Item.resolveItem(getRegistry(), "/help/doc");
		for (String p : path.split("/")) {
			tmp = Item.findFirst(tmp, new ItemCriterion() {

				@Override
				public String getRootDirectory() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean isFulfilled(Item arg0, Object arg1) {
					// TODO Auto-generated method stub
					return arg0.getDescription("Name").equals((String) arg1);
				}
			}, p, false);

		}
		return tmp;
	}

	public int getUINodeType() {
		if (asButton) {
			return UINodeHandler.NT_HELP;
		}
		return UINodeHandler.NT_ITEM;
	}

	class FilterTransformer implements UITreePipeline.Transformer {

		public String filter;

		public FilterTransformer(String filter) {
			this.filter = filter;
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub

		}

		@Override
		public void initialize(UITreePipeline pipeline) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean isAffectedBy(TreePath path) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void transform(de.grogra.pf.ui.tree.UITreePipeline.Node root) {
			isRelevant(root);
		}

		private boolean isRelevant(de.grogra.pf.ui.tree.UITreePipeline.Node node) {
			boolean relevant = false;
			if (((String) ((Item) node.getNode()).getDescription("Name")).toLowerCase()
					.contains(filter.toLowerCase())) {
				relevant = true;
			} else {
				if (node.getNode() instanceof Resource) {
					java.net.URL u = (java.net.URL) ((Resource) node.getNode()).evaluate(HelpExplorerFactory.this,
							new StringMap().putBoolean("pluginURL", true));
						 //.anyMatch(s -> s.contains("four"));
					
						try {
							InputStream s = u.openStream();
							 String text = new String(s.readAllBytes(), StandardCharsets.UTF_8);
							 s.close();
							 text = text.replaceAll("<[^>]*>", "");
							 if (text.toLowerCase().contains(filter.toLowerCase())) {
								relevant=true;
							}
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
				}

			}
			for (de.grogra.pf.ui.tree.UITreePipeline.Node n = (de.grogra.pf.ui.tree.UITreePipeline.Node) node.children; n != null; n = (de.grogra.pf.ui.tree.UITreePipeline.Node) n.next) {
				if (isRelevant(n)) {
					relevant = true;
				} else {
					n.remove();
					n.dispose();
				}
			}
			return relevant;
		}

	}

}
