package de.grogra.pf.ui.autocomplete.impl;

import java.awt.Point;
import java.util.List;

import de.grogra.pf.ui.autocomplete.AutoCompletableTextArea;

/**
 * A completion provider that do not manage the completion itself. The parent it 
 * processing it. 
 */
public class ChildrenCompletionProvider extends AbstractCompletionProvider {

	
	@Override 
	protected void checkProviderAndAdd(Completion c) {
		if (c.getProvider()!=getParent()) {
			throw new IllegalArgumentException("Invalid CompletionProvider");
		}
		completions.add(c);
	}

	
	@Override
	public String getAlreadyEnteredText(AutoCompletableTextArea comp) {
		return comp.getAlreadyEnteredText(getParent());
	}

	@Override
	public boolean isValidChar(char c) {
		return getParent().isValidChar(c);
	}

	@Override
	public List<Completion> getCompletionsAt(AutoCompletableTextArea comp, Point p) {

		String text = comp.getAlreadyEnteredText(this);

		List<Completion> list = getCompletionByInputText(text);
		return list;
	}

	@Override
	public List<ParameterizedCompletion> getParameterizedCompletions(AutoCompletableTextArea tc) {
		// TODO Auto-generated method stub
		return null;
	}

}
