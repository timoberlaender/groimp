package de.grogra.pf.ui.autocomplete;

import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.RegistryContext;

/**
 * Context in which the completion happens. 
 */
public abstract class CompletionContext implements RegistryContext {

	public final static int NOCONTEXT = 0;
	public final static int COMMENT = 1;
	public final static int JAVA = 2;
	public final static int IMPORT = 4;
	//xl is 8
	public final static int NEWCLASS = 16;
	public final static int CLASS = 32;
	public final static int CLASSDEF = 64;
	public final static int INPARAMETERS = 128;
	public final static int INFUNC = 256;
	
	protected Registry r;
	protected AutoCompletableTextArea textComp;
	
	public CompletionContext(AutoCompletableTextArea tc, RegistryContext reg) {
		this.textComp=tc;
		this.r = reg.getRegistry();
	}

	@Override
	public Registry getRegistry() {
		return r;
	}
	
	public void setTextArea(AutoCompletableTextArea tc) {
		if (this.textComp!=null) {
			this.textComp=null;
		}
		this.textComp=tc;
	}
	
	public abstract int getContext();
}
