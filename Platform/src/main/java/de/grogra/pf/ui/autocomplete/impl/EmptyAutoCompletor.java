package de.grogra.pf.ui.autocomplete.impl;

import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.ui.autocomplete.AutoCompletableTextArea;
import de.grogra.pf.ui.autocomplete.CompletionContext;

public class EmptyAutoCompletor extends AbstractAutoCompletor {

	@Override
	protected CompletionProvider createProvider() {
		return 	new DefaultCompletionProvider();
	}

	@Override
	protected CompletionContext createCompletionContext(AutoCompletableTextArea tc, RegistryContext reg) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void installHooks() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void uninstallHooks() {
		// TODO Auto-generated method stub
		
	}


}
