
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.ui.registry;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.nio.charset.StandardCharsets;

import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.tree.TreePath;

import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Link;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.ComponentWrapper;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.event.ActionEditEvent;
import de.grogra.pf.ui.registry.OptionExplorerFactory.FilterTransformer;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.pf.ui.Widget;
import de.grogra.pf.ui.tree.HierarchyFlattener;
import de.grogra.pf.ui.tree.RegistryAdapter;
import de.grogra.pf.ui.tree.SyncMappedTree;
import de.grogra.pf.ui.tree.UITreePipeline;
import de.grogra.pf.ui.tree.UITreePipeline.Node;
import de.grogra.pf.ui.util.ComponentWrapperImpl;
import de.grogra.util.DisposableWrapper;
import de.grogra.util.MappedTree;

public class ProjectExplorerFactory extends PanelFactory
{

	private ComponentWrapper t ;
	private UITreePipeline tp;
	private FilterTransformer ft;
	ProjectTree  pt;
//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new ProjectExplorerFactory ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new ProjectExplorerFactory ();
	}

//enh:end

	class ProjectTree extends RegistryAdapter
	{
		private Object editorComponent;
		private UIToolkit ui;
		private DisposableWrapper dw;
		private Object container;
		ProjectTree (Context c, Registry r,  DisposableWrapper dw , UIToolkit ui,Object container)
		{
			super (c, r);
			this.dw=dw;
			this.ui=ui;
			this.container =container;
		}

		@Override
		public int getType (Object node)
		{
			int t = super.getType (node);
			return (t == NT_UNDEFINED) ? NT_ITEM : t;
		}

		@Override
		public void eventOccured (final Object node,
								  java.util.EventObject event)
		{
			if (!(event instanceof ActionEditEvent))
			{
				super.eventOccured (node, event);
				return;
			}
			Item item = (Item) node;
			if (((ActionEditEvent) event).isConsumed () ){
				return;
			}
			((ActionEditEvent) event).consume ();
			setItem (item);
		}
		
		void setItem (Item item)
		{
			if (dw.disposable != null)
			{
				ui.removeComponent (editorComponent);
				dw.dispose ();
				editorComponent = null;
			}
			ComponentWrapper w = ui.createDisplayForProject(getContext (), item);
//			ComponentWrapper w = new ProjectSelection
//				 (getContext (), item, true)
//				 .createPropertyEditorComponent ();
			if (w != null)
			{
				dw.disposable = w;
				ui.addComponent (container, editorComponent = w.getComponent (), null);
			}
			ui.revalidate (container);
		}
	}
	
	
	ProjectExplorerFactory ()
	{
		super ();
	}


	@Override
	protected Panel configure (Context ctx, Panel p, Item menu)
	{
		if (p == null)
		{
			Registry rr = ctx.getWorkbench ().getRegistry ().getRootRegistry ();
			tp = new UITreePipeline ();
			tp.add (new HierarchyFlattener (null, false)
			{
				@Override
				protected boolean hasContent (Node node)
				{
					return ((Item) node.getNode () instanceof ProjectDirectory);
				}

				@Override
				protected boolean flattenGroup (Node node)
				{
					return (node.parent != null)
							&& !((Item) node.getNode ()).isOptionCategory ();
				}

				@Override
				protected boolean isLeaf (Node node)
				{
					return (node.isLeaf () || ((Item) node.getNode () instanceof ProjectDirectory));
				}
			});
			ft = new FilterTransformer("");
			tp.add(ft);
			final UIToolkit ui = UIToolkit.get (ctx);
			final Object split = ui.createSplitContainer (JSplitPane.HORIZONTAL_SPLIT);
			final Object split2 = ui.createSplitContainer (JSplitPane.VERTICAL_SPLIT);
			final Object container = ui.createContainer (1, 1, 1);
			final DisposableWrapper dw = new DisposableWrapper ();
			pt = new ProjectTree (ctx, rr,dw,ui,container);


			tp.initialize (pt, Item.resolveItem(rr, "/examples"), this);

			p = ui.createPanel (ctx, dw, this);
			t = ui.createTreeInSplit (tp, split);
//			ComponentWrapper t = ui.createTree (tp);
			Widget search = ui.createStringWidget(null);
			
			search.addPropertyChangeListener(new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if(evt.getNewValue()!=null) {
						showOnly(evt.getNewValue().toString());
					}else {
						showOnly("");
					}
				}});
			
			ui.addComponent(split2,search.getComponent() ,null);
			ui.addComponent (split2, ui.createScrollPane (t.getComponent ()), null);
		
			ui.addComponent (split, split2, null);

			ui.addComponent (split, container, null);
			p.setContent (new ComponentWrapperImpl (split, t));
		}
		if (menu != null)
		{
			UI.setMenu (p, menu, this);
		}
		return p;
	}
	public void showOnly(String newValue) {
		ft.filter=newValue;
		tp.update();

	}
	
	public void run(Object info, Context ctx) {
		if (ctx.getWorkbench().isHeadless()) {
			return;
		}
		Panel p = ctx.getWindow().getPanel(getAbsoluteName());
		if (p == null) {
			p = createPanel(ctx, null);
		}
		if (p != null) {
			JTree st = (JTree) t;
			SyncMappedTree o = (SyncMappedTree) st.getModel();
			String path=null;
			
			if(info instanceof String) {
				path =(String)info;
			}
			if(path==null) {
				path = (String) this.getDescription("ItemPath");
			}
			if(path!=null) {
				Item i = Item.resolveItem(this, "/examples"+path);
				pt.setItem(i);
				int ind = path.lastIndexOf("/");
				if(ind>0) {
					path=path.substring(0,ind);
					i = Item.resolveItem(this, "/examples"+path);
				}
				for (int j = 0; j < o.getChildCount(o.getRoot()); j++) {
					MappedTree.Node m = (de.grogra.util.MappedTree.Node) o.getChild(o.getRoot(), j);
					if (((UITreePipeline.Node) m.getSourceNode()).getNode().equals(i)) {
						st.expandPath(m.getTreePath());
					}
				}
				
			}
			
			p.show(true, null);
		}
	}
	public static void showExample(Item item, Object info, Context ctx) {
		if(info instanceof String) {
			Item i =Item.resolveItem(item.getRegistry(), "/examples"+(String)info);
			if(i==null) {
				String path= java.net.URLDecoder.decode((String)info, StandardCharsets.UTF_8);
				i = getRef(path, Item.resolveItem(item.getRegistry(), "/examples"));
				if(i==null) {
					return;
				}
				info = i.getAbsoluteName().substring(9);
			}
		
		ProjectExplorerFactory pef = (ProjectExplorerFactory )Item.resolveItem(item, "/ui/panels/projectsselection");
		pef.run(info, ctx);
		
	}
	}
	public static Item getRef(String path, Item tmp) {
		for(String p :path.split("/")) {
			if(p.length()>0) {
			tmp = Item.findFirst(tmp, new ItemCriterion() {

				@Override
				public String getRootDirectory() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean isFulfilled(Item arg0, Object arg1) {
					// TODO Auto-generated method stub
					return arg0.getDescription("Name").equals((String)arg1);
				}}, p, false);
			}
			}
	return tmp;
	}
	class FilterTransformer implements UITreePipeline.Transformer{

		public String filter;
		
		public FilterTransformer(String filter) {
			this.filter=filter;	
		}
		
		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void initialize(UITreePipeline pipeline) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isAffectedBy(TreePath path) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void transform(de.grogra.pf.ui.tree.UITreePipeline.Node root) {
			isRelevant(root);
		}
		
		
		private boolean isRelevant( de.grogra.pf.ui.tree.UITreePipeline.Node root) {
			boolean relevant=false;
			if(((Item)root.getNode()).getName().toLowerCase().contains(filter.toLowerCase()) || ((String)((Item)root.getNode()).getDescription("Name")).toLowerCase().contains(filter.toLowerCase()) ){
				return true;
			}
			for (de.grogra.pf.ui.tree.UITreePipeline.Node n = (de.grogra.pf.ui.tree.UITreePipeline.Node) root.children; n != null; n = (de.grogra.pf.ui.tree.UITreePipeline.Node)n.next) {
				if(isRelevant(n)) {
					relevant=true;
				}else {
					n.remove();
					n.dispose ();
				}
			}
			return relevant;
		}
		
	}
	
	
	
}
